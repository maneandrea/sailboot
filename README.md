# sailboot

Highly customizable framework for performing numerical bootstrap computations via
the semidefinite program solver [`sdpb`](https://github.com/davidsd/sdpb),
suitable both for usage on a PC or on a computing cluster.

## Quick reference guide

### Getting started

Install the dependencies, written below, then execute the script `install.sh`.

### Documentation and tests

See `docs/documentation.pdf` for the documentation and run `tests/test.py` to test that you are set up correctly.

### Workflow

We define a "job" a single job submitted to the cluster and a "project" a set 
of jobs that differ only in a few parameters. A project will be an entire plot
while a job will be a single point in that plot.

These first steps need to be done only once and are common to all projects:

1. Write the crossing equations in an appropriate format (see documentation)

2. Generate the blocks with [`scalar_blocks`](https://gitlab.com/bootstrapcollaboration/scalar_blocks)

3. Set up a configuration file like `scripts/launcher.config`

Next, for each project, we do the following:

1. Write a script that generates the bootstrap problem you want to solve, like `scripts/example.py`

2. Call `launch <yourscript>.py`

3. Once all jobs are done, copy the content of `<path/to/proj>/results/summary.m` 
   in *Mathematica*
   
After or while the jobs are running you can do the following things

1. Monitor the progress with `monitor <path/to/proj>`

2. Kill jobs with `monitor <path/to/proj> kill -j <jobs>`

3. Run more jobs. To do that it suffices to overwrite the script with other points

In general, if you need help call `<script you need help for> --help`

### Format of the crossing equations

The crossing equations can be written in Mathematica form and they will be parsed by Python 
(so there is no need for a Mathematica license on the cluster).

The blocks that appear in the vectors must be given names that correspond to the folders in which
they have been saved.

For details on how to write the crossing equations, read the comments in `scripts/crosseq_template.m`.

## Dependencies

- python 3.6+
- mpmath
- pyparsing
- sympy (only for parsing crossing equations that contain mathematica code in them)
- sdpb
- scalar_blocks *[optional]*
- sagemath *[optional]* (it's never imported explicitly, but mpmath can use it as a backend to improve performance)
- gmpy2 *[optional/alternative]* (in absence of sagemath, also gmpy can be used as backend)

## License

This program is distributed under the terms of the GNU General Public License,
version 3 or later. See the file [LICENSE][license] for details.


---

For any issues or contributions use the GitLab interface or email me at  
andrea **[dot]** manenti **[at]** physics **[dot]** uu **[dot]** se

[license]: LICENSE
