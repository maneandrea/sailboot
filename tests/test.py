#!/usr/bin/env python
import os
import subprocess
import sys
import time
import shutil
import re
import argparse

sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'src'))

try:
    from sailboot import sailboot
    import launch
    # we have to do this trick otherwise launch puts __init__.py as sailboot executable
    launch.sailboot.__file__ = sailboot.__file__
except ModuleNotFoundError:
    import launch
import monitor


# This is just to automatically count how many jobs are being submitted
try:
    with open('singlet_bound_N20.py') as f:
        text = f.read()
        for_loop = re.search(r'for delta_phi in \[(.*)]:', text)
        if for_loop:
            TOTAL_JOBS = for_loop.group(1).count(',') + 1
        else:
            print('Error: The file singlet_bound_N20.py seems to have been modified')
            sys.exit(1)
except FileNotFoundError:
    print('Error: File singlet_bound_N20.py not found')
    sys.exit(1)


def run_command(command_line):
    """Run a command from within Python"""

    command_list = command_line.split(' ')
    if command_list[0] == 'launch':
        launch.main_array(command_list[1:])
        return
    elif command_list[0] == 'monitor':
        monitor.main_monitor(command_list[1:])
        return
    else:
        return subprocess.run(command_list, encoding='utf-8', capture_output=True)


def main():
    """Test script for the program"""

    parser = argparse.ArgumentParser('test', description='testing script for sailboot')
    parser.add_argument('action', choices=['clean', 'test'], default='test', nargs='?',
                        help='use clean to remove blocks and project folder')

    args = parser.parse_args()

    if args.action == 'clean':
        try:
            shutil.rmtree(os.path.join(os.path.dirname(__file__), 'blocks-nmax2'))
            shutil.rmtree(os.path.join(os.path.dirname(__file__), 'test_project'))
        except FileNotFoundError:
            pass
    else:

        # Number of columns in the console
        line_length = int(os.get_terminal_size()[0])
        occupied = 0

        # Messages like checking .... [PASS]
        def message(text):
            print(text, end='')
            nonlocal occupied
            occupied += len(text)

        def passed():
            nonlocal occupied
            space = (line_length - occupied - 6)*' '
            print(space + '[\033[1m\033[92mPASS\033[0m]')
            occupied = 0

        def failed(text):
            nonlocal occupied
            space = (line_length - occupied - 6) * ' '
            print(space + '[\033[1m\033[91mFAIL\033[0m]')
            print(' └── Error: ' + text)
            sys.exit(1)

        # Initial confirm
        print('This will start a test that makes a singlet bound for the O(N) model with N=20 at nmax=2.')
        print('It will take about 15 minutes, use a processor per job (4 in total) and produce ~300MB of data.', end='')
        cont = None
        while cont is None:
            repl = input(' Continue? [Y/n] ')
            if repl in ['y', 'Y', 'yes', 'Yes', '']:
                cont = True
            elif repl in ['n', 'N', 'no', 'No']:
                cont = False
            else:
                print('Choose yes (y) or no (n).')
        if not cont:
            print('Ok, bye')
            sys.exit(0)

        # 1. Checking that executables exist
        for exe in ['sdpb', 'scalar_blocks']:
            message(f'checking for {exe} executable')
            try:
                exe_process = run_command(f'{exe} --help')
            except FileNotFoundError:
                failed(f'file {exe} not found')
            else:
                if exe_process.returncode == 0:
                    passed()
                else:
                    failed(exe_process.stderr)

        # 2. Computing the blocks
        script_folder = os.path.dirname(__file__)
        block_folder = os.path.join(script_folder, 'blocks-nmax2', 'blocks')
        message('computing blocks')
        blocks_process = run_command(' '.join([
            'scalar_blocks',
            '--dim', '3',
            '--max-derivs', '2',
            '--order', '30',
            '--poles', '8',
            '--spin-ranges', '0-20',
            '--delta-12', '0',
            '--delta-34', '0',
            '--output-poles',
            '--precision', '650',
            '--num-threads', '6',
            '-o', block_folder])
        )

        def expected_blocks():
            for spin in range(21):
                yield 'zzbDerivTable-d3-delta12-0-delta34-0-L{}-nmax2-keptPoleOrder8-order30.m'.format(spin)

        if all([os.path.isfile(os.path.join(block_folder, f)) for f in expected_blocks()]):
            passed()
        else:
            failed(blocks_process.stderr)

        message('computing identity')
        identity_folder = os.path.join(script_folder, 'blocks-nmax2', 'identity')
        identity_process = run_command(' '.join([
            'python',
            'identity_block.py',
            '--dim', '3',
            '--max-derivs', '2',
            '--order', '30',
            '--poles', '8',
            '-o', identity_folder])
        )
        if os.path.isfile(os.path.join(
                identity_folder,
                'zzbDerivTable-d3-delta12-0-delta34-0-L0-nmax2-keptPoleOrder8-order30.m')):
            passed()
        else:
            failed(identity_process.stderr)

        # 3. Launching the jobs
        os.chdir(script_folder)
        print('launching jobs with "launch singlet_bound_N20.py --executable local_sbatch.py --skip"')
        try:
            run_command('launch singlet_bound_N20.py --skip --executable ./local_sbatch.py')
        except Exception as e:
            message('job submission')
            failed(str(e))
        else:
            message('job submission')
            passed()

        # 4. Monitoring jobs
        message(f'monitoring jobs with "monitor test_project --watch-sdpb --repeat -j 1,{TOTAL_JOBS} --max 8"')

        # Open this in a different console
        if os.name == 'nt':
            subprocess.Popen(['monitor', 'test_project', '--watch-sdpb', '--repeat', '-j', f'1,{TOTAL_JOBS}',
                              '--max', '8'],
                             creationflags=subprocess.CREATE_NEW_CONSOLE)
        else:
            monitor_string = f'monitor test_project --watch-sdpb --repeat -j 1,{TOTAL_JOBS} --max 8'
            if shutil.which('konsole') is not None:
                subprocess.Popen([
                    'konsole', '--hold', '-e', monitor_string
                ], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            elif shutil.which('gnome-terminal') is not None:
                subprocess.Popen([
                    'gnome-terminal', '-x', monitor_string
                ], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            else:
                message('\n')
                occupied = 0
                message('could not find default terminal emulator, run the above command by hand.')

        passed()

        # 5. Checking that the jobs terminated correctly
        string = 'checking if the jobs terminated successfully'
        print(string, end='\r')  # I need a carriage return otherwise the console won't print it until it's all done
        completed = set()
        while completed != set(range(1, TOTAL_JOBS+1)):
            outcome = monitor.read_monitor(
                os.path.join(script_folder, 'test_project', 'monitor.txt'), f'1-{TOTAL_JOBS}'
            )
            for job, messages in outcome.items():
                if messages[-1] == 'Has crashed':
                    message(string)
                    failed(f'job {job} has crashed')
                elif messages[-1] == 'Has ended':
                    completed.add(int(job))
            print(string, end='\r')
            time.sleep(5)

        message(string)
        passed()

        message('checking if the result file exists')
        if os.path.isfile(os.path.join(script_folder, 'test_project', 'results', 'summary.m')):
            passed()
        else:
            failed('the file test_project/results/summary.m does not exist')

        # 6. Checking the result
        print('Great! Now I will open the notebook show_plot.nb and you can execute it')
        message(f'checking for mathematica executable')
        try:
            exe_process = subprocess.run(['mathematica', '--help'], encoding='utf-8', capture_output=True)
        except FileNotFoundError:
            failed('you don\'t have mathematica in your system. Check the result in some other way.')
        else:
            if exe_process.returncode == 0:
                passed()
            else:
                failed(exe_process.stderr)

        subprocess.run(['mathematica', 'show_plot.nb'], encoding='utf-8', capture_output=True)

        success = None
        while success is None:
            success = input('Does it look like the one in the documentation? [y/n]\n')
            if success in ['y', 'Y', 'yes', 'Yes']:
                passed()
            elif success in ['n', 'N', 'no', 'No']:
                failed('the plot did not appear to be the same as in the documentation')
            else:
                print('Choose yes (y) or no (n).')

        # 7. End
        print('Yay!')


if __name__ == '__main__':
    main()
