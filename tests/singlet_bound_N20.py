# Name and path
proj_name = 'test_project'
proj_path = 'test_project'
# Settings for sbatch
sbatch_opts = '''\
#!/bin/bash
'''
# String of commands
commands = '''\
bisect --Lambda 3 --precision 650 --threshold 0.1
--to-bisect '("singlet",0,"gap")' -m 1.5 -M 5
--assumptions '("tensor",0,"gap",1)'
--external-dim {0:f}
--job-param {0:f}
--replacement "{{'n'->{1:f}}}"
--config launcher.config
'''


# Function that generates jobs
def generate_args_list():

    n = 20

    for delta_phi in [.501, .502, .503, .504]:
        yield commands.format(delta_phi, n)

