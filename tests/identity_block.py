#!/usr/bin/env python

import os
import argparse


def main_identity():

    parser = argparse.ArgumentParser('identity_block', description='output the identity blocks (i.e. "1") in the '
                                                                   'format of scalar_blocks')
    parser.add_argument('--dim', help='Dimension')
    parser.add_argument('--order', help='Depth of recursion', type=int)
    parser.add_argument('--max-derivs', help='Max number of derivs (n_max)', type=int)
    parser.add_argument('--poles', help='A single number indicating the pole order', type=int)
    parser.add_argument('--output-ab', help='Output a,b derivaties instead of z,zb derivatives', action='store_true')
    parser.add_argument('-o', dest='output', help='Output directory')

    args = parser.parse_args()

    output_directory = args.output

    os.makedirs(output_directory, exist_ok=True)

    order = args.order
    dimension = args.dim
    kept_pole_order = args.poles
    n_max = args.max_derivs
    ab = args.output_ab

    if ab:
        name_template = 'abDerivTable-d{dim:}-delta12-0-delta34-0-L0-nmax{nmax:}-keptPoleOrder{kept:}-order{ord:}.m'
    else:
        name_template = 'zzbDerivTable-d{dim:}-delta12-0-delta34-0-L0-nmax{nmax:}-keptPoleOrder{kept:}-order{ord:}.m'

    def deriv(n, m):
        if n == 0 and m == 0:
            return '1'
        else:
            return '0'

    with open(os.path.join(output_directory, name_template.format(
            dim=dimension,
            nmax=n_max,
            kept=kept_pole_order,
            ord=order
    )), 'w+') as f:
        f.write('{')
        if ab:
            for m in range(n_max):
                for n in range(min(2*n_max, 2*n_max - 2*m)):
                    f.write(f'abDeriv[{n},{m}] -> ' + deriv(n, m) + ',\n ')
        else:
            for n in range(2*n_max):
                for m in range(min(n+1, 2*n_max-n)):
                    f.write(f'zzbDeriv[{n},{m}] -> ' + deriv(n, m) + ',\n ')
        f.write('shiftedPoles -> {{}}}\n')


if __name__ == '__main__':
    main_identity()
