(* ::Package:: *)

(* ::Section:: *)
(*Crossing equations for O(N)*)


(* My vectors *)
crossingEquations = {
	{
		0,
		F["blocks"][spin->Leven, deltaPhi -> \[CapitalDelta]1],
		H["blocks"][spin->Leven, deltaPhi -> \[CapitalDelta]1]
	},
	{
		F["blocks"][spin->Leven, deltaPhi -> \[CapitalDelta]1],
		coeff[1-2/n]F["blocks"][spin->Leven, deltaPhi -> \[CapitalDelta]1],
		coeff[-1-2/n]H["blocks"][spin->Leven, deltaPhi -> \[CapitalDelta]1]
	},
	{
		-F["blocks"][spin->Lodd, deltaPhi -> \[CapitalDelta]1],
		F["blocks"][spin->Lodd, deltaPhi -> \[CapitalDelta]1],
		-H["blocks"][spin->Lodd, deltaPhi -> \[CapitalDelta]1]
	}
};


(* The identity block *)
identityBlock= {
	0,
	F["identity"][deltaPhi -> \[CapitalDelta]1, delta->0],
	H["identity"][deltaPhi -> \[CapitalDelta]1, delta->0]
};



(* Representations *)
vectorNaming = {singlet, tensor, antisymmetric};
