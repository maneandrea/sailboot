#!/usr/bin/env python

import threading
import sys, subprocess
import re
import os

n = hash(sys.argv[1])

if os.path.isfile(sys.argv[1]):
    slurm = os.path.join(os.path.dirname(sys.argv[1]), 'slurm-{:08d}.out'.format(n & 0xffffffff))
    print('Submitted batch job {:08d}'.format(n & 0xffffffff))
    pre = os.path.realpath(sys.argv[1])
else:
    slurm = os.path.join(os.getcwd(),'slurm-{:08d}.out'.format(n & 0xffffffff))
    print('Submitted batch job {:08d}'.format(n & 0xffffffff))
    pre = sys.argv[1]

fd = open(slurm, 'w+')


def f():
    subprocess.Popen([pre] + sys.argv[2:], stdout=fd, stderr=fd)


proc = threading.Thread(target=f)

proc.start()
