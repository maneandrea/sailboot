#!/bin/bash

# Colors
GRN="\033[1m\033[32m"
YEL="\033[1m\033[93m"
NOR="\033[0m"

PYVER=$(python --version | sed -e "s/Python 3\.\([0-9]\+\)\.\([0-9]\+\)/3.\1/")

showHelp() {
# `cat << EOF` This means that cat should stop reading when EOF is detected
cat << EOF
Usage: ./install.sh [-h] [options]
Install sailboot in a custom location

-h, --help      show this help message and exit
-b, --bin-path  path of the executables
-l, --lib-path  path of the libraries

EOF
# EOF is found above and hence cat command stops reading. This is equivalent to echo but much neater when printing out.
}

# $@ is all command line parameters passed to the script.
# -o is for short options like -v
# -l is for long options with double dash like --version
# the comma separates different long options
# -a is for long options with single dash like -version
options=$(getopt -l "help,bin-path:,lib-path:" -o "h,b:,l:" -a -- "$@")

export expanded_bin_dir=""
export expanded_lib_dir=""

# set --:
# If no arguments follow this option, then the positional parameters are unset. Otherwise, the positional parameters
# are set to the arguments, even if some of them begin with a ‘-’.
eval set -- "$options"

while true
do
case "$1" in
-h|--help)
    showHelp
    exit 0
    ;;
-b|--bin-path)
    shift
    bin_dir="$1"
    export expanded_bin_dir="${bin_dir/#\~/$HOME}"
    ;;
-l|--lib-path)
    shift
    lib_dir="$1"
    export expanded_lib_dir="${lib_dir/#\~/$HOME}"
    ;;
--)
    shift
    break;;
esac
shift
done

# Asking for where to put the executable
if [[ -z "${expanded_bin_dir}" ]] || [[ ! -d "${expanded_bin_dir}" ]]; then
  if [[ -n "${expanded_bin_dir}" ]]; then
      echo "${expanded_bin_dir} does not exist - please enter a valid directory path"
  fi
  while read -ep "Enter path where to install the executables (leave blank for ~/.local/bin): " bin_dir; do
      expanded_bin_dir="${bin_dir/#\~/$HOME}"
      if [[ -d "${expanded_bin_dir}" ]]; then
           BINS="${expanded_bin_dir}"
           break
      elif [[ -z "${expanded_bin_dir}" ]]; then
          if [[ -d "$HOME/.local/bin" ]]; then
              BINS="$HOME/.local/bin"
              break
          else
              echo "$HOME/.local/bin does not exist - please enter a valid directory path"
          fi
      else
           echo "${bin_dir} does not exist - please enter a valid directory path"
      fi
  done
else
  BINS="${expanded_bin_dir}"
fi

# Asking for where to put the library
if [[ -z "${expanded_lib_dir}" ]] || [[ ! -d "${expanded_lib_dir}" ]]; then
  if [[ -n "${expanded_lib_dir}" ]]; then
      echo "${expanded_lib_dir} does not exist - please enter a valid directory path"
  fi
  while read -ep "Enter prefix path for the libraries (leave blank for ~/.local): " lib_dir; do
      expanded_lib_dir="${lib_dir/#\~/$HOME}"
      if [[ -d "${expanded_lib_dir}" ]]; then
           PREFIX="${expanded_lib_dir}"
           break
      elif [[ -z "${expanded_lib_dir}" ]]; then
          if [[ -d "$HOME/.local" ]]; then
              PREFIX="$HOME/.local"
              break
          else
              echo "$HOME/.local does not exist - please enter a valid directory path"
          fi
      else
           echo "${lib_dir} does not exist - please enter a valid directory path"
      fi
  done
else
  PREFIX="${expanded_lib_dir}"
fi

# Checking dependencies
python -c "import pyparsing"
if [[ $? -eq 1 ]]
then
  echo -e "${YEL}Warning${NOR}: Module pyparsing not found, installing it with pip..."
  echo "❯ python -m pip install --upgrade pyparsing --prefix=$PREFIX"
  while true; do
    read -p "Should I proceed? [y/n] " yn
    case $yn in
        [Yy]* ) python -m pip install --upgrade pyparsing --prefix="$PREFIX"; break;;
        [Nn]* ) echo "cannot continue with the installation"; exit;;
        * ) echo "answer yes or no.";;
    esac
  done
fi

python -c "import mpmath"
if [[ $? -eq 1 ]]
then
  echo -e "${YEL}Warning${NOR}: Module mpmath not found, installing it with pip..."
  echo "❯ python -m pip install --upgrade mpmath --prefix=$PREFIX"
  while true; do
    read -p "Should I proceed? [y/n] " yn
    case $yn in
        [Yy]* ) python -m pip install --upgrade mpmath --prefix="$PREFIX"; break;;
        [Nn]* ) echo "cannot continue with the installation"; exit;;
        * ) echo "answer yes or no.";;
    esac
  done
fi

python -c "import sympy"
if [[ $? -eq 1 ]]
then
  echo -e "${YEL}Warning${NOR}: Module sympy not found. Some features will be missing"
  echo "❯ python -m pip install --upgrade sympy --prefix=$PREFIX"
  while true; do
    read -p "Should I proceed? [y/n] " yn
    case $yn in
        [Yy]* ) python -m pip install --upgrade sympy --prefix="$PREFIX"; break;;
        [Nn]* ) echo "proceeding without sympy"; break;;
        * ) echo "answer yes or no.";;
    esac
  done
fi

python -c "import gmpy2"
if [[ $? -eq 1 ]]
then
  echo -e "${YEL}Warning${NOR}: Module gmpy2 not found. Installing gmpy2 will improve mpmath speed"
  echo "❯ python -m pip install --upgrade gmpy2 --prefix=$PREFIX"
  while true; do
    read -p "Should I proceed? [y/n] " yn
    case $yn in
        [Yy]* ) python -m pip install --upgrade gmpy2 --prefix="$PREFIX"; break;;
        [Nn]* ) echo "proceeding without gmpy2"; break;;
        * ) echo "answer yes or no.";;
    esac
  done
fi

# Making sure we are in the correct folder
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Making scripts executable and linking them
cd "$SCRIPT_DIR" || exit

python setup.py build

mkdir -p "$PREFIX/lib/python$PYVER/site-packages/sailboot/"
rsync -avhP "build/lib/sailboot/" "$PREFIX/lib/python$PYVER/site-packages/sailboot/"
chmod +x  "$PREFIX/lib/python$PYVER/site-packages/sailboot/launch.py"
chmod +x  "$PREFIX/lib/python$PYVER/site-packages/sailboot/monitor.py"
chmod +x  "$PREFIX/lib/python$PYVER/site-packages/sailboot/sailboot.py"
chmod +x  "$PREFIX/lib/python$PYVER/site-packages/sailboot/grid_evaluate.py"

ln -sf "$PREFIX/lib/python$PYVER/site-packages/sailboot/sailboot.py" "$BINS/sailboot"
ln -sf "$PREFIX/lib/python$PYVER/site-packages/sailboot/launch.py" "$BINS/launch"
ln -sf "$PREFIX/lib/python$PYVER/site-packages/sailboot/monitor.py" "$BINS/monitor"
ln -sf "$PREFIX/lib/python$PYVER/site-packages/sailboot/grid_evaluate.py" "$BINS/grid_evaluate"

# Check if $BINS is in $PATH by default
if [[ ! ":$PATH:" == *":$BINS:"* ]]; then
  echo -e "${YEL}Warning${NOR}: $BINS is not in your default path."
  echo "Add this line to your .$(basename $SHELL)rc file: export PATH=\$PATH:$BINS"
fi

PYPA=$PREFIX/lib/python$PYVER/site-packages
# Check if $PYPA is in $PYTHONPATH by default
if [[ ! ":$PYTHONPATH:" == *":$PYPA:"* ]]; then
  echo -e "${YEL}Warning${NOR}: $PYPA is not in your default python path."
  echo "Add this line to your .$(basename $SHELL)rc file: export PYTHONPATH=\$PYTHONPATH:$PYPA"
fi


echo -e "${GRN}Done!${NOR}"
