from json import JSONEncoder
from classes import gaps


class MyEncoder(JSONEncoder):
    """Custom class to JSON Encode my objects"""

    def default(self, o):
        if isinstance(o, gaps.Operator):
            return o.json_dump()
        else:
            return JSONEncoder.default(self, o)
