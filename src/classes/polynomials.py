from itertools import zip_longest
import mpmath
import copy


class Polynomial:
    """A univariate polynomial with arbitrary coefficients"""

    def __init__(self, coeff_list):
        self.coeffs = list(coeff_list)

        # Delete trailing zeros (if any)
        while self.coeffs and self.coeffs[-1] == 0:
            del self.coeffs[-1]

        if not self.coeffs:
            self.coeffs = [0]
            self.is_mpmath = False
        else:
            if isinstance(self.coeffs[-1], mpmath.mp.mpf):
                mpmath.mp.prec = self.coeffs[-1].context.prec
                self.is_mpmath = True
            else:
                print('Warning: the coefficients of the polynomial are not mpmath multiple precision floats')
                self.is_mpmath = False

        self.degree = len(self.coeffs)-1

    def short(self):
        """Short string representation (for debugging purposes)"""
        if len(self.coeffs) == 0:
            return '0'
        elif len(self.coeffs) == 1:
            return mpmath.mp.nstr(self.coeffs[0], 2)
        else:
            if self.degree == 1:
                pow = '*x'
            else:
                pow = f'*x^{self.degree}'
            if self.coeffs[-1] > 0:
                return mpmath.mp.nstr(self.coeffs[0], 2) + '+...+' + mpmath.mp.nstr(self.coeffs[-1], 2) + pow
            else:
                return mpmath.mp.nstr(self.coeffs[0], 2) + '+...-' + mpmath.mp.nstr(-self.coeffs[-1], 2) + pow

    def __iter__(self):
        return self.coeffs.__iter__()

    def __str__(self):
        result = ""
        start = True
        for pw, coeff in enumerate(self.coeffs):
            if pw == 0:
                if coeff == 0:
                    pass
                else:
                    result += str(coeff)
                    start = False
            elif pw == 1:
                if coeff == 0:
                    pass
                elif coeff == 1:
                    if start:
                        result += 'x'
                        start = False
                    else:
                        result += ' + x'
                        start = False
                elif coeff == -1:
                    if start:
                        result += '-x'
                        start = False
                    else:
                        result += ' - x'
                        start = False
                elif coeff < 0:
                    if start:
                        result += str(coeff) + '*x'
                        start = False
                    else:
                        result += ' - ' + str(-coeff) + '*x'
                        start = False
                else:
                    if start:
                        result += str(coeff) + '*x'
                        start = False
                    else:
                        result += ' + ' + str(coeff) + '*x'
                        start = False
            else:
                if coeff == 0:
                    pass
                elif coeff == 1:
                    if start:
                        result += f'x^{pw}'
                        start = False
                    else:
                        result += f' + x^{pw}'
                        start = False
                elif coeff == -1:
                    if start:
                        result += f'-x^{pw}'
                        start = False
                    else:
                        result += f' - x^{pw}'
                        start = False
                elif coeff < 0:
                    if start:
                        result += str(coeff) + f'*x^{pw}'
                        start = False
                    else:
                        result += ' - ' + str(-coeff) + f'*x^{pw}'
                        start = False
                else:
                    if start:
                        result += str(coeff) + f'*x^{pw}'
                        start = False
                    else:
                        result += ' + ' + str(coeff) + f'*x^{pw}'
                        start = False

        if result == '':
            result = '0'

        return result

    def __repr__(self):
        return 'Polynomial(' + str(self.coeffs) + ')'

    def __add__(self, other):
        """Sum of polynomials"""

        if isinstance(other, Polynomial):
            return Polynomial(list(
                map(sum, zip_longest(self.coeffs, other.coeffs, fillvalue=0))
            ))
        elif isinstance(other, int) or isinstance(other, mpmath.mp.mpf):
            if not self.coeffs:
                self.coeffs = [0]
            return Polynomial([other + self.coeffs[0]] + self.coeffs[1:])

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        """Difference of polynomials"""

        if isinstance(other, Polynomial):
            return Polynomial(list(
                map(lambda p: p[0] - p[1], zip_longest(self.coeffs, other.coeffs, fillvalue=0))
            ))
        elif isinstance(other, int) or isinstance(other, mpmath.mp.mpf):
            if not self.coeffs:
                self.coeffs = [0]
            return Polynomial([self.coeffs[0] - other] + self.coeffs[1:])

    def __rsub__(self, other):
        """The other way around"""

        if isinstance(other, Polynomial):
            return Polynomial(list(
                map(lambda p: p[1] - p[0], zip_longest(self.coeffs, other.coeffs, fillvalue=0))
            ))
        elif isinstance(other, int) or isinstance(other, mpmath.mp.mpf):
            if not self.coeffs:
                self.coeffs = [0]
            return Polynomial([other - self.coeffs[0]] + self.coeffs[1:])

    def __mul__(self, other):
        """Multiply polynomial by polynomial or scalar"""

        if isinstance(other, Polynomial):
            product = []
            I = self.degree
            J = other.degree
            for n in range(I + J + 1):
                term = 0
                for i in range(max(0, n-I), min(n, J)+1):
                    term += self.coeffs[n-i] * other.coeffs[i]
                product.append(term)

            return Polynomial(product)

        else:
            return Polynomial([other * c for c in self.coeffs])

    def __rmul__(self, other):
        return self.__mul__(other)

    def eval(self, val):
        """Evaluate the polynomial at x=val"""

        result = 0
        for coeff in reversed(self.coeffs):
            result = result * val + coeff

        return result

    def eval_as_poly(self, val):
        """Evaluates the polynomial at x=val but returns a constant Polynomial"""

        return Polynomial([self.eval(val)])

    @staticmethod
    def binom_int(a, b):
        """At the moment is not used, but in any case, it implements the binomial coefficient"""
        result = 1
        for i in range(b):
            result *= (a-i)/(i+1)
        return int(result)

    def compose(self, shift):
        """Evaluates the polynomial at x + shift"""

        if self.is_mpmath:
            binom = mpmath.binomial
            pwr = mpmath.mp.power
        else:
            binom = self.binom_int
            pwr = lambda x, y: x**y

        result = []
        I = self.degree
        for k in range(I+1):
            term = 0
            for l in range(I-k+1):
                term += self.coeffs[I-l] * binom(I-l, k) * pwr(shift, I-l-k)
            result.append(term)

        return Polynomial(result)

    def __copy__(self):
        new_coeffs_list = copy.copy(self.coeffs)

        return Polynomial(new_coeffs_list)

