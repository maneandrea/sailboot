import time

from mpmath import mp
import os
import sys
import json
import re
import subprocess

from classes import polynomial_matrix
from classes.polynomials import Polynomial
from utilities import mathematica
import monitor


def mpfize(lis):
    """Transforms a list of coefficients into a list generator that can be given to Polynomial"""
    return [mp.mpf(a) for a in lis]


def write_identity_block(file_descriptor, n_max, zzb_ab_deriv):
    """Writes the identity block to a file descriptor given the necessary parameters"""
    ab = (zzb_ab_deriv == 'ab')

    def deriv(n, m):
        if n == 0 and m == 0:
            return '1'
        else:
            return '0'

    file_descriptor.write('{')
    if ab:
        for m in range(n_max):
            for n in range(min(2*n_max, 2*n_max - 2*m)):
                file_descriptor.write(f'abDeriv[{n},{m}] -> ' + deriv(n, m) + ',\n ')
    else:
        for n in range(2*n_max):
            for m in range(min(n+1, 2*n_max-n)):
                file_descriptor.write(f'zzbDeriv[{n},{m}] -> ' + deriv(n, m) + ',\n ')
    file_descriptor.write('shiftedPoles -> {{}}}\n')


def auto_generate_blocks(filename, args):
    """Generates blocks automatically based on the requested filename"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # If the block was an identity block the name should match the following pattern
    match_identity = re.match(
        os.path.join(args.blocks, 'identity'),
        filename
    )

    # We get the info from the filename. We need only delta12 and delta34 really
    match_scalar = re.search(
        r'/(zzb|ab)DerivTable-d\d+-delta12-(-?\d+(\.\d+)?)-delta34-(-?\d+(\.\d+)?)'
        r'-L\d+-nmax\d+-keptPoleOrder\d+-order\d+.m'
        , filename)

    if match_identity:
        print_monitor(f'Recognized "identity" block in {filename}.')

        os.makedirs(os.path.dirname(filename), exist_ok=True)
        try:
            with open(filename, 'x') as f:
                write_identity_block(f, args.n_max, args.zzb_ab_deriv)
            print_monitor(f'Identity block generated in {os.path.dirname(filename)}')
        except FileExistsError:
            print_monitor('Warning: identity block already exists...')
        finally:
            success = os.path.isfile(filename)
            if not success:
                print_monitor(f'Error: Failed to generate identity block {filename}')
            return success

    elif match_scalar:

        # Prepare the call to scalar_blocks
        this_block_dir = os.path.dirname(filename)
        this_delta12 = match_scalar.group(2)
        this_delta34 = match_scalar.group(4)
        l_to_generate = f'0-{args.l_max}'

        if args.blocks_bin is not None:
            blocks_executable = os.path.join(args.blocks_bin, 'scalar_blocks')
        else:
            blocks_executable = 'scalar_blocks'

        to_call = [blocks_executable,
                   '--num-threads', str(args.n_tasks),
                   '-o', this_block_dir,
                   '--spin-ranges', l_to_generate,
                   '--dim', str(args.dimensions),
                   '--order', str(args.order),
                   '--poles', str(args.kept_pole_order),
                   '--output-poles',
                   '--precision', str(args.precision),
                   '--delta-12', str(this_delta12),
                   '--delta-34', str(this_delta34),
                   '--max-derivs', str(args.n_max)
                   ]
        if args.zzb_ab_deriv == 'ab':
            to_call.append('--output-ab')

        # Since we are here, we generate all the spins so that the next call will be ready to go
        # We do this by creating a lockfile so there are no issues
        lock_file_name = os.path.join(
            args.blocks,
            '_'.join([
                os.path.relpath(this_block_dir, args.blocks).replace('/', '_').replace('\\', '_'),
                f'delta12-{this_delta12}-delta34-{this_delta34}'
                f'-nmax{args.n_max}.lock'
            ])
        )
        success = False
        try:
            with open(lock_file_name, 'x') as f:
                f.write('\n')
                print_monitor('Block unavailable, generating it automatically...')
                subproc = subprocess.run(to_call, capture_output=True)
                if subproc.returncode != 0:
                    print_monitor(f'Error: scalar_blocks has produced the following error'
                                  f'\n\t"{str(subproc.stderr.decode("utf8"))}"')
                    success = False
                else:
                    print_monitor(f'Blocks generated in {os.path.dirname(filename)}')
                    success = True
        except FileExistsError:
            time_to_sleep = 2
            while os.path.isfile(lock_file_name):
                print_monitor('Another process is building blocks, waiting...')
                time.sleep(time_to_sleep)
                time_to_sleep = 10
            success = os.path.isfile(filename)
            if not success:
                print_monitor(f'Error: Failed to generate block {filename}')
        else:
            os.remove(lock_file_name)
        finally:
            return success

    else:
        print_monitor(f'Error: the block {filename} does not seem a scalar block from scalar_blocks.'
                      '\nI cannot generate it automatically')
        return False


class Block:
    """Class representing a conformal block"""

    def __init__(self, filename, args):

        self.filename = filename
        self.args = args
        try:
            self.hashesfolder = os.path.join(args.sdpfiles, 'pickles', 'bilinear_bases')
        except AttributeError:
            self.hashesfolder = None
        self.prefactor = None
        self.delta_minus_x = None
        self.spin = None
        self.series_coeffs = None

        mp.prec = self.args.precision

        # If we multiplied by v^Δφ we keep it in mind
        self.convolved_with = 0

        if os.path.isfile(filename):
            print(f'Loading block from {filename}')

            # This will contain the various entries of the block with keys matching the JSON fields in the file
            self.block_entries = {}
            self.load_block()

        else:
            if self.args.auto:

                if auto_generate_blocks(self.filename, args):
                    self.block_entries = {}
                    self.load_block()
                else:
                    sys.exit(1)

            else:
                self.print_monitor(f'Warning: The block {self.filename} does not exist. Loading a zero block in its stead')
                self.block_entries = {}

                # This will search in the folder for any file and use it as a template for building a zero block
                block_dir = os.path.dirname(filename)
                try:
                    fallback_dir = os.listdir(block_dir)
                    fallback_filename = [
                        os.path.join(block_dir, f) for f in fallback_dir if os.path.isfile(os.path.join(block_dir, f))
                    ][0]
                except FileNotFoundError:
                    self.print_monitor(f'Error: Even the folder {block_dir} does not exist')
                    sys.exit(1)
                except IndexError:
                    self.print_monitor(f'Error: The directory {block_dir} is empty')
                    sys.exit(1)
                else:
                    # Now we use this new file as a template
                    self.filename = fallback_filename
                    self.load_block()
                    first_entry = list(self.block_entries.keys())[0]
                    self.block_entries = {first_entry: self.zero_block()}

                    self.prefactor = polynomial_matrix.DampedRational(self.hashesfolder, 0, poles=[])

    def print_monitor(self, text):
        print(text)
        monitor.write_to_monitor(self.args.monitor_file, self.args.job, text)

    def load_block(self):
        """Loads a block from a file into memory"""

        with open(self.filename, 'r') as b:
            extension = os.path.splitext(self.filename)[1]
            if extension == '.json':
                block = json.load(b)
            elif extension in ['.m', '.wl']:
                block = mathematica.load_block(b, self.args)
            else:
                self.print_monitor('Error: The file is neither a json nor a mathematica file')
                sys.exit(1)

        self.delta_minus_x = block['delta_minus_x']
        self.series_coeffs = block.get('series_coeff', False)
        self.spin = block['spin']

        poles = []
        for p in block['poles']:
            if self.args.poles_in_x:
                poles.append(mp.mpf(str(p)))
            else:
                poles.append(mp.mpf(str(p)) - mp.mpf(str(self.delta_minus_x)))

        # We consider all entries that aren't description fields
        for k in block.keys():
            if k not in ['description', 'p', 'type', 'spin', 'lambda', 'order', 'kept_pole_order',
                         'irrep', 'poles', 'parameters', 'delta_minus_x', 'structure']:
                self.block_entries[k] = block[k]

        self.prefactor = polynomial_matrix.DampedRational(
            self.hashesfolder,
            self.delta_minus_x,
            poles=poles,
            # If there are no poles, we assume that the prefactor is just 1
            base=None if poles else 1,
            const=1,
            precision=self.args.precision
        )

    def convolve_block(self, delta_phi_low_prec):
        """Transforms a list of derivatives of g(u,v) into a list of
        derivatives of v**delta_phi * g(u,v). If series_coeff == True we use the series
        coefficients instead of the derivatives"""

        if self.convolved_with == delta_phi_low_prec:
            return
        else:
            if self.convolved_with != 0:
                self.print_monitor(f'Warning: I was asked to multiply {self.filename} by '
                                   f'v^{self.convolved_with} and later by v^{delta_phi_low_prec}. '
                                   f"I will do it, but it's better to use different names for different blocks.")

            if self.args.zzb_ab_deriv != 'zzb':
                self.print_monitor(f'Warning: For this routine to work the derivatives have to be of zzb type, '
                                   f'not {self.args.zzb_ab_deriv}.')

            delta_phi = mp.mpf(str(delta_phi_low_prec)) - mp.mpf(str(self.convolved_with))

            self.convolved_with = delta_phi_low_prec

        def poch(a, n):
            """poch(a,n) = a(a+1)...(a+n-1)"""
            ret = 1
            for k in range(n):
                ret *= a+k
            return ret

        def double_binomial(M, N, m, n):
            """double_binomial(M, N, m, n) = binomial(M,m)binomial(N,n) / (N! M!)"""

            # If series_coeffs is True it means that the table[m][n] = m-th, n-th derivative / m! n!
            # If series_coeffs is False it means that the table[m][n] = m-th, n-th derivative
            # series_coeffs is False in the .m and can be specified in the .json (by default False nevertheless)
            if self.series_coeffs:
                return 1/(mp.fac(N-n) * mp.fac(M - m))
            else:
                return (mp.fac(M) * mp.fac(N)) / (mp.fac(m) * mp.fac(n) * mp.fac(N - n) * mp.fac(M - m))

        def two_to_the(M, N, m, n):
            """two_to_the(M, N, m, n) = 2^(M+N-m-n-2*delta_phi) (-1)^(M+N+m+n)"""
            return mp.power(2, M + N - m - n - 2*delta_phi) * (-1)**(M + N + m + n)

        for block, table in self.block_entries.items():
            # This assumes that the entry m,n is the mth derivative w.r.t. z and the nth w.r.t zb
            newtable = []
            for M, row in enumerate(table):
                newtable.append([])
                for N, derivzb in enumerate(row):
                    running_sum = 0
                    # This conversion can be done here because the next formula uses n<=N, m<=M only
                    table[M][N] = Polynomial(mpfize(table[M][N]))
                    for m in range(M+1):
                        for n in range(N+1):
                            # Here we assume that the derivatives are symmetric, i.e. f^(m,n) == f^(n,m)
                            # We also make it work regardless whether the derivatives are m>n or m<n
                            maxmin = max if M >= N else min
                            minmax = min if M >= N else max
                            running_sum += double_binomial(M, N, m, n) * two_to_the(M, N, m, n) * \
                                           poch(delta_phi-N+n+1, N-n) * poch(delta_phi-M+m+1, M-m) * \
                                           table[maxmin(n, m)][minmax(n, m)]
                    newtable[M].append(running_sum)
            # This means that the convolved blocks will be tables of polynomials and the others will be tables of
            # coefficients lists. It does not matter because the subsequent code will handle both cases.
            self.block_entries[block] = newtable

    def zero_block(self):
        """Returns a block which is completely zero but is has the correct shape as a matrix"""

        templateblock = self.block_entries[list(self.block_entries.keys())[0]]
        zeroblock = []
        # We just simply take the first block in the list and return a matrix with the same shape but all zeros
        for M, row in enumerate(templateblock):
            zeroblock.append([])
            for col in row:
                zeroblock[M].append([0])
        return zeroblock

    def make_poly_vector(self, entry, deriv_map):
        """Builds and returns the polynomial vector of an entry given deriv_map"""

        # Vector of derivatives
        vector = []

        if entry in self.block_entries:

            # Replacement table of derivatives
            table = self.block_entries[entry]

            # deriv_map is an ordered list of derivatives [(0,0), (0,1), ...]
            for e in deriv_map:
                try:
                    vector.append(table[e[0]][e[1]])
                except IndexError:
                    self.print_monitor(f'Warning: requested entry ({e[0]},{e[1]}) not present in {entry}')
                    vector.append([0])

        else:

            # If the entry is not there we return a zero vector
            self.print_monitor(f'Warning: Entry {entry} not available, returning the zero vector')
            vector = [[0]] * len(deriv_map)

        return polynomial_matrix.PolynomialVector(vector, self.args.precision, self.delta_minus_x)
