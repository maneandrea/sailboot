from mpmath import mp
from functools import reduce


class Expr:
    """A rudimentary class for general symbolic expressions"""

    def __init__(self, *args):
        self.type = 'Expr'
        self.args = args
        self.operator = '?'
        self.eval_fn = lambda x: x
        self.default = None
        # Number of args expected, 1 for one, 2 for two, + for any > 0, * for any
        self.arity = "1"
        self.free_symb = set()
        for e in self.args:
            self.free_symb = self.free_symb.union(e.free_symb)

    def _is_expr(self):
        """Method whose presence signals that this is an Expr object"""
        pass

    def __str__(self):
        if self.type == 'Expr':
            return self.operator.join([str(a) for a in self.args])
        else:
            if len(self.args) > 1 and self.operator:
                return '(' + self.operator.join([str(a) for a in self.args]) + ')'
            else:
                return f'{self.type}(' + ','.join([x.__str__() for x in self.args]) + ')'

    def __repr__(self):
        return f'{self.type}(' + ','.join([x.__repr__() for x in self.args]) + ')'

    def __add__(self, other):
        if hasattr(other, '_is_expr'):
            return Sum(self, other)
        elif isinstance(other, mp.mpf) or isinstance(other, float) or isinstance(other, int):
            return Sum(self, Literal(other))
        else:
            raise NotImplementedError(f'Addition of {self.type} and {type(other)} not implemented')

    def __radd__(self, other):
        return self.__add__(other)

    def __mul__(self, other):
        if hasattr(other, '_is_expr'):
            return Prod(self, other)
        elif isinstance(other, mp.mpf) or isinstance(other, float) or isinstance(other, int):
            return Prod(self, Literal(other))
        else:
            raise NotImplementedError(f'Product of {self.type} and {type(other)} not implemented')

    def __rmul__(self, other):
        return self.__mul__(other)

    def free_q(self, var):
        """Returns True if the Variable object var is not found in expr"""

        return var.name not in self.free_symb

    def evaluate(self):
        """Evaluates the expression"""

        if len(self.args) == 0:
            if self.default:
                if self.arity == '*':
                    return self.default
                else:
                    raise ValueError(f'Operator {self.type} does not support zero operands')
            else:
                raise ValueError(f'Default value not available for {self.type}')

        elif len(self.args) == 1:
            if self.arity in ['*', '+', '1']:
                return self.eval_fn(self.args[0].evaluate())
            else:
                raise ValueError(f'Operator {self.type} does not support one operand')

        elif len(self.args) == 2:
            if self.arity in ['*', '+', '2']:
                try:
                    return self.eval_fn(*[a.evaluate() for a in self.args])
                except TypeError:
                    raise TypeError(f'Evaluation function undefined for expression of type {self.type}')
            else:
                raise TypeError(f'Operator {self.type} does not support two operands')

        else:
            if self.arity in ['*', '+']:
                try:
                    return reduce(self.eval_fn, [a.evaluate() for a in self.args])
                except TypeError:
                    raise TypeError(f'Evaluation function undefined for expression of type {self.type}')
            else:
                raise TypeError(f'Operator {self.type} does not support more than two operands')

    def derive(self, var):
        """Takes the derivative of the expression with respect to the variable named var"""

        if self.free_q(var):
            return Literal(0)
        else:
            # return NonImpl('Derivative of generic expression', self.free_symb)
            return None

    def derive_more(self, var, mult):
        """Takes multiple derivatives"""

        temp = self
        for a in range(mult):
            temp = temp.derive(var)

        return temp


class Sum(Expr):
    """Sum of objects"""

    def __init__(self, *args):
        super().__init__(*args)
        self.operator = '+'
        self.type = 'Sum'
        self.eval_fn = lambda x, y: x + y
        self.default = 0
        self.arity = '*'

    def derive(self, var):
        result = super().derive(var)
        if result:
            return result
        else:
            return Sum(*[e.derive(var) for e in self.args])


class Prod(Expr):
    """Product of objects"""

    def __init__(self, *args):
        super().__init__(*args)
        self.operator = '*'
        self.type = 'Prod'
        self.eval_fn = lambda x, y: x * y
        self.default = 1
        self.arity = '*'

    def derive(self, var):
        result = super().derive(var)
        if result:
            return result
        else:
            terms = []
            for pos, factor in enumerate(self.args):
                term = self.args[:pos] + (factor.derive(var),) + self.args[pos + 1:]
                terms.append(Prod(*term))
            return Sum(*terms)


class Funct(Expr):
    """Implements a unary function"""

    def __init__(self, arg):
        super().__init__(arg)
        self.operator = ''
        self.default = None
        self.arity = '1'

    def derivative(self):
        """Implements the derivative of it with respect to its argument"""
        pass

    def derive(self, var):
        # The class takes care of function composition
        result = super().derive(var)
        if result:
            return result
        else:
            gprime = self.args[0].derive(var)
            if isinstance(gprime, Literal) and gprime.val == 0:
                return gprime
            else:
                return Prod(self.derivative(), gprime)


class FunctTwo(Expr):
    """Implements a binary function"""

    def __init__(self, arg1, arg2):
        super().__init__(arg1, arg2)
        self.operator = ''
        self.default = None
        self.arity = '2'

    def derivative_first(self):
        """Implements the derivative in its first argument"""
        pass

    def derivative_last(self):
        """Implements the derivative in its second argument"""
        pass

    def derive(self, var):
        # The class takes care of function composition
        result = super().derive(var)
        if result:
            return result
        else:
            gprime = self.args[0].derive(var)
            hprime = self.args[1].derive(var)
            if isinstance(gprime, Literal) and gprime.val == 0:
                if isinstance(hprime, Literal) and hprime.val == 0:
                    return hprime
                else:
                    return Prod(self.derivative_last(), hprime)
            elif isinstance(hprime, Literal) and hprime.val == 0:
                return Prod(self.derivative_first(), gprime)
            else:
                return Sum(
                    Prod(self.derivative_first(), gprime),
                    Prod(self.derivative_last(), hprime),
                )


class Pow(FunctTwo):
    """Power of base to the exponent"""

    def __init__(self, base, expo):
        super().__init__(base, expo)
        self.operator = '^'
        self.type = 'Pow'
        self.eval_fn = mp.power

    def derivative_first(self):
        base, expo = self.args
        return Prod(expo, Pow(base, Sum(expo, Literal(-1))))

    def derivative_last(self):
        base, expo = self.args
        return Prod(Pow(base, expo), Log(base))


class Log(Funct):
    """Natural logarithm"""

    def __init__(self, arg):
        super().__init__(arg)
        self.type = 'log'
        self.eval_fn = mp.log

    def derivative(self):
        return Pow(self.args[0], Literal(-1))


class Exp(Funct):
    """Exponential function"""

    def __init__(self, arg):
        super().__init__(arg)
        self.type = 'exp'
        self.eval_fn = mp.exp

    def derivative(self):
        return Exp(self.args[0])


class Gamma(Funct):
    """Gamma function"""

    def __init__(self, arg):
        super().__init__(arg)
        self.type = 'Γ'
        self.eval_fn = mp.gamma

    def derivative(self):
        arg = self.args[0]
        return Prod(Gamma(arg), PolyGamma(Literal(0), arg))


class Divide(FunctTwo):
    """Division"""

    def __init__(self, dividend, divisor):
        super().__init__(dividend, divisor)
        self.type = 'Divide'
        self.eval_fn = lambda x, y: x / y
        self.operator = '/'

    def derivative_first(self):
        return Divide(Literal(1), self.args[1])

    def derivative_last(self):
        num = self.args[0]
        den = self.args[1]
        return Prod(Literal(-1), num, Pow(den, Literal(-2)))


class GammaInc(FunctTwo):
    """Incomplete gamma function Γinc(z,a) = \\int_a^\\infty t^{z-1} e^{-t}"""

    def __init__(self, zeta, bottom):
        super().__init__(zeta, bottom)
        self.type = 'Γinc'
        self.eval_fn = mp.gammainc

    def derivative_first(self):
        return NonImpl('Derivative of the PolyGamma with respect to its first argument', self.free_symb)

    def derivative_last(self):
        zeta = self.args[0]
        bottom = self.args[1]
        return Prod(Literal(-1), Exp(Prod(Literal(-1), bottom)), Pow(bottom, Sum(zeta, Literal(-1))))


class PolyGamma(FunctTwo):
    """PolyGamma ψ^{(n)}(z)"""

    def __init__(self, n, zeta):
        super().__init__(n, zeta)
        self.type = 'ψ'
        self.eval_fn = mp.polygamma

    def derivative_first(self):
        return NonImpl('Derivative of the PolyGamma with respect to its first argument', self.free_symb)

    def derivative_last(self):
        n, zeta = self.args
        return PolyGamma(Sum(n, Literal(1)), zeta)


class Literal(Expr):
    """Literal (number)"""

    def __init__(self, val):
        self.operator = ''
        self.type = 'Literal'
        self.args = []
        self.val = val
        self.arity = '1'
        self.free_symb = set()

    def __str__(self):
        return str(self.val)

    def __repr__(self):
        return f'Literal({self.val})'

    def free_q(self, var):
        return True

    def evaluate(self):
        return self.val

    def derive(self, var):
        return Literal(0)


class NonImpl(Expr):
    """Placeholder for a non implemented derivative"""

    def __init__(self, msg, free_symbs):
        self.operator = ''
        self.type = 'NonImpl'
        self.args = []
        self.msg = msg
        self.arity = '0'
        self.free_symb = free_symbs

    def __str__(self):
        return self.msg

    def __repr__(self):
        return f'NonImpl({self.msg}, ' + ','.join(self.free_symb) + ')'

    def free_q(self, var):
        return True

    def evaluate(self):
        raise ValueError('NonImplemented has no associated value for evaluation')

    def derive(self, var):
        if var.name in self.free_symb:
            return self
        else:
            return Literal(0)


class Variable(Expr):
    """A variable"""

    def __init__(self, name, var_dict=None):
        self.operator = ''
        self.name = name
        self.type = 'Variable'
        self.args = []
        self.arity = ''
        # Replacement rules for this variable are stored into a dictionary passed to the instance
        if var_dict:
            self.var_dict = var_dict
        else:
            self.var_dict = {}
        self.free_symb = {self.name}

    def __str__(self):
        return self.name

    def __repr__(self):
        return f'Variable({self.name})'

    def free_q(self, var):
        return var.name != self.name

    def evaluate(self):
        if self.name in self.var_dict:
            return self.var_dict[self.name]
        else:
            raise ValueError(f'Variable {self.name} has no associated value')

    def derive(self, var):
        # Obviously this means that the names of the variables need to be unique
        if var.name == self.name:
            return Literal(1)
        else:
            return Literal(0)
