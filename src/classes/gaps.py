import re
from mpmath import mp

from utilities import params


def n_to_str(number):
    return mp.nstr(number, params.RESULT_PRECISION)


class Operator:
    """Defines a wrapper for the gap specifications such as (irrep, L, "gap", Delta, angle)"""

    def __init__(self, string, prec):

        # The field "irrep" must correspond to the name given to the vector in the crossing equations.
        # If it is not provided, python will assign automatically "1", "2", ...
        # L is the spin or a spin list like:
        #   "Leven"  = all evens
        #   "Lodd"   = all odds
        #   "Leven+" = all evens > 0
        #   "Lodd+"  = all odds > 1
        #   "Lall"   = all L
        #   "Lall+"  = all L > 0
        #   <int>    = that particular value of the spin
        # type is the type of specification (gap, twistGap, isolatedValue, isolatedTwist, skip)
        # Delta is there is one is fixing the gap and is not there if one is telling it who to bisect
        # angle is also optional

        self.prec = prec
        mp.prec = prec

        # Puts quotes to avoid issues with eval
        op_string = re.sub(
            '(?<!")(gap|twistGap|isolatedValue|isolatedTwist|skip|'
            r'Leven\+|Lall\+|Lodd\+|Leven|Lodd|Lall|\$\d+)(?!")', r'"\g<1>"', string)

        try:
            op_list = eval(op_string)
        except (SyntaxError, NameError) as e:
            raise OperatorError(f'operator specification {op_string} invalid: ' + str(e))

        self.irrep = None
        self.spin = None
        self.type = None
        self.gap = None
        self.angle = None

        self.placeholder_count = 0

        # Puts the content in the class
        if len(op_list) >= 2:
            self.irrep = op_list[0]
            self.spin = op_list[1]
        if len(op_list) >= 3:
            self.type = op_list[2]
        if len(op_list) >= 4:
            if isinstance(op_list[3], str):
                placeholder_match = re.match(r'\$(\d+)', op_list[3])
            else:
                placeholder_match = None
            if placeholder_match:
                self.gap = Placeholder(placeholder_match.group(1))
                self.placeholder_count += 1
            else:
                self.gap = self.make_precise(op_list[3])
        if len(op_list) >= 5:
            if isinstance(op_list[4], str):
                placeholder_match = re.match(r'\$(\d+)', op_list[4])
            else:
                placeholder_match = None
            if placeholder_match:
                self.angle = Placeholder(placeholder_match.group(1))
                self.placeholder_count += 1
            else:
                self.angle = self.make_precise(op_list[4])

        self.length = len(op_list)

    def make_precise(self, arg):
        mp.prec = self.prec
        if isinstance(arg, str):
            return mp.mpf(arg)
        elif isinstance(arg, list) or isinstance(arg, tuple):
            return [self.make_precise(val) for val in arg]
        else:
            return arg

    def json_dump(self):
        """Defines how to serialize this object with JSON"""

        self.length = len([a for a in [self.irrep, self.spin, self.type, self.gap, self.angle] if a is not None])

        if self.length == 2:
            return self.irrep, self.spin
        elif self.length == 3:
            return self.irrep, self.spin, self.type
        elif self.length == 4:
            safe_gap = n_to_str(self.gap)
            return self.irrep, self.spin, self.type, safe_gap
        elif self.length >= 5:
            safe_gap = n_to_str(self.gap)
            if isinstance(self.angle, tuple) or isinstance(self.angle, list):
                safe_angle = [n_to_str(a) for a in self.angle]
            else:
                safe_angle = [n_to_str(self.angle)]
            return self.irrep, self.spin, self.type, safe_gap, safe_angle

    def replace_placeholders(self, *values):
        """Replaces all placeholders with the actual values"""

        if isinstance(self.gap, Placeholder):
            self.gap = self.gap.replace(values)
        if isinstance(self.angle, Placeholder):
            self.angle = self.gap.replace(values)

    def __repr__(self):
        """This is the representation of the object"""

        self.length = len([a for a in [self.irrep, self.spin, self.type, self.gap, self.angle] if a is not None])

        if self.length == 2:
            return f'Operator(\'({self.irrep}, {self.spin})\')'
        elif self.length == 3:
            return f'Operator(\'({self.irrep}, {self.spin}, "{self.type}")\')'
        elif self.length == 4:
            safe_gap = str(self.gap)
            return f'Operator(\'({self.irrep}, {self.spin}, "{self.type}", ' + '{:s})\')'.format(safe_gap)
        elif self.length >= 5:
            safe_gap = str(self.gap)
            safe_angle = str(self.angle)
            return f'Operator(\'({self.irrep}, {self.spin}, "{self.type}", ' + '{0:s}, {1:s})\')'.format(safe_gap, safe_angle)

    def __str__(self):
        """This is called everytime we call str(object)"""

        self.length = len([a for a in [self.irrep, self.spin, self.type, self.gap, self.angle] if a is not None])

        if self.length == 2:
            return f'irrep={self.irrep}, L={self.spin}'
        elif self.length == 3:
            return f'irrep={self.irrep}, L={self.spin}, type="{self.type}"'
        elif self.length == 4:
            safe_gap = float(self.angle)
            return f'irrep={self.irrep}, L={self.spin}, {self.type}=' + '{:.9f}'.format(safe_gap)
        elif self.length >= 5:
            safe_gap = float(self.gap)
            safe_angle = str(self.angle)
            return f'irrep={self.irrep}, L={self.spin}, {self.type}=' + '{0:.9f}, angle={1:s}'.format(safe_gap, safe_angle)

    def __copy__(self):
        """Creates a copy of this instance"""

        new_op = Operator('()', self.prec)

        new_op.irrep = self.irrep
        new_op.spin = self.spin
        new_op.type = self.type
        new_op.gap = self.gap
        new_op.angle = self.angle
        new_op.length = self.length

        return new_op


class Placeholder:
    """Class for holding a placeholder that will be substituted by actual values in any given run"""
    def __init__(self, order):
        self.order = int(order) - 1

    def replace(self, *values):
        try:
            return values[self.order]
        except IndexError:
            raise OperatorError(f'not enough values to replace: I was ${self.order} and I got {len(values)} values')

    def __str__(self):
        return '$' + str(self.order+1)

    def __format__(self, format_spec):
        return '$' + str(self.order + 1)


class OperatorError(Exception):
    """Exception for failure in parsing an operator specification"""
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message
