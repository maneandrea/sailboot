from mpmath import mp, ceil, cholesky, matrix, nstr
from collections import Counter
from hashlib import md5
import os
import pickle
import copy

from classes.polynomials import Polynomial
from classes.expressions import *


def rho(prec):
    mp.prec = prec
    return 3 - 2 * mp.sqrt(2)


def four_rho(prec):
    mp.prec = prec
    return 12 - 8 * mp.sqrt(2)


def mpfize(lis):
    """Transforms a list of coefficients into a list generator that can be given to Polynomial"""
    return (mp.mpf(str(a)) for a in lis)


class Matrix:
    """Interface for doing __add__ and __rmul__ component-wise"""
    def __init__(self, matrix):
        self.matrix = matrix

    def __add__(self, other):
        to_return = []
        for i, row in enumerate(self.matrix):
            to_return.append([])
            for j, col in enumerate(row):
                to_return[i].append(self.matrix[i][j] + other.matrix[i][j])

        return Matrix(to_return)

    def __rmul__(self, other):
        to_return = []
        for i, row in enumerate(self.matrix):
            to_return.append([])
            for j, col in enumerate(row):
                to_return[i].append(other * self.matrix[i][j])

        return Matrix(to_return)

    def __truediv__(self, other):
        return (1/other) * self

    def __sub__(self, other):
        to_return = []
        for i, row in enumerate(self.matrix):
            to_return.append([])
            for j, col in enumerate(row):
                to_return[i].append(self.matrix[i][j] - other.matrix[i][j])

        return Matrix(to_return)

    def concatenate(self, other):
        """Given two matrices it concatenates them entry by entry"""

        if isinstance(self.matrix[0][0], list):
            combine_funct = lambda a, b: a + b
        else:
            combine_funct = lambda a, b: a.concatenate(b)

        new_matrix = []
        for i, row in enumerate(self.matrix):
            new_matrix.append([])
            for j, col in enumerate(row):
                new_matrix[i].append([])
                new_matrix[i][j] = combine_funct(col, other.matrix[i][j])

        return Matrix(new_matrix)


class DiscreteDeltaCollection:
    """List of VectorMatrices with their delta_list"""
    def __init__(self, vector_list, delta_list, delta_minus_x):

        # vector_list is a list of matrices, each entry being associated to the corresponding Delta in delta_list
        self.delta_list = delta_list
        self.vector_list = vector_list
        self.delta_minus_x = delta_minus_x
        self.origin = delta_minus_x
        self.prefactor = ConstantOne()
        self.max_exponent = 0  # for compatibility with the PolynomialMatrixWithPrefactor class
        self.skip = False

    @staticmethod
    def get_Laguerre_sample(n):
        """Gives the rescaled Laguerre sample points of order n"""

        ret = []
        for k in range(n):
            ret.append(mp.pi ** 2 * (-1 + 4 * k) ** 2 / (-64 * n * mp.log(rho(mp.prec))))
        return ret

    def check_grid(self, other):
        """Checks if delta_minus_x and delta_list agree and return the common values,
        except when one of the two is constant (a constant is given by an empty grid)"""

        if not self.delta_list:
            return other.delta_list, other.delta_minus_x
        elif not other.delta_list:
            return self.delta_list, self.delta_minus_x
        else:
            if self.delta_minus_x != other.delta_minus_x:
                raise ValueError('trying to add vectors with different delta_minus_x: '
                                 f'{self.delta_minus_x} and {other.delta_minus_x}')
            else:
                return self.delta_list, self.delta_minus_x

    @staticmethod
    def magical_zip(one, other):
        """Zips over two vector_lists, except when one is constant,
        then loops over one and returns the same thing over and over for the other"""
        def yield_one():
            for e in one.vector_list:
                yield e, other.vector_list[0]

        def yield_other():
            for e in other.vector_list:
                yield one.vector_list[0], e

        if one.delta_list and other.delta_list:
            # Both non-constant
            return zip(one.vector_list, other.vector_list)
        elif one.delta_list and not other.delta_list:
            # Other is constant
            return yield_one()
        elif not one.delta_list and other.delta_list:
            # One is constant
            return yield_other()
        else:
            # Both constant
            return [(one.vector_list[0], other.vector_list[0])]

    def __add__(self, other):
        """Adding two vector matrix instances entry-wise"""

        if isinstance(other, DiscreteDeltaCollection):
            new_delta_list, new_delta_minus_x = self.check_grid(other)

            return DiscreteDeltaCollection(
                [a + b for a, b in DiscreteDeltaCollection.magical_zip(self, other)],
                new_delta_list,
                new_delta_minus_x
            )

        else:

            return DiscreteDeltaCollection(
                [a + other for a in self.vector_list],
                self.delta_list,
                self.delta_minus_x
            )

    def __rmul__(self, other):
        """Multiplying a polynomial matrix by a scalar"""

        return DiscreteDeltaCollection(
            [other * a for a in self.vector_list],
            self.delta_list,
            self.delta_minus_x
        )

    def concatenate(self, other):
        """Given two polynomial matrices it concatenates them"""

        new_delta_list, new_delta_minus_x = self.check_grid(other)

        return DiscreteDeltaCollection(
            [a.concatenate(b) for a, b in DiscreteDeltaCollection.magical_zip(self, other)],
            new_delta_list,
            new_delta_minus_x
        )

    def __copy__(self):
        """Creates a copy of this instance"""

        newm = copy.deepcopy(self.vector_list)
        newlist = self.delta_list.copy()

        return DiscreteDeltaCollection(newm, newlist, self.delta_minus_x)

    def binary_search(self, target):
        """Does a binary search of a value of delta inside delta_list returning the two closest positions"""
        upper = len(self.delta_list) - 1
        lower = 0

        if upper <= 0:
            return 0, 0

        if mp.mpf(self.delta_list[upper]) < target:
            raise ValueError(f'the requested dimension ({str(target)}) '
                             f'is larger than the upper bound of the grid ({str(self.delta_list[upper])})')

        if mp.mpf(self.delta_list[lower]) > target:
            raise ValueError(f'the requested dimension ({str(target)}) '
                             f'is smaller than the lower bound of the grid ({str(self.delta_list[lower])})')

        while upper - lower > 1:
            middle = (upper + lower) // 2
            if mp.mpf(self.delta_list[middle]) <= target:
                lower = middle
            else:
                upper = middle

        return lower, upper

    def evaluate(self, delta_low_prec):
        """Evaluates the DiscreteDeltaCollection in delta by doing a search in the closest values in delta_list"""

        if len(self.delta_list) == 1:
            raise ValueError(f'the grid is too short (<2)')
        elif len(self.delta_list) == 0:
            try:
                return self.vector_list[0]
            except IndexError:
                raise ValueError(f'the constant DiscreteDeltaCollection {self.vector_list} has no entries')

        ex = mp.mpf(str(delta_low_prec)) - self.delta_minus_x

        # We do a binary search and return a linear interpolation of the closest two values
        lower, upper = self.binary_search(ex)

        try:
            high = self.vector_list[upper]
            low = self.vector_list[lower]
            a = mp.mpf(self.delta_list[lower])
            b = mp.mpf(self.delta_list[upper])
            alpha = (ex - a) / (b - a)

            return alpha * high + (1 - alpha) * low
        except IndexError:
            raise ValueError(f'there are only {len(self.vector_list)} vectors for a grid of size {len(self.delta_list)}')

    def shift(self, gap_low_prec):
        """Moves the origin of the delta_list *in place*"""

        # The way we do the shifts here is to just move the origin of the list which is stored in self.origin
        # Then the actual (shifted) vector is output by self.shifted_vector_list()
        # whereas in vector_list we always have the full vector from Δ=delta_minus_x to the max

        gap = mp.mpf(str(gap_low_prec))
        if gap != self.origin:
            self.origin = gap

            # Return True if we did something
            return True
        else:
            return False

    def shifted_vector_list(self):
        """Returns the vector list only starting from origin. The first element is obtained as a linear interpolation
        with origin and the closes grid values
        """

        if len(self.delta_list) == 0 or self.origin == self.delta_minus_x:
            return self.vector_list

        xgap = self.origin - self.delta_minus_x

        # We do a binary search and replace the value below gap with a linear interpolation with it and the next
        lower, upper = self.binary_search(xgap)

        try:
            high = self.vector_list[upper]
            low = self.vector_list[lower]
            a = mp.mpf(self.delta_list[lower])
            b = mp.mpf(self.delta_list[upper])
            alpha = (xgap - a) / (b - a)

            return [alpha * high + (1 - alpha) * low] + self.vector_list[upper:]

        except IndexError:
            raise ValueError(f'there are only {len(self.vector_list)} vectors for a grid of size {len(self.delta_list)}')


class PolynomialVector:
    """Class for handling vectors of polynomials"""

    def __init__(self, vector, precision=650, delta_minus_x=0, convert_to_poly=True):

        self.precision = precision
        self.delta_minus_x = delta_minus_x
        mp.prec = precision

        self.polyvector = vector

        if convert_to_poly:
            self.is_poly = True
            for i, poly in enumerate(self.polyvector):
                if hasattr(poly, '__iter__'):
                    self.polyvector[i] = Polynomial(mpfize(poly))
                elif not isinstance(poly, Polynomial):
                    raise TypeError(f'Polynomial type {type(poly)} is neither a list or a Polynomial')
        else:
            self.is_poly = False

    def __add__(self, other):
        """Adding two polynomial vector instances entry-wise"""

        new_polyvector = []
        for i, poly in enumerate(self.polyvector):
            new_polyvector.append(poly + other.polyvector[i])

        return PolynomialVector(new_polyvector,
                                precision=self.precision,
                                delta_minus_x=self.delta_minus_x,
                                convert_to_poly=self.is_poly)

    def __sub__(self, other):
        """Adding two polynomial vector instances entry-wise"""

        new_polyvector = []
        for i, poly in enumerate(self.polyvector):
            new_polyvector.append(poly - other.polyvector[i])

        return PolynomialVector(new_polyvector,
                                precision=self.precision,
                                delta_minus_x=self.delta_minus_x,
                                convert_to_poly=self.is_poly)

    def __rmul__(self, other):
        """Multiplying a polynomial vector by a scalar"""

        new_polyvector = []
        for poly in self.polyvector:
            new_polyvector.append(poly * other)

        return PolynomialVector(new_polyvector,
                                precision=self.precision,
                                delta_minus_x=self.delta_minus_x,
                                convert_to_poly=self.is_poly)

    def concatenate(self, other):
        """Given two polynomial vectors it concatenates them"""

        return PolynomialVector(self.polyvector + other.polyvector,
                                precision=self.precision,
                                delta_minus_x=self.delta_minus_x,
                                convert_to_poly=self.is_poly)

    def shift(self, gap_low_prec):
        """Implements polyvector /. x -> x + gap - delta_minus_x *in place*"""

        gap = mp.mpf(str(gap_low_prec))
        if gap != self.delta_minus_x:
            for k, poly in enumerate(self.polyvector):
                self.polyvector[k] = poly.compose(gap - self.delta_minus_x)

            # Update delta_minus_x so that further shifts will be correct
            self.delta_minus_x = gap

            # Return True if we did something
            return True
        else:
            return False

    def evaluate(self, delta_low_prec, as_poly=False):
        """Implements polyvector /. x -> delta - delta_minus_x and returns an instance of PolyVector
        with numerical (i.e. non-polynomial) entries"""

        delta = mp.mpf(str(delta_low_prec))
        ret = []
        if as_poly:
            for poly in self.polyvector:
                ret.append(poly.eval_as_poly(delta - self.delta_minus_x))
        else:
            for poly in self.polyvector:
                ret.append(mp.mpf(poly.eval(delta - self.delta_minus_x)))

        return PolynomialVector(ret, precision=self.precision, delta_minus_x=self.delta_minus_x, convert_to_poly=False)


class PolynomialMatrixWithPrefactor:
    """Class for the polynomial matrix together with its prefactor DampedRational"""

    def __init__(self, prefactor, polymatrix, delta_minus_x=0, precision=650, max_exponent=None):

        if not isinstance(prefactor, DampedRational):
            raise TypeError('The prefactor must be a DampedRational type')
        self.prefactor = prefactor
        self.precision = precision
        self.skip = False

        mp.prec = precision

        # This polynomial matrix is of the type:
        # [ [ [poly11_1, ... , poly11_E], ... , [poly1n_1, ... , poly1n_E] ],
        #   ...
        #   [ [polyn1_1, ... , polyn1_E], ... , [polynn_1, ... , polynn_E] ] ]
        #
        # Where E are the number of crossing equations times the number of derivatives (~ Λ^2),
        # n is the size of the matrix (typically 1 or 2) and polyij_k(x) are either
        # polynomials or lists of coefficients. Alternatively, an entry could be a PolynomialVector.
        self.polymatrix = polymatrix

        # We now iterate through all entries, unless max_exponent is set, in which case we skip it
        # Setting max_exponent can also be used to have mp.mpf numbers instead of polynomials as entries
        if max_exponent is None:
            self.max_exponent = 0
            for i, row in enumerate(self.polymatrix):
                for j, col in enumerate(row):

                    # The input could be already a PolynomialVector
                    if isinstance(col, PolynomialVector):
                        self.polymatrix[i][j] = col.polyvector
                        for poly in col.polyvector:
                            self.max_exponent = max(self.max_exponent, poly.degree)

                    # Or a list of Polynomial or a list of coefficients
                    else:
                        for k, poly in enumerate(col):
                            # If the entries passed to polymatrix are lists of coefficients rather
                            # than polynomials we convert them
                            if hasattr(poly, '__iter__'):
                                self.polymatrix[i][j][k] = Polynomial(mpfize(poly))
                            elif not isinstance(poly, Polynomial):
                                raise TypeError(f'Polynomial type {type(poly)} is neither a list or a Polynomial')
                            # This is needed to obtain the degree
                            self.max_exponent = max(self.max_exponent, self.polymatrix[i][j][k].degree)
        else:
            self.max_exponent = max_exponent

        # This is used to turn gaps in Delta to gaps in x
        self.delta_minus_x = mp.mpf(str(delta_minus_x))

    def shift(self, gap_low_prec):
        """Implements polymatrix /. x -> x + gap - delta_minus_x *in place*, returns True if it does something"""

        gap = mp.mpf(str(gap_low_prec))

        if self.delta_minus_x != gap:
            self.prefactor.shift(gap - self.delta_minus_x)
            for i, row in enumerate(self.polymatrix):
                for j, col in enumerate(row):
                    for k, poly in enumerate(col):
                        self.polymatrix[i][j][k] = poly.compose(gap - self.delta_minus_x)

            self.delta_minus_x = gap
            self.prefactor.delta_minus_x = gap

            # Return True if we did something
            return True
        else:
            return False

    def evaluate(self, delta_low_prec):
        """Returns polymatrix /. x -> delta - delta_minus_x as an instance of
        PolynomialMatrixWithPrefactor with numerical (i.e. non-polynomial) entries"""

        delta = mp.mpf(str(delta_low_prec))
        new_prefactor = DampedRational(
            self.prefactor.hashesfolder,
            self.delta_minus_x,
            self.prefactor.evaluate(delta),
            [],
            1,
            self.prefactor.precision)

        new_polymatrix = []
        for i, row in enumerate(self.polymatrix):
            new_polymatrix.append([])
            for j, col in enumerate(row):
                new_polymatrix[i].append([])
                for poly in col:
                    new_polymatrix[i][j].append(mp.mpf(poly.eval(delta - self.delta_minus_x)))

        new_max_exponent = 0

        return PolynomialMatrixWithPrefactor(
            new_prefactor, new_polymatrix, self.delta_minus_x, self.precision, new_max_exponent
        )

    def full_evaluate(self, delta_low_prec):
        """Evaluates the polynomial matrix, prefactor included, returning a matrix of lists"""

        delta = mp.mpf(str(delta_low_prec))
        prefactor = self.prefactor.evaluate(delta_low_prec)

        new_polymatrix = []
        for i, row in enumerate(self.polymatrix):
            new_polymatrix.append([])
            for j, col in enumerate(row):
                new_polymatrix[i].append([])
                for poly in col:
                    new_polymatrix[i][j].append(prefactor * mp.mpf(poly.eval(delta - self.delta_minus_x)))

        return new_polymatrix

    @staticmethod
    def get_Laguerre_sample(n):
        """Gives the rescaled Laguerre sample points of order n"""

        ret = []
        for k in range(n):
            ret.append(mp.pi ** 2 * (-1 + 4 * k) ** 2 / (-64 * n * mp.log(rho(mp.prec))))
        return ret

    def __add__(self, other):
        """Adding two polynomial matrix instances entry-wise assuming the prefactor is the same"""

        new_polymatrix = []
        for i, row in enumerate(self.polymatrix):
            new_polymatrix.append([])
            for j, col in enumerate(row):
                new_polymatrix[i].append([])
                for k, poly in enumerate(col):
                    new_polymatrix[i][j].append(poly + other.polymatrix[i][j][k])

        max_exp = max(self.max_exponent, other.max_exponent)

        return PolynomialMatrixWithPrefactor(self.prefactor, new_polymatrix,
                                             self.delta_minus_x, self.precision, max_exp)

    def __rmul__(self, other):
        """Multiplying a polynomial matrix by a scalar"""

        new_polymatrix = []
        for i, row in enumerate(self.polymatrix):
            new_polymatrix.append([])
            for j, col in enumerate(row):
                new_polymatrix[i].append([])
                for poly in col:
                    new_polymatrix[i][j].append(poly * other)

        return PolynomialMatrixWithPrefactor(self.prefactor, new_polymatrix,
                                             self.delta_minus_x, self.precision, self.max_exponent)

    def concatenate(self, other):
        """Given two polynomial matrices it concatenates them assuming the prefactor is the same"""

        new_polymatrix = []
        for i, row in enumerate(self.polymatrix):
            new_polymatrix.append([])
            for j, col in enumerate(row):
                new_polymatrix[i].append([])
                new_polymatrix[i][j] = col + other.polymatrix[i][j]

        max_exp = max(self.max_exponent, other.max_exponent)

        return PolynomialMatrixWithPrefactor(self.prefactor, new_polymatrix,
                                             self.delta_minus_x, self.precision, max_exp)

    def __copy__(self):
        """Creates a copy of this instance"""

        newpref = copy.copy(self.prefactor)
        newpolym = copy.deepcopy(self.polymatrix)
        newdmx = self.delta_minus_x
        newprec = self.precision
        newmexp = self.max_exponent

        return PolynomialMatrixWithPrefactor(newpref, newpolym, newdmx, newprec, newmexp)

    def __str__(self):
        """String representation (for debugging purposes)"""

        result = 'PolynomialMatrixWithPrefactor(\n\tprefactor = ' + str(self.prefactor) + ',\n\tpolymatrix = {'
        for row in self.polymatrix:
            result += '\n\t\t{'
            for col in row:
                result += '\n\t\t\t{'
                if isinstance(col, PolynomialVector):
                    vector = col.polyvector
                else:
                    vector = col
                result += ',\n\t\t\t\t'.join([a.short() if isinstance(a, Polynomial) else mp.nstr(a, 2) for a in vector])
                result += '\n\t\t\t},'
            result += '\n\t\t},'
        result += '\n\t}\n)'

        return result


class DampedRational:
    """
    Class for the rational prefactor damped by an exponential that multiplies the blocks
    DampedRational(const=c, poles=[p1, p2, ...], base=b) stands for c b^Δ / ((x-p1)(x-p2)...)
    """

    def __init__(self, hashesfolder, delta_minus_x, const=1, poles=None, base=None, precision=650):

        mp.prec = precision
        self.const = const
        self.precision = precision
        self.delta_minus_x = mp.mpf(str(delta_minus_x))
        if poles is None:
            self.poles = []
        else:
            self.poles = poles
        if base is None:
            self.base = four_rho(precision)
        elif isinstance(base, float) or isinstance(base, int):
            self.base = mp.mpf(str(base))
        else:
            self.base = base

        self.hashesfolder = hashesfolder

    def shift(self, a_low_prec):
        """Implements the shift : x -> x + a *in place*"""

        # WARNING: After using this method one needs to change self.delta_minus_x appropriately!
        # The method self.evaluate relies on this.

        a = mp.mpf(str(a_low_prec))
        # Careful: we don't need to shift the constant because we also update self.delta_minus_x.
        # So it's always true that the exponent is b^(x + delta_minus_x)
        self.poles = [p - a for p in self.poles]

    def evaluate(self, a_low_prec):
        """Evaluates the damped rational in Δ=a"""

        delta = mp.mpf(str(a_low_prec))
        xval = delta - self.delta_minus_x
        const_base = mp.power(self.base, delta) * self.const
        prod_poles = 1
        for p in self.poles:
            prod_poles = prod_poles * (xval - p)

        return const_base / prod_poles

    def evaluate_x(self, a_low_prec):
        """Evaluates the damped rational in x=a"""

        return self.evaluate(mp.mpf(str(a_low_prec)) + self.delta_minus_x)

    def remove_zero_poles(self, threshold='1e-10'):
        """Removes poles close to zero"""
        self.poles = [p for p in self.poles if abs(p) < mp.mpf(threshold)]

    def bilinear_form(self, m):
        """Computes Integral[x^m self(x), {x,0,inf}]"""

        negative_poles = [p for p in self.poles if p < 0]

        if len(negative_poles) == 0:
            # No poles
            return self.const * mp.fac(m) * mp.power(-mp.log(self.base), -1 - m)

        elif len(negative_poles) == len(set(negative_poles)):
            # Only single poles
            s = 0
            for p in negative_poles:
                one_over_prod = 1
                for q in negative_poles:
                    if q != p:
                        one_over_prod = one_over_prod / (p - q)
                s += mp.power(-p, m) * mp.power(self.base, p) * mp.fac(m) * mp.gammainc(-m, a=p * mp.log(
                    self.base)) * mp.mpf(str(one_over_prod))

            return self.const * s
        else:
            # Multiple poles as well
            tallied = Counter(negative_poles)
            ltot = len(negative_poles)

            s = 0
            for p, mult in tallied.items():
                derivative = self.compute_deta(tallied, p, mult, m)
                # if p was integer the first replacement is redundant, but it doesn't matter
                s += derivative.evaluate() / mp.gamma(mult)

            return self.const * mp.fac(m) * (-1) ** (ltot + 1) * s

    def __copy__(self):
        """Creates a copy of this instance"""

        newhash = self.hashesfolder
        newdmx = self.delta_minus_x
        newconst = self.const
        newpoles = self.poles.copy()
        newbase = self.base
        newprec = self.precision

        return DampedRational(newhash, newdmx, newconst, newpoles, newbase, newprec)

    ##################################################
    # Methods for getting the orthogonal polynomials #
    ##################################################

    def compute_deta(self, tallied, p, mult, mm):
        """Computes residue that goes into the integral for the multiple pole case above"""

        repl = {'eta': -mp.mpf(str(p)), 'b': self.base}
        eta = Variable('eta', var_dict=repl)
        b = Variable('b', var_dict=repl)

        expr = Literal(1)
        for q, nu in tallied.items():
            if q != p:
                expr = Divide(expr, Pow(Sum(Literal(q), eta), Literal(nu)))

        # Note: we made the base into a symbol to avoid converting it into a Float.
        # Later b will be replaced with self.base
        expr = Prod(
            expr,
            Pow(eta, Literal(mm)),
            Exp(Prod(Literal(-1), eta, Log(b))),
            GammaInc(Literal(-mm), Prod(Literal(-1), eta, Log(b)))
        )

        return expr.derive_more(eta, mult - 1)

    @staticmethod
    def anti_band_matrix(bs):
        """Matrix with constant antidiagonals given by the elements of bs"""

        n = int(ceil(len(bs) / 2))
        ret = matrix(n)
        for i in range(n):
            for j in range(n):
                ret[i, j] = bs[i + j]

        return ret

    def get_polynomials(self, order, as_poly=False):
        """
        self.get_polynomials(n) returns a set of polynomials with degree 0
        through n which are orthogonal with respect to the measure DampedRational(x) dx
        """

        def cholesky_inverse(mat):
            """Does Choleski decomposition and takes inverse"""
            return cholesky(mat) ** -1

        this_header = {'order': order, 'poles': self.poles}

        # If there is a file whose name matches the hash, we load it instead (only if as_poly is false)
        saved_file = os.path.join(self.hashesfolder, self.md5hash(order) + '.pickle')
        if os.path.isfile(saved_file) and not as_poly:
            with open(saved_file, 'rb') as f:
                header, bilinear_bases = pickle.load(f)
                if header == this_header:
                    return bilinear_bases
                else:
                    print('Warning: Found hash collision for the bilinear bases pickles')

        if order == 0 and len(self.poles) == 0:
            return [[1 / mp.sqrt(self.const)]]
        else:
            bs = []
            for m in range(2 * order + 1):
                bs.append(self.bilinear_form(m))
            ret = cholesky_inverse(self.anti_band_matrix(bs))
            if as_poly:
                # This is in case we want the polynomial
                ret = [Polynomial(mpfize(row)) for row in ret.tolist()]
            else:
                ret = ret.tolist()

        # Save the file containing the result (only if as_poly is false)
        if not as_poly:
            try:
                with open(saved_file, 'w+b') as f:
                    pickle.dump((this_header, ret), f)
            except FileNotFoundError:
                print('Warning: the bilinear_bases folder has likely been removed')
                os.makedirs(self.hashesfolder, exist_ok=True)
                with open(saved_file, 'w+b') as ff:
                    pickle.dump((this_header, ret), ff)
        return ret

    def md5hash(self, order):
        """Gives the hash of the DampedRational object that we will use
        to save the result of the orthogonal polynomials."""

        h = md5()
        h.update(str(float(self.base)).encode('utf-8'))
        h.update(str(float(self.const)).encode('utf-8'))
        h.update(str(order).encode('utf-8'))
        h.update(repr(tuple([float(p) for p in self.poles])).encode('utf-8'))
        return h.hexdigest()

    def __str__(self):
        """String representation (for debugging purposes)"""

        result = f'{nstr(self.const, 20)}*{nstr(self.base, 20)}^delta / ('
        for p in self.poles:
            if p > 0:
                result += f'(x-{nstr(p,20)})'
            elif p < 0:
                result += f'(x+{nstr(-p, 20)})'
            else:
                result += 'x'
        result += ')'
        return result


class ConstantOne:
    """
    Interface for DampedRational that simply represents the constant "1" but has all the methods that DampedRational has
    """

    def __init__(self):

        self.const = 1
        self.delta_minus_x = 0
        self.poles = []
        self.base = 1

    def shift(self, a_low_prec):
        pass

    def evaluate(self, a_low_prec):
        return 1

    def evaluate_x(self, a_low_prec):
        return 1

    def remove_zero_poles(self, threshold='1e-10'):
        pass

    def get_polynomials(self, order, as_poly=False):
        return [[1]]
