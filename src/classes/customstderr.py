import os
import re
from datetime import datetime

from utilities import simpleflock
import monitor


class StandardErr:
    """Redirects stderr to a logfile with timestamps. Locks the file to allow for concurrent processes to generate
    error logs. """

    def __init__(self, path, monitor_path, identification, job):

        self.path = os.path.realpath(path)
        self.identification = identification
        self.job = job
        self.last_write = datetime(1999, 1, 1)
        self.monitor_path = monitor_path

    def write(self, text):

        # Locks the file before writing to it
        try:
            with simpleflock.SimpleFlock(self.path, timeout=60, mode='a+') as f:
                n = datetime.now()
                if (n - self.last_write).total_seconds() > 1:
                    print(f'An error occurred, see {self.path}')
                    f.write(datetime.now().strftime(f'[%a %d-%m-%Y %H:%M:%S] doing {self.identification} in job {self.job}\n'))
                    if self.monitor_path is not None:
                        monitor.write_to_monitor(self.monitor_path, self.job, 'Error: an unhandled exception occurred.'
                                                 f' See {self.path} or the stderr of this job.')
                    self.last_write = datetime.now()
                print(text, end='')
                f.write(text)
        except IOError:
            # Since we are just writing messages, we don't want to crash the entire job if this somehow fails
            this_hash = hash((self.job, self.identification, text)) & (2**32-1)
            with open(re.sub(r'\.(\w+)$', rf'_{this_hash:x}.\1', self.path), 'w+') as f:
                f.write(datetime.now().strftime(f'[%a %d-%m-%Y %H:%M:%S] doing {self.identification} in job {self.job}\n'))
                f.write(text)

    def flush(self):
        pass
