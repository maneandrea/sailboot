from mpmath import mp

from utilities import params


class Point:
    """Basic class for points"""

    def __init__(self, p):
        self.coords = tuple(mp.mpf(a) for a in p)
        mp.dps = params.POINT_PRECISION

    def __add__(self, other):
        return Point(map(sum, zip(self.coords, other.coords)))

    def __sub__(self, other):
        def sub(a):
            return a[0] - a[1]

        return Point(
            map(sub, zip(self.coords, other.coords))
        )

    def __rmul__(self, other):
        return Point(map(lambda x: other*x, self.coords))

    def __truediv__(self, other):
        return Point(map(lambda x: x/other, self.coords))

    def __lt__(self, other):
        xa, ya = self.coords
        xb, yb = other.coords

        if xa == xb:
            return ya < yb
        else:
            return xa < xb

    def __le__(self, other):
        return self < other or self == other

    def __eq__(self, other):
        return self.coords == other.coords

    def __repr__(self):
        return '(' + ','.join(map(str, self.coords)) + ')'

    def dot(self, other):
        """Dot product between self and other"""

        def prod(a):
            return a[0] * a[1]

        return sum(map(prod, zip(self.coords, other.coords)))

    def length_sq(self):
        """Length squared of self"""
        return sum(map(lambda x: x**2, self.coords))

    def length(self):
        """Norm of self"""
        return mp.sqrt(self.length_sq())

    def distance_sq(self, other):
        """Distance squared between self and other"""
        return (self-other).length_sq()

    def distance(self, other):
        """Euclidean distance between self and other"""
        return (self-other).length()

    def rotate_90(self):
        """Rotates by 90 degrees counterclockwise (only for 2d)"""
        x, y = self.coords
        return Point((-y, x))

    def rotate_minus90(self):
        """Rotates by 90 degrees clockwise (only for 2d)"""
        x, y = self.coords
        return Point((y, -x))

    def angle_alternative(self, other, origin, sign=1):
        """Computes the angle between self and other based at origin"""

        if len(self.coords) == 2:
            x, y = (self - origin).coords
            z, t = (other - origin).coords

            ang = mp.atan2(t*x - y*z, t*y + x*z)
            ang = ang if ang > 0 else 2 * mp.pi + ang

            # With this we reduce the angle to a finite set of 36000 degrees so that there are no comparison issues
            ang = int(100000 * ang)

            # Return also the distance to choose the ordering between ties
            return ang, sign*(z**2 + t**2)

        else:
            print(f'Delaunay search for spaces with dimension {len(self.coords)} not implemented yet')
            raise NotImplementedError

    def angle(self, other, origin, two_pi_minus=False):
        """Computes the angle between self and other based at origin"""

        if len(self.coords) == 2:
            x, y = (self - origin).coords
            z, t = (other - origin).coords

            ang = mp.atan2(t*x - y*z, t*y + x*z)
            if two_pi_minus:
                ang = 2*mp.pi - ang if ang > 0 else -ang
            else:
                ang = ang if ang > 0 else 2 * mp.pi + ang

            # With this we reduce the angle to a finite set of 36000 degrees so that there are no comparison issues
            ang = int(100000 * ang)

            # Return also the distance to choose the ordering between ties
            return ang, x**2 + y**2

        else:
            print(f'Delaunay search for spaces with dimension {len(self.coords)} not implemented yet')
            raise NotImplementedError

    def counter_clockwise(self, b, c, resolve_ties=0):

        try:
            xa, ya = self.coords
            xb, yb = b.coords
            xc, yc = c.coords

            test = xa*yb + ya*xc + xb*yc - xc*yb - xb*ya - yc*xa

            if test == 0:
                if resolve_ties == 0:
                    # We resolve the edge case by returning true if b is in between self and c
                    return (xa-xb)**2 + (ya-yb)**2 < (xa-xc)**2 + (ya-yc)**2
                else:
                    # We resolve the edge case by returning true if c is in between self and b
                    return (xa - xb) ** 2 + (ya - yb) ** 2 > (xa - xc) ** 2 + (ya - yc) ** 2
            else:
                return test > 0

        except ValueError as e:
            if str(e) == 'too many values to unpack (expected 2)':
                print(f'Delaunay search for spaces with dimension {len(self.coords)} not implemented yet')
                raise NotImplementedError
            else:
                raise
