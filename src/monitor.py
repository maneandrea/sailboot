#!/usr/bin/env python3
#
# sailboot
# Copyright (C) 2021 Andrea Manenti
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Utility to read and write into a "monitor" file which contains all the information about currently running jobs"""

import re
import socket
import sys
import os
import subprocess
from datetime import datetime
import time

from utilities import myargparser, results, simpleflock
from classes import customstderr

SDPB_HDR = '''\t\t          time    mu     P-obj       D-obj      gap         P-err       p-err       D-err      P-step   D-step   beta
\t\t---------------------------------------------------------------------------------------------------------------------'''


def compute_elapsed_time(unix_time):
    """Computes the time difference from now and a given unix time and returns a string representation"""

    timediff = int(datetime.now().timestamp()) - int(unix_time)
    hours, remainder = divmod(timediff, 3600)
    minutes, seconds = divmod(remainder, 60)

    ssh = '' if hours == 1 else 's'
    ssm = '' if minutes == 1 else 's'
    sss = '' if seconds == 1 else 's'

    return 'Elapsed {0:d} hour{3:s} {1:d} minute{4:s} and {2:d} second{5:s}'.format(hours, minutes, seconds, ssh, ssm, sss)


def find_logfile(filename, messages):
    """Returns filename if it exists otherwise tries to scp it from the assigned node"""
    if os.path.isfile(filename):
        return False, filename
    else:
        try:
            host_match = re.match(r'node assigned: .*', messages[2])
            if host_match:
                hostname = re.sub(r'node assigned: (.*)$', r'\1', messages[2])
                os.makedirs(os.path.join(os.path.expanduser('~'), '.cache', 'sailboot'), exist_ok=True)
                cpd_file = os.path.join(os.path.expanduser('~'), '.cache', 'sailboot', os.path.basename(filename))
                print(f'\t\t...Retrieving file from {hostname} via scp...', end='\r')
                subproc = subprocess.run(['scp', f'{hostname}:{filename}', cpd_file], capture_output=True)
                if subproc.returncode == 0:
                    return True, cpd_file
                else:
                    raise ValueError(subproc.stderr.decode('utf8').strip())
            else:
                raise ValueError('could not find the assigned node')
        except IndexError:
            raise ValueError('the monitor file is corrupted')


def monitor_print(args):
    """Displays the output of the "read" action"""

    def color(string):
        if args.no_color:
            return string
        else:
            return re.sub('^Has ended', r'\033[1m\033[92mHas ended\033[0m',
                          re.sub('^Has started', r'\033[1m\033[92mHas started\033[0m',
                                 re.sub('^Has crashed', r'\033[1m\033[91mHas crashed\033[0m',
                                        re.sub(r'^(\s*)Warning', r'\1\033[1m\033[93mWarning\033[0m',
                                               re.sub(r'(^\s*)Error', r'\1\033[1m\033[91mError\033[0m', string)))))

    def grep_color(pattern, string):
        if args.no_color:
            return string
        else:
            return re.sub(pattern, r'\033[1m\033[95m\g<0>\033[0m', string, flags=re.IGNORECASE)

    def uncolor(string):
        return re.sub(r'\033\[\d+m', '', string)

    try:
        linelength = int(os.get_terminal_size()[0])
    except OSError:
        linelength = 10

    d = read_monitor(args.file, args.job)
    for job, messages in d.items():
        print(f'job {job}:')
        if len(messages) > 1:
            # We don't show the second message but we use it to compute the elapsed time
            try:
                elapsed_time = compute_elapsed_time(messages[1])
            except ValueError:
                elapsed_time = 'Could not find start time'
            else:
                del messages[1]
        elif len(messages) == 1:
            if args.no_color:
                elapsed_time = 'In queue...'
            else:
                elapsed_time = '\033[1m\033[93mIn queue...\033[0m'
        else:
            elapsed_time = 'Empty log'

        try:
            if messages[-1] not in ['Has ended', 'Has crashed']:
                messages.append(elapsed_time)
        except IndexError:
            pass

        # Here we select a subset of the messages if --max is given
        if args.max is None:
            to_show = messages
        else:
            if len(args.max) == 1:
                args.max = [0, args.max[0]] if args.max[0] > 0 else [-args.max[0], 0]

            head = max(args.max[0], 0)
            tail = max(args.max[1], 0)

            if tail + head >= len(messages):
                to_show = messages
            elif head == 0:
                to_show = messages[-tail:]
            elif tail == 0:
                to_show = messages[:head]
            else:
                n_lines = len(messages) - head - tail
                ss = 's' if n_lines > 1 else ''
                to_show = messages[:head] + [f'\t« {n_lines} line{ss} »'] + messages[-tail:]

        # Here we do a pass to grep and filter
        # Note that grepping is done after --max is applied, so some would-be matches are ignored if they aren't
        # included in the --max bounds
        if args.filter is None and args.grep is None:
            filtered = len(to_show) * [1]
        elif args.filter is None and args.grep is not None:
            # Only in this case we apply the -A and -B options
            filtered = len(to_show) * [0]
            after = max(args.after, 0)
            before = max(args.before, 0)
            for i, message in enumerate(to_show):
                if re.search(args.grep, message, re.IGNORECASE):
                    to_show[i] = grep_color(args.grep, message)
                    for j in range(i-before, i+after+1):
                        try:
                            filtered[j] = 1
                        except IndexError:
                            pass
        elif args.filter is not None and args.grep is None:
            filtered = len(to_show) * [1]
            for i, message in enumerate(to_show):
                if re.search(args.filter, message, re.IGNORECASE):
                    filtered[i] = 0
        else:
            filtered = len(to_show) * [0]
            for i, message in enumerate(to_show):
                if re.search(args.grep, message, re.IGNORECASE) and not re.search(args.filter, message, re.IGNORECASE):
                    to_show[i] = grep_color(args.grep, message)
                    filtered[i] = 1

        # Now we make a second pass to print
        for i, message in enumerate(to_show):
            if filtered[i]:
                print('\t' + color(message))
            else:
                continue
            if args.watch_sdpb:
                message_match = re.match(r'Executing sdpb\. See (.*stdout\.log)', uncolor(message))
                if message_match:
                    try:
                        is_file_temp, found_file = find_logfile(message_match.group(1), messages)
                        with open(found_file, 'r') as f:
                            print(SDPB_HDR)
                            print('\t\t'+'\t\t'.join(f.readlines()[-max(args.watch_sdpb, 1):]).rstrip('\n'))
                    except FileNotFoundError:
                        print(color('\t\tWarning: the above file was not found'))
                    except ValueError as err:
                        print(color(f'\t\tWarning: {err}'))
                    else:
                        if is_file_temp and os.path.isfile(found_file):
                            os.remove(found_file)

        print(linelength * '—')


def main_monitor(passed_arg=None):
    """Main routine"""

    if not passed_arg:
        args = myargparser.monitor_argparse().parse_args()
    else:
        args = myargparser.monitor_argparse().parse_args(passed_arg)

    log_path = os.path.join(os.path.expanduser('~'), '.local', 'sailboot_error.log')
    sys.stderr = customstderr.StandardErr(log_path, None, f'monitor {args.file} {args.action}', args.job)

    # Gives the possibility of giving the folder containing monitor.txt
    if os.path.isdir(args.file):
        args.file = os.path.join(args.file, 'monitor.txt')

    if args.action == 'started':
        write_to_monitor(args.file, int(args.job), 'Has started')

    elif args.action == 'ended':
        all_ended = write_to_monitor(args.file, int(args.job), 'Has ended', args.total)

        # If all jobs have terminated we write a summary .m file
        if all_ended and args.output:
            additional_prints = results.summarize(args.output)
            amend_write_to_monitor(args.file, int(args.job), additional_prints)
            # Optionally we can also send an email
            if args.email_to is not None:
                try:
                    subprocess.run(['mail', '-s', 'Jobs finished', args.email_to],
                                   stdout=subprocess.PIPE,
                                   input='The jobs in ' + os.path.dirname(args.file) +
                                         ' have terminated successfully.\n\nBye!\n',
                                   encoding='ascii')
                except FileNotFoundError:
                    amend_write_to_monitor(args.file, int(args.job), ['Warning: unable to send email'])

    elif args.action == 'communicate':
        try:
            j = int(args.job)
        except ValueError:
            print(f'For the action "communicate" you need to specify a single job, not "{args.job}"')
            sys.exit(1)
        else:
            write_to_monitor(args.file, j, args.message)

    elif args.action == 'read':

        try:
            linelength = int(os.get_terminal_size()[0])
        except OSError:
            linelength = 10

        monitor_print(args)

        # We call the function over and over a bit like "watch". This can be stopped only with Ctrl+C
        if args.repeat is not None:
            if args.repeat < 1:
                args.repeat = 1
            try:
                while True:
                    if os.name == 'nt':
                        os.system('cls')
                    else:
                        os.system('clear')
                    str_left = f'Every {args.repeat}s: ' + ' '.join(sys.argv).replace(' --repeat', '').replace(' -r ', ' ')
                    str_right = datetime.now().strftime('Last updated on %a %d %b %Y at %X')
                    print(str_left + ' '*(linelength - len(str_left) - len(str_right)) + str_right)
                    print(linelength * '—')
                    monitor_print(args)
                    time.sleep(args.repeat)
            except KeyboardInterrupt:
                print('')
                if not passed_arg:
                    sys.exit(0)
                else:
                    raise

    elif args.action == 'kill':
        d = read_monitor(args.file, args.job)
        confirm = input('About to cancel jobs ' + ','.join([str(a) for a in d.keys()]) + '. Confirm?[y/n]\n')
        if confirm in ['y', 'Y', 'yes', 'Yes']:
            for job, messages in d.items():
                sbatch_n = None
                for a in messages:
                    sbatch_match = re.match(r'sbatch number assigned: (\d+)$', a)
                    if sbatch_match:
                        sbatch_n = sbatch_match.group(1)
                        break
                if sbatch_n is not None:
                    print(f'Killing job {job}: scancel {sbatch_n}')
                    # subprocess.run(['scancel', '--signal=SIGTERM', str(sbatch_n)])
                    # time.sleep(.2)
                    subprocess.run(['scancel', str(sbatch_n)])
                    time.sleep(.2)
                    write_to_monitor(args.file, job, 'Error: process terminated by user')
                else:
                    print(f'sbatch number not found for job {job}, skipping')
        elif confirm in ['n', 'N', 'no', 'No']:
            print('Ok, fiuu, that was close')
        else:
            print("I won't do anything just to be safe")

    elif args.action == 'summarize':
        proj_folder = os.path.dirname(args.file)
        results.summarize(os.path.join(proj_folder, 'results'), True)


def parse_job_specification(string, all_jobs):
    """Turns a job specification such as 1-3, 1,2,3 or "running" into a list"""

    single_job = re.match(r'\d+$', string)
    if single_job:
        return {int(string): all_jobs.get(int(string), [])}

    if string == 'all' or string == 'a':
        return all_jobs

    if string == 'queued' or string == 'q':
        return {a: b for a, b in all_jobs.items() if len(b) == 1}

    if string == 'running' or string == 'r':
        return {a: b for a, b in all_jobs.items() if len(b) > 1 and b[-1] not in ['Has ended', 'Has crashed']}

    if string == 'finished' or string == 'f':
        return {a: b for a, b in all_jobs.items() if b[-1] == 'Has ended'}

    if string == 'error' or string == 'e':
        return {a: b for a, b in all_jobs.items() if b[-1] == 'Has crashed'}

    def range_replace(match_obj):
        lis = range(int(match_obj.group(1)), int(match_obj.group(2)) + 1)
        return ','.join([str(a) for a in lis])

    parsed_string = re.sub(r'(\d+)-(\d+)', range_replace, string)
    if re.match(r'(\d+,)+\d+$', parsed_string):
        return {a: all_jobs.get(int(a), []) for a in parsed_string.split(',')}
    else:
        raise ValueError('The given string of jobs does not have the required form.')


def read_monitor(file, jobs):
    """Reads the monitor file and returns a dict"""

    try:
        with simpleflock.SimpleFlock(file, timeout=10, mode='r') as f:
            read_dict = eval(f.read())
    except (ValueError, SyntaxError) as err:
        print('Could not parse monitor file. It might be corrupted.')
        read_dict = {}
    except FileNotFoundError:
        print(f'File {file} not found.')
        read_dict = {}

    return parse_job_specification(jobs, read_dict)


def write_to_monitor(file, job, message, total=0):
    """Writes a message in the monitor file by evaluating and updating its content (which is a dict)"""

    try:
        with simpleflock.SimpleFlock(file, timeout=60, mode='r+') as f:
            # We try to parse the content and if something goes wrong we save the corrupted file somewhere else
            try:
                content = f.read()
                if content:
                    old_dict = eval(content)
                else:
                    old_dict = {}
            except (ValueError, SyntaxError) as err:
                print('Could not parse monitor file. ' + str(err))
                print('Saving old file in ' + file.replace('monitor.txt', f'monitor_error_j{job}.txt'))
                with open(file.replace('monitor.txt', f'monitor_error_j{job}.txt'), 'w+') as g:
                    f.seek(0)
                    g.write('# There was an error in this monitor.txt file. Here is what I could recover\n')
                    g.write(f.read())
                    old_dict = {}
            finally:
                f.truncate(0)
                f.seek(0)

            # Every job has the following messages:
            # [
            #   sbatch number assigned: <xyz>,
            #   <timestamp in Unix time>,
            #   Has started,
            #   <various messages...>,
            #   Elapsed <H> hours <M> minutes and <S> seconds,
            #   Has ended
            # ]
            if job not in old_dict:
                if message == 'Has started':
                    old_dict[job] = ['sbatch number assigned: 000000', str(int(datetime.now().timestamp())),
                                     datetime.now().strftime('Has started on %d/%m/%Y at %H:%M:%S'),
                                     f'node assigned: {socket.gethostname()}']
                else:
                    old_dict[job] = [message]
                    if re.match('Error:', message):
                        old_dict[job] = [message, 'Has crashed']

            elif message == 'Has started':  # It's allowed to write only one thing before "Has started"
                old_dict[job] = [old_dict[job][0], str(int(datetime.now().timestamp())),
                                 datetime.now().strftime('Has started on %d/%m/%Y at %H:%M:%S'),
                                 f'node assigned: {socket.gethostname()}']

            elif message == 'Has ended':
                old_dict[job] = old_dict[job] + [
                    compute_elapsed_time(int(old_dict[job][1])),
                    'Has ended'
                ]
            else:
                try:
                    match_repeated = re.match(message + r'( \((\d+)x\))?$', old_dict[job][-1])
                except re.error:
                    match_repeated = None
                if match_repeated:
                    if match_repeated.group(1) is None:
                        old_dict[job] = old_dict[job][:-1] + [old_dict[job][-1] + ' (2x)']
                    else:
                        repeat = int(match_repeated.group(2))
                        old_dict[job] = old_dict[job][:-1] + [re.sub(r' \(\d+x\)$', '', old_dict[job][-1]) + f' ({repeat+1}x)']
                else:
                    old_dict[job] = old_dict[job] + [message]
                    # If the message starts with "Error:" it means that it's critical, so the job would have stopped.
                    if re.match('Error:', message):
                        old_dict[job] = old_dict[job] + ['Has crashed']

            f.write(re.sub(r',\s*(\d+):', r',\n\g<1>:', str(old_dict)))
    except IOError:
        # Since we are just writing messages, we don't want to crash the entire job if this somehow fails
        with open(re.sub(r'\.txt$', f'_j{job}_{hash(message) & (2**32-1):x}.txt', file), 'w+') as f:
            f.write('{\n')
            f.write(f'\t{job}: [{message}]\n')
            f.write('}\n')

    # Returns True if all jobs have terminated
    if message in ['Has ended', 'Has crashed']:
        if len(old_dict) == total:
            return all([a[-1] in ['Has ended', 'Has crashed'] for a in old_dict.values()])
        else:
            return False
    else:
        return False


def amend_write_to_monitor(file, job, messages):
    """Writes some messages just before the 'Has ended'"""

    with simpleflock.SimpleFlock(file, timeout=60, mode='r+') as f:
        # We try to parse the content and if something goes wrong we save the corrupted file somewhere else
        try:
            old_dict = eval(f.read())
        except (ValueError, SyntaxError) as err:
            print('Could not parse monitor file. It might be corrupted.')
            print('Saving old file in ' + file.replace('monitor.txt', f'monitor_error_j{job}.txt'))
            with open(file.replace('monitor.txt', f'monitor_error_j{job}.txt'), 'w+') as g:
                f.seek(0)
                g.write('# There was an error in this monitor.txt file. Here is what I could recover\n')
                g.write(f.read())
                old_dict = {}
        finally:
            f.truncate(0)
            f.seek(0)

        old_dict[job] = old_dict[job][:-2] + messages + old_dict[job][-2:]

        f.write(re.sub(r',\s*(\d+):', r',\n\g<1>:', str(old_dict)))


if __name__ == '__main__':
    main_monitor()
