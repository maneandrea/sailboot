#!/usr/bin/env python3
#
# sailboot
# Copyright (C) 2021 Andrea Manenti
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Program that generates one or more bash scripts and submits them to the cluster as batch jobs"""

import subprocess
import os
import sys
import re
import errno

import sailboot
import monitor
from utilities import myargparser, resume_bisection
from classes import customstderr


# These definitions will be overwritten by the exec instruction in main_array
def generate_args_list():
    raise NameError('The method generate_args_list() is not provided in the script file')

proj_path = os.path.expanduser('~/untitled')
proj_name = 'untitled'
scratch_path = None
sbatch_opts = '#!/bin/bash\n'

Warning_text = '\033[1m\033[93mWarning\033[0m'
Error_text = '\033[1m\033[91mError\033[0m'


def drange(a, b, delta=1):
    """Returns a range of values including a, b and all reals in between that are delta spaced from a"""

    if (b-a)/delta < 0:
        print(f'Error: delta={delta} must have the same sign as b-a={b-a}.')
        sys.exit(1)

    sign = 1 if delta > 0 else -1

    current = a
    while sign*current < sign*b:
        yield current
        current += delta
    yield b


def arg_to_str(arg):
    if isinstance(arg, str):
        return arg
    else:
        ret = ''
        try:
            ret += arg['command']
            del arg['command']
            for key in arg.keys():
                prefix = '-' if len(key) == 1 else '--'
                if re.match(r'[0-9.]*', str(arg[key])):
                    delimiter = ''
                else:
                    delimiter = "'"
                ret += ' ' + prefix + key.replace('_', '-') +\
                       ' ' + delimiter + str(arg[key]).replace("'", '"').replace(' ', '') + delimiter

        except KeyError:
            print(Error_text + f': entry \'command\' not found. Check the launch script.')
            sys.exit(1)


def main_array(passed_arg=None):
    """Reads the script and executes all the lines as separate jobs"""

    if not passed_arg:
        args = myargparser.array_argparse().parse_args()
    else:
        args = myargparser.array_argparse().parse_args(passed_arg)

    log_path = os.path.join(os.path.expanduser('~'), '.local', 'sailboot_error.log')
    sys.stderr = customstderr.StandardErr(log_path, None, args.script, 0)

    if not args.recover:

        ###########################################
        # Usual behavior                          #
        ###########################################

        # Executes the script file, overwriting the definitions above
        try:
            script_dir = os.path.dirname(os.path.realpath(args.script))
            exec(f'import sys\nsys.path.append("{script_dir}")\n' + open(args.script).read(), globals())
        except FileNotFoundError:
            print(Error_text + f': file {args.script} not found. Exiting.')
            sys.exit(1)
        except (SyntaxError, NameError) as e:
            print(Error_text + f': exception {e} in {args.script}. Exiting.')
            sys.exit(1)

        args_list = [arg_to_str(arg).replace('\n', ' ') for arg in generate_args_list()]

        output_path = os.path.join(proj_path, 'results')

        if args.scratch_files is None and scratch_path is None:
            the_scratch_path = proj_path
            remember_to_link = False
        else:
            if args.scratch_files is None:
                the_scratch_path = scratch_path
            else:
                the_scratch_path = args.scratch_files
            remember_to_link = True

        sdpfiles_path = os.path.join(the_scratch_path, 'sdp_files')
        pickles_path = os.path.join(the_scratch_path, 'sdp_files', 'pickles')
        b_bases_path = os.path.join(the_scratch_path, 'sdp_files', 'pickles', 'bilinear_bases')

        # Creates folders if necessary
        for path_to_create in [proj_path, the_scratch_path, output_path, sdpfiles_path, pickles_path, b_bases_path]:
            if not os.path.exists(path_to_create):
                try:
                    os.makedirs(path_to_create)
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise

        # Linking sdp_files folder if it is stored in a different drive
        if remember_to_link:
            try:
                os.symlink(sdpfiles_path, os.path.join(proj_path, 'sdp_files'), target_is_directory=True)
            except FileExistsError:
                this_path = os.path.join(proj_path, 'sdp_files')
                if os.path.islink(this_path):
                    print(Warning_text + f': {this_path} exists, overwriting the link')
                    os.unlink(this_path)
                else:
                    print(Warning_text + f': {this_path} exists, renaming the folder')
                    os.rename(this_path, this_path + '-bk')
                os.symlink(sdpfiles_path, os.path.join(proj_path, 'sdp_files'), target_is_directory=True)

            # Also, we might need to create these paths in each node as well
            create_paths = b_bases_path
        else:
            create_paths = None

        # Creates the monitor file with an empty dict
        with open(os.path.join(proj_path, 'monitor.txt'), 'w+') as f:
            f.write('{}')

        # Removes the queue file if it exists
        if os.path.isfile(os.path.join(proj_path, 'sdp_files', 'queue.txt')):
            os.remove(os.path.join(proj_path, 'sdp_files', 'queue.txt'))

        # Checks if some results file are already present
        files_in_result = os.listdir(output_path)
        files_in_result = [f for f in files_in_result if re.search(r'-job\d+.json', f)]
        if len(files_in_result) > 0:
            choice = None
            while choice not in ['n', 'N', 'no', 'No', 'y', 'Y', 'yes', 'Yes']:
                if len(files_in_result) == 1:
                    files_string = 'file ' + files_in_result[0] + ' is', 'it'
                elif len(files_in_result) == 2:
                    files_string = f'files {files_in_result[0]}, {files_in_result[1]} are', 'them'
                else:
                    files_string = f'files {files_in_result[0]}, ..., {files_in_result[-1]} are', 'them'
                choice = input(f'The {files_string[0]} already in ./results/.\n'
                               f'Should I rename {files_string[1]}?[y/n]\n')
                if choice in ['n', 'N', 'no', 'No']:
                    print('Ok, some of them may end up being overwritten')
                elif choice in ['y', 'Y', 'yes', 'Yes']:
                    for f in files_in_result:
                        rename(os.path.join(output_path, f))
                else:
                    print('Choose yes (y) or no (n).')

        # Checks if the dumpsave is already there
        if not os.path.isfile(os.path.join(sdpfiles_path, 'pickles', 'dumpsave-vectors.pickle')) and \
            not os.path.isfile(os.path.join(sdpfiles_path, 'pickles', 'dumpsave-identity.pickle')) and \
                not os.path.isfile(os.path.join(sdpfiles_path, 'dumpsave_skip')) and not args.skip:

            print('It looks like you have not precomputed the vectors of the crossing equations yet')
            print('Should I send a job to [c]ompute them, you want to [l]ink them from a different project or [s]kip them?')
            choice = None
            while choice not in ['c', 'l', 's', '']:
                choice = input(f'Choose "c", "l" or "s" (default "s"):\n')

            # We compute the dumpsave and then exit
            if choice == 'c':
                # Creates the dumpsave by reading the options from the first script
                parse_first = args_list[0]
                option_string = re.sub(r'^\s*[a-zA-Z]+\s+', 'dumpsave ', parse_first)

                save_identity = None
                while save_identity is None:
                    save_identity = input('Save also the identity vector?[y/n]\n')
                    if save_identity in ['y', 'Y', 'yes', 'Yes']:
                        save_identity = True
                    elif save_identity in ['n', 'N', 'no', 'No']:
                        save_identity = False
                    else:
                        print('Choose yes (y) or no (n).')

                if save_identity:
                    to_run = write_file(option_string,
                                        proj_name, proj_path, sdpfiles_path, output_path, sbatch_opts, 0, 2,
                                        args.interpreter, args.email, create_paths)
                else:
                    to_run = write_file(option_string + ' --skip-identity-dumpsave',
                                        proj_name, proj_path, sdpfiles_path, output_path, sbatch_opts, 0, 2,
                                        args.interpreter, args.email, create_paths)

                launch_job(to_run, args.executable, proj_path, 0)

                print(f'After the job is done, call launch.py again')
                return

            # We give the user a possibility to link the dumpsave from another project with same Lambda and Lmax
            elif choice == 'l':
                path_to_link = input('Ok, where would you like me to take those equations from?\n')
                if os.path.isdir(path_to_link):
                    print(f'ln {path_to_link}/dumpsave-identity.pickle ' +
                          os.path.join(sdpfiles_path, 'pickles', 'dumpsave-identity.pickle'))
                    path_to_link_f = os.path.join(path_to_link, 'dumpsave-identity.pickle')
                    os.link(path_to_link_f, os.path.join(sdpfiles_path, 'pickles', 'dumpsave-identity.pickle'))
                    print(f'ln {path_to_link}/dumpsave-vectors.pickle ' +
                          os.path.join(sdpfiles_path, 'pickles', 'dumpsave-vectors.pickle'))
                    path_to_link_f = os.path.join(path_to_link, 'dumpsave-vectors.pickle')
                    os.link(path_to_link_f, os.path.join(sdpfiles_path, 'pickles', 'dumpsave-vectors.pickle'))
                if os.path.isfile(path_to_link):
                    print(f'ln {path_to_link} '+os.path.join(sdpfiles_path, 'pickles', os.path.basename(path_to_link)))
                    os.link(path_to_link, os.path.join(sdpfiles_path, 'pickles', os.path.basename(path_to_link)))
                else:
                    print(f'File {path_to_link} not existent, exiting.')
                    return

            # If the user decides to skip we just continue but save an empty "dumpsave_skip" file to remember the preference
            else:
                open(os.path.join(sdpfiles_path, 'dumpsave_skip'), 'a').close()
                print('Ok, but if your crossing equations do not change between runs it might be convenient to do so.')
                print('If you change your mind remove the empty file '+os.path.join(sdpfiles_path, 'dumpsave_skip'))

        # Runs through all lines with a counter
        counter = 0
        for arg in args_list:
            counter += 1
            to_run = write_file(arg, proj_name, proj_path, sdpfiles_path, output_path, sbatch_opts, counter,
                                len(args_list), args.interpreter, args.email, create_paths)
            if not args.dry_run:
                launch_job(to_run, args.executable, proj_path, counter)

    else:

        ###########################################
        # Recover bisection                       #
        ###########################################

        # Gives the possibility of giving the folder containing monitor.txt
        if os.path.isdir(args.script):
            args.script = os.path.join(args.script, 'monitor.txt')

        proj_path_recover = os.path.dirname(args.script)

        # Writes the last bisection bounds in the launcher scripts
        to_launch = resume_bisection.main_recover(args.script, args.job)

        if to_launch:
            os.rename(args.script, args.script + '.bk')

            # Creates the monitor file with an empty dict
            with open(args.script, 'w+') as f:
                f.write('{}')

            # Renames previous results
            output_path = os.path.join(proj_path_recover, 'results')
            files_in_result = os.listdir(output_path)
            files_in_result = [f for f in files_in_result if re.search(r'-job\d+.json', f)]
            for f in files_in_result:
                rename(os.path.join(output_path, f))

            # Launches the new jobs
            for i, script in enumerate(to_launch):
                launch_job(script, args.executable, proj_path_recover, i+1)


def rename(filename):
    """Renames a filename appending a _n before the extension"""

    def new_filename(n):
        root, ext = os.path.splitext(filename)
        return root+f'_{n}'+ext

    tentative = 1
    while os.path.isfile(new_filename(tentative)):
        tentative += 1

    os.rename(filename, new_filename(tentative))


def write_file(arg, name, path, sdpfiles, output, opts, count, total, python_interpreter, email_to, create_paths):
    """Writes the scripts to be executed by sbatch"""

    run_file = os.path.join(path, f'{name}_j{count}.run')

    main_path = os.path.realpath(sailboot.__file__)
    monitor_path = os.path.realpath(monitor.__file__)

    if python_interpreter is None:
        python_path = ''
    else:
        python_path = python_interpreter + ' '

    with open(run_file, 'w+') as f:
        f.write(opts)

        # Gives a start date
        f.write(('\n'
                 'echo "Started on `date +"%d-%m-%y at %H:%M:%S"`"\n'
                 'a=`date +%s`\n'
                 '\n'))

        # Maybe create paths in scratch
        if create_paths:
            f.write('\n' + 'mkdir -p ' + create_paths + '\n')

        # Write start on monitor
        f.write('\n' + monitor_path + ' ' + os.path.join(path, 'monitor.txt') + f' started -j {count}\n')

        # The actual job
        f.write('\n' + python_path + main_path + ' ' + arg +
                ' --sdpfiles ' + os.path.abspath(sdpfiles) +
                ' --output ' + os.path.abspath(output) +
                ' --monitor-file ' + os.path.join(path, 'monitor.txt') + f' --job {count}\n')

        # Check if the job has completed successfully or exited with nonzero status
        f.write('\n\nif [ $? -eq 0 ]; then')

        # Write ended on monitor
        f.write('\n    ' + monitor_path + ' ' + os.path.join(path, 'monitor.txt') + f' ended -j {count} -t {total} ' +
                '--output ' + os.path.abspath(output))

        if email_to is not None:
            f.write(' --email-to ' + email_to)

        f.write(('\n\n'
                 '    echo "Ended on `date +"%d-%m-%y at %H:%M:%S"`"\n'
                 '    echo "Elapsed ~ $(($((`date +%s`-$a)) / 3600)) hours and $(($(($((`date +%s`-$a)) % 3600))/ 60)) minutes."'))

        f.write('\nelse')

        # Write that the job exited with errors
        if email_to is not None:
            f.write(f'\nmail -s "Jobs crashed" {email_to} <<< "Job {count:d} has crashed\n\nBye!\n"')

        f.write(('\n'
                 '    echo "Ended with errors on `date +"%d-%m-%y at %H:%M:%S"`"\n'
                 '    echo "Before the error ~ $(($((`date +%s`-$a)) / 3600)) hours and $(($(($((`date +%s`-$a)) % 3600))/ 60)) minutes have passed."'))

        f.write('\nfi')

    return run_file


def launch_job(to_run, exe, path, counter):
    """Executes the script with sbatch"""

    if len(exe) > 0 and exe[-1] != '/':
        true_exe = exe+' '
    else:
        true_exe = exe

    os.chmod(to_run, 0o754)
    # linelength = int(subprocess.check_output(['stty', 'size']).split()[1])
    # print(linelength*'—')
    print(f'❯ {true_exe}{to_run}')

    # Launch the job and read the sbatch number from stdout
    process_instance = subprocess.run(f'{true_exe}{to_run}'.split(), stdout=subprocess.PIPE, encoding='utf-8')
    print('  ' + process_instance.stdout.strip('\n'))
    sbatch_no = re.search(r'^[Ss]ubmitted s?batch job:? (\d+)$', process_instance.stdout, flags=re.MULTILINE|re.DOTALL)
    if sbatch_no:
        monitor.write_to_monitor(os.path.join(path, 'monitor.txt'), counter, 'sbatch number assigned: ' + sbatch_no.group(1))
    else:
        monitor.write_to_monitor(os.path.join(path, 'monitor.txt'), counter, 'sbatch number assigned: None')


if __name__ == '__main__':
    main_array()
