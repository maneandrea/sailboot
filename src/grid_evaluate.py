#!/usr/bin/env python3
#
"""Program that evaluates conformal blocks in a given grid of conformal dimensions"""
import os
import argparse
import sys
import glob
import re
from mpmath import mp
from multiprocessing import Pool

from classes import customstderr, blocks, polynomial_matrix

Warning_text = '\033[1m\033[93mWarning\033[0m'
Error_text = '\033[1m\033[91mError\033[0m'


def str_to_bool(string):
    if string.capitalize() in ['True', 'Yes']:
        return True
    elif string.capitalize() in ['False', 'No']:
        return False
    else:
        print(Warning_text + f': {string} is not a True/False string, using True')
        return True


def initialize_pool(prec):
    """Initializes a pool worker in such a way that signals do not trigger the handler of the main process"""
    def func():
        import signal
        from mpmath import mp
        # This is probably not needed, but we keep it there for safety
        mp.prec = prec

        def graceful_exit(*args):
            sys.exit(0)

        signal.signal(signal.SIGTERM, graceful_exit)
        signal.signal(signal.SIGINT, graceful_exit)

    return func


grid_argparse = argparse.ArgumentParser(prog='grid_evaluate.py',
                                        formatter_class=argparse.RawTextHelpFormatter,
                                        description='Evaluates conformal blocks in a grid of dimensions')

grid_argparse.add_argument('folder', type=str,
                           help='folder containing the conformal blocks or single file to convert')
grid_argparse.add_argument('--dump-grid', action='store_true',
                           help='simply output the grid with a given spacing and delta-max in a .m file')
grid_argparse.add_argument('-g', '--grid', nargs='+',
                           help='explicit grid of conformal dimensions')
grid_argparse.add_argument('-M', '--max',
                           help='maximal value of the dimension')
grid_argparse.add_argument('--monitor-file', type=str,
                           help=argparse.SUPPRESS)
grid_argparse.add_argument('-n', '--n-jobs', type=int, default=1,
                           help='number of parallel jobs to use')
grid_argparse.add_argument('-o', '--out',
                           help='path of the output folder (default: FOLDER_discrete)')
grid_argparse.add_argument('--offset', type=float, default=0,
                           help='offset from the unitarity bound where to start the grid [default: 0]')
grid_argparse.add_argument('--poles-in-x', nargs='?', const=True, type=str_to_bool, default=True,
                           help='the poles in the blocks are given in x and not in Delta [default: True]')
grid_argparse.add_argument('-p', '--precision', type=int,
                           help='bit precision of the computations')
grid_argparse.add_argument('-s', '--spacing-ini',
                           help='initial spacing between consecutive values of the dimensions')
grid_argparse.add_argument('-S', '--spacing-fin',
                           help='final spacing between consecutive values of the dimensions (smooth interpolation)')
grid_argparse.add_argument('--the-job', type=int,
                           help=argparse.SUPPRESS)


class TempArgs:
    """Placeholder args class for compatibility with the already defined module"""

    def __init__(self, n_max, Lambda, zzb_ab_deriv, dim, n_jobs, poles_in_x,
                 blocks_dir, monitor_file, the_job):
        self.auto = False
        self.monitor_file = monitor_file
        self.job = the_job
        self.precision = mp.prec
        self.n_max = n_max
        self.Lambda = Lambda
        self.zzb_ab_deriv = zzb_ab_deriv
        self.dimensions = dim
        self.n_tasks = n_jobs
        self.l_max = 0
        self.blocks_bin = None  # because we never use it since auto = False
        self.blocks = blocks_dir
        self.poles_in_x = poles_in_x


##########################################################
# Defining functions                                     #
##########################################################

def load_block(filename, temp_args):
    """Loads a block in memory with its internal representation"""

    block = blocks.Block(filename, temp_args)

    try:
        entry = list(block.block_entries.keys())[0]
    except IndexError:
        print(Error_text + ': No entries found in the block')
        sys.exit(1)

    # Vector of derivatives
    vector = []

    # Replacement table of derivatives
    table = block.block_entries[entry]

    if temp_args.Lambda and temp_args.n_max:
        # If we know n_max and Lambda in advance

        l = temp_args.Lambda
        nm = temp_args.n_max
        # All derivatives
        if temp_args.zzb_ab_deriv == 'zzb':
            derivs = [(n + i - j, n - i) for n in range(1, nm + 1) for i in range(1, n + 1) for j in (1, 2)]
        else:
            derivs = [(m, n) for m in range(l + 1) for n in range(int((l - m) / 2) + 1)]

        # derivs is an ordered list of derivatives [(0,0), (0,1), ...] containing all of them
        for e in derivs:
            try:
                vector.append(table[e[0]][e[1]])
            except IndexError:
                print(Warning_text + f': requested entry ({e[0]},{e[1]}) not present in {entry}')
                vector.append([0])
    else:
        # Otherwise we just take all that we find
        derivs = []

        for i, row in enumerate(table):
            for j, col in enumerate(row):
                vector.append(col)
                derivs.append((i, j))

    return block.spin, derivs, polynomial_matrix.PolynomialMatrixWithPrefactor(
        block.prefactor,
        [[vector]],
        block.delta_minus_x,
        mp.prec,
    )


def evaluate(derivs, poly_matrix, delta_list, spin, dim, offset):
    """Evaluates a block in delta"""

    shift = spin + offset + dim - 2 if spin > 0 else offset + (dim - 2)/2
    shifted_delta_list = [a + mp.mpf(shift) for a in delta_list]

    result = []
    for delta_low_prec in shifted_delta_list:
        result.append(poly_matrix.full_evaluate(delta_low_prec)[0][0])

    deriv_dict = {}
    for index, deriv in enumerate(derivs):
        deriv_dict[deriv] = [result[site][index] for site in range(len(delta_list))]

    return deriv_dict


def evaluate_const(derivs, poly_matrix):
    """Evaluates a block in delta"""

    deriv_dict = {}
    for index, deriv in enumerate(derivs):
        deriv_dict[deriv] = [poly_matrix.full_evaluate(0)[0][0][index]]

    return deriv_dict


def output(deriv_dict, delta_list, temp_args):
    """Generator which returns the content of the file"""
    yield '{\n'

    for deriv, values in deriv_dict.items():
        yield f'{temp_args.zzb_ab_deriv}Deriv[{deriv[0]}, {deriv[1]}] -> ' + \
              '{' + ',\n\t'.join(
                mp.nstr(a, mp.dps, strip_zeros=False, min_fixed=-mp.inf, max_fixed=mp.inf) for a in values
            ) + '},\n'

    yield 'deltaList -> {\n\t' + ',\n\t'.join(
        mp.nstr(a, mp.dps, strip_zeros=False, min_fixed=-mp.inf, max_fixed=mp.inf) for a in delta_list
    ) + '\n}}\n'


def process_file(filename, temp_args, out_folder, grid, offset):
    """Processes a single file"""

    base_name = os.path.basename(filename)
    out_file = os.path.join(out_folder, base_name)

    spin, derivs, poly = load_block(filename, temp_args)
    generator = output(
        evaluate(derivs, poly, grid, spin, temp_args.dimensions, offset),
        grid,
        temp_args
    )

    with open(out_file, 'w+') as out_fd:
        for line in generator:
            out_fd.write(line)

    return base_name


def make_constant_block(filename, out_file, temp_args):
    """Make the discrete delta table for a block which is constant in Δ"""

    # We have a look at the file to figure out if it's a zzb or ab Deriv Table. The first line usually suffices
    with open(filename, 'r') as peek:
        for line in peek.readlines():
            match = re.search(r'(zzb|ab)Deriv', line)
            if match:
                temp_args.zzb_ab_deriv = match.group(1)
                break
        else:
            raise ValueError('Could not tell if this is a zzbDerivTable or abDerivTable')

    spin, derivs, poly = load_block(filename, temp_args)
    generator = output(
        evaluate_const(derivs, poly),
        [],
        temp_args
    )

    with open(out_file, 'w+') as out_fd:
        for line in generator:
            out_fd.write(line)


def make_grid(zero, max, ini, fin):
    """Makes a grid given initial value, max value and two spacings which are quadratically interpolated"""
    current = zero

    if ini == fin:
        while current < max:
            yield current
            current += ini
    else:
        while current < max:
            yield current
            x = (current-zero)/(max-zero)
            current += (1-x)*ini + x*fin

    yield max


def read_data_from(filename):
    data = re.match(r'(ab|zzb)DerivTable.*-d(\d+)-.*-nmax(\d+)-.*', os.path.basename(filename))
    if data:
        zzb_ab_deriv = data.group(1)
        dim = int(data.group(2))
        n_max = int(data.group(3))
        Lambda = 2 * n_max - 1

        return zzb_ab_deriv, dim, n_max, Lambda
    else:
        raise ValueError


def dump_grid(file, grid):
    """Outputs the grid into a file"""
    with open(file, 'w+') as f:
        f.write('{\n\t')
        f.write(
            ',\n\t'.join(mp.nstr(a, mp.dps, strip_zeros=False, min_fixed=-mp.inf, max_fixed=mp.inf) for a in grid)
        )
        f.write('\n}')


def main_grid_eval(passed_arg=None):
    """Main routine of the module"""
    if not passed_arg:
        args = grid_argparse.parse_args()
    else:
        args = grid_argparse.parse_args(passed_arg)

    log_path = os.path.join(os.path.expanduser('~'), '.local', 'sailboot_error.log')
    sys.stderr = customstderr.StandardErr(log_path, None, args.folder, 0)

    ##########################################################
    # Parsing paths                                          #
    ##########################################################

    if args.spacing_ini is not None and args.spacing_fin is None:
        args.spacing_fin = args.spacing_ini

    if args.monitor_file is None:
        args.monitor_file = os.path.join(os.path.expanduser('~'), '.local', 'default_monitor.txt')

    if args.the_job is None:
        args.the_job = 0

    if args.precision:
        mp.prec = args.precision
    else:
        print(Warning_text + ': precision not specified, using 500')
        mp.prec = 500
        args.precision = 500

    n_jobs = args.n_jobs
    poles_in_x = args.poles_in_x

    if args.dump_grid:
        # If we have --dump-grid the argument folder contains where to save the file
        if os.path.isfile(args.folder):
            print(Warning_text + f': file {args.folder} already exists, it will be overwritten')
        elif os.path.isdir(args.folder):
            print(Error_text + f': path {args.folder} is a directory, file expected for --dump-grid')
            sys.exit(1)

        n_max, Lambda, zzb_ab_deriv, dim, ls = None, None, None, None, None

    elif os.path.isdir(args.folder):
        # If the input is a folder

        if args.out is None:
            args.out = args.folder + '_discrete'

        try:
            os.makedirs(args.out)
        except FileExistsError:
            print(Warning_text + f': folder {args.out} already exists, files will be overwritten')

        ls = glob.glob(os.path.join(args.folder, 'abDerivTable*.m')) + \
             glob.glob(os.path.join(args.folder, 'zzbDerivTable*.m'))

        if ls:
            try:
                zzb_ab_deriv, dim, n_max, Lambda = read_data_from(ls[0])
            except ValueError:
                print(Error_text + f': the filename {ls[0]} does not follow a recognizable pattern')
                sys.exit(1)
        else:
            print(Error_text + f': folder {args.folder} contains no (ab|zzb)DerivTable*.m files')
            sys.exit(1)

    elif os.path.isfile(args.folder):
        # If the input is a single file

        if args.out is None:
            args.out = os.path.join(
                os.path.dirname(args.folder) + '_discrete',
                os.path.basename(args.folder)
            )

        try:
            os.makedirs(os.path.dirname(args.out))
        except FileExistsError:
            print(Warning_text + f': folder {args.out} already exists, files will be overwritten')

        try:
            zzb_ab_deriv, dim, n_max, Lambda = read_data_from(args.folder)
            ls = [args.folder]
        except ValueError:
            print(Warning_text + f': the filename {args.folder} will be treated as a constant block')

            temp_args = TempArgs(None, None, None, None, n_jobs,
                                 True, args.folder, args.monitor_file, args.the_job)

            make_constant_block(args.folder, args.out, temp_args)
            return

    else:
        print(Error_text + f': folder or file {args.folder} does not exist')
        sys.exit(1)

    ##########################################################
    # Parsing grid                                           #
    ##########################################################

    # The grid is given starting from zero, on every operator it is translated by its unitarity bound
    if args.grid is None:
        # Given by spacing and max
        if (args.spacing_ini is not None) and (args.spacing_fin is not None) and (args.max is not None):
            grid = list(make_grid(
                mp.mpf(f'1e-{args.precision // 10}'),
                mp.mpf(args.max),
                mp.mpf(args.spacing_ini),
                mp.mpf(args.spacing_fin)
            ))
        else:
            print(Error_text + ': if --grid is not given then both --spacing-ini and --max must be present')
            sys.exit(1)
    else:
        # Given explicitly
        if (args.spacing_ini is not None) or (args.spacing_fin is not None) or (args.max is not None):
            print(Warning_text + ': if --grid is given then --spacing-ini, --spacing-fin and --max are ignored')
        grid = [mp.mpf(a) for a in args.grid]

    if args.dump_grid:
        print(f'Dumping grid in {args.folder} and exiting')
        dump_grid(args.folder, grid)
    else:

        ##########################################################
        # Processing files                                       #
        ##########################################################

        temp_args = TempArgs(n_max, Lambda, zzb_ab_deriv, dim, n_jobs,
                             poles_in_x, args.folder, args.monitor_file, args.the_job)

        with Pool(processes=min(args.n_jobs, len(ls)), initializer=initialize_pool(args.precision)) as pool:
            input_gen = ((file, temp_args, args.out, grid, args.offset) for file in ls)
            out_files = pool.starmap(process_file, input_gen)

        ls_set = {os.path.basename(f) for f in ls}
        missing = ls_set - set(out_files)
        if not missing == set():
            print(Warning_text + ': files' + str(missing) + ' have not been computed')


if __name__ == '__main__':
    main_grid_eval()
