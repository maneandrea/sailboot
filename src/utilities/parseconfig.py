import os
import sys
import re
import glob

from classes import gaps
from utilities import params
import monitor


def get_from_presets(preset_dict, lambda_val):
    """Gets a value from a preset by choosing the closest lambda"""
    viable = [a for a in preset_dict.keys() if a >= lambda_val]
    if viable:
        key = min(viable)
    else:
        key = max(list(preset_dict.keys()))
    return preset_dict[key]


def set_override(attribute, value, args):
    """Sets args.attribute to value unless args.override_attribute is set, then it assigns the latter instead"""
    override_attribute = f'override_{attribute}'
    if getattr(args, override_attribute) is None:
        setattr(args, attribute, value)
    else:
        if attribute == 'spin_selection':
            if re.search('range', args.override_spin_selection) or re.match('\[.*]', args.override_spin_selection):
                try:
                    overridden = eval(args.override_spin_selection)
                except SyntaxError:
                    monitor.write_to_monitor(args.monitor_file, args.job,
                                             f'Error: invalid list of spins {args.override_spin_selection}')
                    sys.exit(1)
            else:
                overridden = [int(s) for s in args.override_spin_selection.split(',')]
        else:
            overridden = getattr(args, override_attribute)
        setattr(args, attribute, overridden)
        monitor.write_to_monitor(args.monitor_file, args.job,
                                 f'Warning: attribute {attribute} overrode by command line argument')


def using_params(args):
    """Returns a list of strings summarizing the used parameters"""
    result = []
    for param in [
        'Lambda',
        'precision',
        'order',
        'kept_pole_order',
        'spin_selection',
        'grid_max',
        'r_max'
    ]:
        if hasattr(args, param) and getattr(args, param) is not None:
            result.append(f'{param}: {getattr(args, param)}')

    return result


def parse(args):
    """Parses the .config file and processes the command line arguments further"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    ##########################################################
    # Parsing config file                                    #
    ##########################################################

    # This will parse the .config file
    if args.config is None:
        print_monitor('Warning: no config file specified. It may be convenient to put common settings in a file.')
    else:
        try:
            with open(args.config, 'r') as f:
                text = f.read()
        except FileNotFoundError:
            print_monitor(f'Error: config file {args.config} not found. Exiting')
            sys.exit(1)
        else:
            # Put Python dicts on the same line
            text = re.sub(r'{([^}{]*)}', lambda m: '{' + m.group(1).replace('\n', ' ') + '}', text, flags=re.DOTALL)
            lines = text.split('\n')
            for line in lines:
                if len(line) > 0 and not line[0] == '#':
                    parsedline = re.match('((threshold)|(precision)|(blocks)|(crosseqns)|(Lambda)|(legacy_input)|'
                                          '(paramfile)|(parallelize)|(sdpb_bin)|(poles_in_x)|(dimensions)|'
                                          '(order_dict)|(kept_pole_order_dict)|(spin_selection_dict)|(precision_dict)|'
                                          '(n_tasks)|(integrated_blocks)|(grid_max_dict)|(grid_spacing_dict)|'
                                          '(auto)|(discrete_blocks)|(discrete_delta)|(r_max_dict)|(hotstart)|'
                                          r'(zzb_ab_deriv)|(blocks_bin)|(unisolve_bin))\s*=\s*(.*)$', line, re.VERBOSE)
                    if parsedline:
                        last_group = parsedline.lastindex

                        if parsedline.group(1) in ['precision', 'Lambda', 'n_tasks', 'dimensions']:
                            if getattr(args, parsedline.group(1)) is None:
                                setattr(args, parsedline.group(1), int(parsedline.group(last_group)))

                        elif parsedline.group(1) in ['threshold', 'hotstart']:
                            if getattr(args, parsedline.group(1)) is None:
                                setattr(args, parsedline.group(1), float(parsedline.group(last_group)))

                        elif parsedline.group(1) == 'paramfile':
                            if args.paramfile is None:
                                checkiftwo = re.match('(.*),(.*)', parsedline.group(last_group))
                                if checkiftwo:
                                    args.paramfile = checkiftwo.group(1).strip('"\' '), checkiftwo.group(2).strip('"\' ')
                                else:
                                    print_monitor(
                                        'Error: Could not parse both parameter files '
                                        '(one for feasibility one for maximization)'
                                    )
                                    sys.exit(1)

                        elif parsedline.group(1) in ['poles_in_x', 'legacy_input', 'auto', 'discrete_delta']:
                            if getattr(args, parsedline.group(1)) is None:
                                parsed_group = parsedline.group(last_group).capitalize()
                                if parsed_group in ['Yes', 'True']:
                                    true_or_false = True
                                elif parsed_group in ['No', 'False']:
                                    true_or_false = False
                                else:
                                    print_monitor(f'Warning: {parsed_group} is not a True/False string, using default')
                                    true_or_false = None
                                setattr(args, parsedline.group(1), true_or_false)

                        elif parsedline.group(1) in [
                            'order_dict',
                            'kept_pole_order_dict',
                            'spin_selection_dict',
                            'grid_max_dict',
                            'grid_spacing_dict',
                            'r_max_dict',
                            'precision_dict'
                        ]:
                            if getattr(args, parsedline.group(1)) is None:
                                try:
                                    the_dict = eval(parsedline.group(last_group).strip('"\''))
                                except (SyntaxError, NameError) as e:
                                    print_monitor(f'Warning: could not parse {parsedline.group(1)}, exception: {e}')
                                else:
                                    setattr(args, parsedline.group(1), the_dict)

                        else:
                            if getattr(args, parsedline.group(1)) is None:
                                setattr(args, parsedline.group(1), parsedline.group(last_group).strip('"\''))
                    else:
                        print_monitor('Error: Line "' + line.strip('\n') + '" cannot be parsed.')
                        sys.exit(1)

    ##########################################################
    # Parameter verification                                 #
    ##########################################################

    # This will make sure that all the needed parameters are there
    if args.Lambda is None:
        print_monitor('Error: Lambda not found, specify it either in the script or the .config file')
        sys.exit(1)
    if args.n_tasks is None:
        print_monitor(f'Warning: n-tasks not found, using {os.cpu_count()}. Specify it either in the script or the .config file')
        args.n_tasks = os.cpu_count()
    if args.precision is not None:
        print_monitor('Warning: --precision is deprecated, use --override-precision or --precision-dict')
        args.override_precision = args.precision
    if args.dimensions is None:
        print_monitor('Warning: spacetime dimensions not found, using 3. Specify it either in the script '
                      'or the .config file')
        args.dimensions = 3
    if args.command == 'bisect' and args.threshold is None:
        print_monitor('Error: threshold not found, specify it either in the script or the .config file')
        sys.exit(1)
    if args.legacy_input is None:
        args.legacy_input = False
    if args.no_dumpsave is None:
        args.no_dumpsave = False
    if args.auto is None:
        args.auto = False
    if args.discrete_delta is None:
        args.discrete_delta = False
    if args.blocks is None:
        if args.discrete_delta:
            if args.auto:
                print_monitor('Error: block folder not found, if you want to generate the blocks over the Δ-grid '
                              'automatically then specify it either in the script or the .config file')
                sys.exit(1)
        else:
            print_monitor('Error: block folder not found, specify it either in the script or the .config file')
            sys.exit(1)
    if args.crosseqns is None:
        print_monitor('Error: crossing equations file not found, specify it either in the script or the .config file')
        sys.exit(1)
    if args.paramfile is None:
        print_monitor('Warning: sdpb paramfile not found, using the default ones')
        args.paramfile = (
            os.path.join(os.path.dirname(__file__), '..', 'scripts', 'param.sdpb'),
            os.path.join(os.path.dirname(__file__), '..', 'scripts', 'paramOPE.sdpb')
        )
    if args.poles_in_x is None:
        print_monitor('Warning: poles_in_x not set. Assuming that the block poles are in x (i.e. shifted)')
        args.poles_in_x = True
    if args.order_dict is None:
        args.order_dict = params.ORDER_DEFAULT
    if args.kept_pole_order_dict is None:
        args.kept_pole_order_dict = params.KEPT_POLE_ORDER_DEFAULT
    if args.spin_selection_dict is None:
        args.spin_selection_dict = params.SPIN_SELECTION_DEFAULT
    if args.precision_dict is None:
        args.precision_dict = params.PRECISION_DEFAULT

    if args.spectrum_script is not None and args.unisolve_bin is None:
        args.unisolve_bin = os.path.join(args.spectrum_script, 'MPSolve-2.2')
    if args.zzb_ab_deriv is None:
        args.zzb_ab_deriv = 'zzb'

    # This will make checks on the parameters specific for a given command
    if args.command == 'bisect':
        if args.to_bisect is None:
            print_monitor('Error: --to-bisect is mandatory for the bisect command')
            sys.exit(1)
        if args.bisect_max is None or args.bisect_max is None:
            print_monitor('Error: -m and -M are mandatory for the bisect command')
            sys.exit(1)
        if args.hotstart is None:
            args.hotstart = 0
    elif args.command == 'ope':
        if args.ope is None:
            print_monitor('Error: --ope is mandatory for the ope command')
            sys.exit(1)
    elif args.command == 'delaunay':
        if args.initial is None:
            print_monitor('Error: --initial is mandatory for the delaunay command')
            sys.exit(1)

    # This will make checks on the parameters specific to the discrete delta mode
    if args.discrete_delta:
        if args.grid_max_dict is None:
            args.grid_max_dict = params.GRID_MAX_DEFAULT
        if args.grid_spacing_dict is None:
            args.grid_spacing_dict = params.GRID_SPACING_DEFAULT
        if args.discrete_blocks is None:
            print_monitor('Error: discrete block folder not found, specify it either in the script or the .config file')
            sys.exit(1)
        if args.integrated_blocks is None:
            print_monitor('Warning: integrated blocks folder not found, '
                          'if you plan to use an integrated constraint then '
                          'specify it either in the script or the .config file')
        else:
            if args.r_max_dict is None:
                args.r_max_dict = params.R_MAX_DEFAULT

    ##########################################################
    # Setting folders and converting parameters              #
    ##########################################################

    # Lambda and nMax are related as such
    args.n_max = int((args.Lambda + 1)/2)

    # This is the n_max of the folder, not necessarily the n_max of the problem
    args.folder_n_max = args.n_max

    # We scale all parameters based on Lambda. We take the values from a dictionary of optimal choices
    set_override('order', get_from_presets(args.order_dict, args.Lambda), args)
    set_override('kept_pole_order', get_from_presets(args.kept_pole_order_dict, args.Lambda), args)
    set_override('spin_selection', get_from_presets(args.spin_selection_dict, args.Lambda), args)
    set_override('precision', get_from_presets(args.precision_dict, args.Lambda), args)
    args.l_max = max(args.spin_selection)

    # Same for the integrated constraint
    if args.discrete_delta:
        set_override('grid_max', get_from_presets(args.grid_max_dict, args.Lambda), args)
        args.grid_spacing = get_from_presets(args.grid_spacing_dict, args.Lambda)

    # And for the max order in the r-expansion for the integrated blocks
    if args.integrated_blocks and args.discrete_delta:
        set_override('r_max', get_from_presets(args.r_max_dict, args.Lambda), args)
        # We also set the folder to have the same r_max by default
        args.folder_r_max = args.r_max
    else:
        args.r_max = 0

    def find_good_folders(folder_template: str, change_n_max: bool, change_r_max: bool) -> str:
        """Find folders with required Lmax and Lambda or bigger"""
        good_directories = []
        ls = glob.glob(
            folder_template.replace('$LMAX', '*').replace('$LAMBDA', '*').replace('$NMAX', '*').replace('$RMAX', '*')
        )
        pattern = folder_template.replace('$LMAX', r'(?P<l_max>\d+)'
                                          ).replace('$LAMBDA', r'(?P<Lambda>\d+)'
                                                    ).replace('$NMAX', r'(?P<n_max>\d+)'
                                                              ).replace('$RMAX', r'(?P<r_max>\d+)') + '$'

        # We gave to the groups the same names as the attributes, so we can check them in a list comprehension
        for directory in ls:
            isgood = re.match(pattern, directory)
            if isgood:
                groupdict = isgood.groupdict()
                if all([int(val) >= getattr(args, key) for key, val in groupdict.items()]):
                    keys = [0]
                    # We give precedence to the lowest n_max or Lambda, then the lowest r_max, then the lowest l_max
                    if 'n_max' in groupdict:
                        keys.append(int(groupdict['n_max']))
                    if 'Lambda' in groupdict:
                        keys.append(int(groupdict['Lambda']))
                    if 'r_max' in groupdict:
                        keys.append(int(groupdict['r_max']))
                    if 'l_max' in groupdict:
                        keys.append(int(groupdict['l_max']))
                    good_directories.append((keys, directory))

        if len(good_directories) == 0:
            if args.auto:
                new_dir = folder_template.replace('$LMAX', f'{args.l_max}'
                                                  ).replace('$LAMBDA', f'{args.Lambda}'
                                                            ).replace('$NMAX', f'{args.n_max}'
                                                                      ).replace('$RMAX', f'{args.r_max}')
                print_monitor(f'Warning: blocks directory not found, '
                              f'creating new directory {new_dir}')
                good_directories = [(0, new_dir)]
                os.makedirs(new_dir)
            else:
                if args.r_max:
                    print_monitor(f'Error: blocks directory with LMAX≥{args.l_max}, '
                                  f'Lambda≥{args.Lambda} and RMAX≥{args.r_max} not found')
                else:
                    print_monitor(f'Error: blocks directory with LMAX≥{args.l_max} and Lambda≥{args.Lambda} not found')
                sys.exit(1)

        blocks_temp = min(good_directories, key=lambda x: x[0])[1]

        # We found a good folder, but it's not exactly with the requested n_max and L_max
        if blocks_temp != folder_template.replace('$LMAX', f'{args.l_max}'
                                                  ).replace('$LAMBDA', f'{args.Lambda}'
                                                            ).replace('$NMAX', f'{args.n_max}'
                                                                      ).replace('$RMAX', f'{args.r_max}'):
            print_monitor(f'Warning: requested blocks directory not found, loading blocks from {blocks_temp}')

            # Now readjust order and kept_pole_order with the new n_max
            pattern_match = re.match(pattern, blocks_temp)
            if 'n_max' in pattern_match.groupdict():
                current_Lambda = 2*int(pattern_match.group('n_max')) - 1
            elif 'Lambda' in pattern_match.groupdict():
                current_Lambda = int(pattern_match.group('Lambda'))
            else:
                current_Lambda = int(args.Lambda)

            if 'r_max' in pattern_match.groupdict():
                current_r_max = int(pattern_match.group('r_max'))
            else:
                current_r_max = int(args.r_max)

            if change_n_max:
                args.folder_n_max = int((current_Lambda + 1) / 2)
                set_override('order', get_from_presets(args.order_dict, current_Lambda), args)
                set_override('kept_pole_order', get_from_presets(args.kept_pole_order_dict, current_Lambda), args)
            if change_r_max:
                args.folder_r_max = current_r_max

        return blocks_temp

    # This will find the folder with the required LMax and Lambda, finding the closest above it if not present
    if args.blocks:
        args.blocks = find_good_folders(args.blocks, change_n_max=True, change_r_max=False)

    # Same for discrete_blocks
    if args.discrete_blocks:
        args.discrete_blocks = find_good_folders(args.discrete_blocks, change_n_max=True, change_r_max=False)

    # Same for the integrated blocks folder
    if args.integrated_blocks:
        args.integrated_blocks = find_good_folders(args.integrated_blocks, change_n_max=False, change_r_max=True)

    # This will put the gap specifications such as (p,L,"gap",Delta) into our own custom class
    try:
        if args.assumptions is not None:
            args.assumptions = [gaps.Operator(a, args.precision) for a in args.assumptions]
        else:
            args.assumptions = []
        if args.command == 'bisect':
            # We can bisect on an operator dimension or on a crossing equation parameter
            # In the latter case args.to_bisect will remain a string
            if re.match(r'\(.*\)$', args.to_bisect):
                args.to_bisect = gaps.Operator(args.to_bisect, args.precision)
        if args.command == 'ope':
            args.ope = gaps.Operator(args.ope, args.precision)
    except gaps.OperatorError as e:
        print_monitor('Error: failure in parsing assumptions: ' + str(e))
        sys.exit(1)
    else:
        # Check that the number of arguments given in --initial matches the number of placeholders found
        if args.command == 'delaunay':
            given = len(args.initial)
            found = sum([op.placeholder_count for op in args.assumptions])
            if given != found:
                print_monitor(f'Error: number of placeholders (found: {found}) does not match the number of arguments'
                              f' (given: {given}).')
                sys.exit(1)

    # We make upper and lower bound specifications lowercase
    if args.command == 'ope':
        args.ope_bound = args.ope_bound.lower()
        if args.ope_bound not in ['l', 'u', 'lu', 'ul']:
            print_monitor('Error: --ope-bound should be l, u or ul, not ' + args.ope_bound)
            sys.exit(1)

    # This will check that the replacement rules are correct
    if args.replacement is not None:
        args.replacement = args.replacement.replace('->', ':')
        try:
            args.replacement = eval(args.replacement)
        except SyntaxError:
            print_monitor(f'Error: {args.replacement} does not parse. It should be of the form {{ key : val, ...}}')
            sys.exit(1)
        except NameError:
            print_monitor(f'Error: {args.replacement} parses with errors. Please enclose variable names in quotes')
            sys.exit(1)
        else:
            for key in args.replacement:
                args.replacement[key] = str(args.replacement[key]).strip('\'"')

    print_monitor('Using parameters:')
    for line in using_params(args):
        print_monitor('\t' + line)

    return args
