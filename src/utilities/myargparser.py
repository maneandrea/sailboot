#!/usr/bin/env python3
import argparse

# No need to modify this constant: modify the one with the same name in sailboot.py
EXECUTABLE_NAME = 'sailboot'
VERSION_INFO = 'custom'


# Courtesy of https://stackoverflow.com/a/12269143/12008379
class SortingHelpFormatter(argparse.RawDescriptionHelpFormatter):
    def add_arguments(self, actions):
        actions = sorted(actions, key=lambda x: [a.strip('-') for a in x.option_strings])
        super(SortingHelpFormatter, self).add_arguments(actions)


def single_argparse():
    """Parses the commandline arguments for the main routine. These may be customized for a given project."""

    # dir_path = os.path.dirname(os.path.realpath(__file__))

    ##########################################################
    # Setup the parsers                                      #
    ##########################################################

    base_parser = argparse.ArgumentParser(prog=EXECUTABLE_NAME,
                                          formatter_class=argparse.RawDescriptionHelpFormatter,
                                          description='prepares inputs and launches instances of sdpb '
                                                      'for a specified bootstrap problem',
                                          epilog='assumption syntax:\t\n'
                                                 '                  \tLspec = Leven, Lodd, Lall, Leven+, Lodd+, Lall+, <int>\n'
                                                 '                  \ttype = gap, twistGap, isolatedValue, isolatedTwist, skip\n'
                                                 '                  \t(irrep, Lspec, type[, gap[, angle]])'
                                          )
    subparsers = base_parser.add_subparsers(dest='command', help='command to execute, '
                                            f'call {EXECUTABLE_NAME} <command> --help for specific documentation')

    base_parser.add_argument('-v', '--version', action='version', version=VERSION_INFO,
                             help='display version information and exit')

    parser = argparse.ArgumentParser(add_help=False)

    ##########################################################
    # Build the shared parser                                #
    ##########################################################

    def str_to_bool(string):
        if string.capitalize() in ['True', 'Yes']:
            return True
        elif string.capitalize() in ['False', 'No']:
            return False
        else:
            print(f'Warning: {string} is not a True/False string, using None')
            return None

    parser.add_argument('--auto', nargs='?', const=True, type=str_to_bool,
                        help='automatically generate blocks when needed (only for external scalars)')
    parser.add_argument('--blocks', type=str,
                        help='path of the conformal blocks with possible patterns like $LAMBDA or $LMAX')
    parser.add_argument('--blocks-bin', type=str,
                        help='path of the folder containing the binary of scalar_blocks to generate blocks on the fly')
    parser.add_argument('--config', type=str,
                        help='the configuration file with the settings (will be overwritten by command line arguments)')
    parser.add_argument('--crosseqns', type=str,
                        help='the .m file containing the crossing equations')
    parser.add_argument('--dimensions', type=int,
                        help='number of spacetime dimensions (only needed to locate the blocks from scalar_blocks)')
    parser.add_argument('--discrete-blocks', type=str,
                        help='path of the conformal blocks over a discretized Δ-grid '
                             'with possible patterns like $LAMBDA or $LMAX')
    parser.add_argument('-e', '--external-dim', type=str, nargs='+',
                        help='values of the external dimensions. '
                             'For mixed correlators, the order must agree with the block naming.')
    parser.add_argument('-j', '--job', type=int,
                        # help='counter that counts the job in the array'
                        help=argparse.SUPPRESS)
    parser.add_argument('--job-param', type=float, nargs='+',
                        help='parameters that identify the job, corresponding to the parameters that are varied over '
                             'when making the plot (e.g. if there is only one, it will be the value of the x axis).')
    parser.add_argument('--kept-pole-order-dict', type=str,
                        help='python dict associating values of Lambda to appropriate choices of kept pole order'
                             ' (for the blocks)')
    parser.add_argument('-L', '--Lambda', type=int,
                        help='the value of Lambda = 2*nmax - 1')
    parser.add_argument('--legacy-input', nargs='?', const=True, type=str_to_bool,
                        help='use legacy input format for sdpb (namely the folder instead of the single file)')
    parser.add_argument('--monitor-file', type=str,
                        # help='path of the monitor file (to be used when running an array of jobs)'
                        help=argparse.SUPPRESS)
    parser.add_argument('--n-tasks', type=int,
                        help='number of parallel tasks for internal computations such as applying gaps or convolving '
                             'blocks')
    parser.add_argument('--no-dumpsave', nargs='?', const=True, type=str_to_bool,
                        help='do not save crossing vectors project-wide '
                             '(by default they are saved if the external dimensions stay fixed)')
    parser.add_argument('--order-dict', type=str,
                        help='python dict associating values of Lambda to appropriate choices of recursion order'
                             ' (for the blocks)')
    parser.add_argument('--output', type=str,
                        # help='the path of the folder in which to save the output files'
                        help=argparse.SUPPRESS)
    parser.add_argument('--override-order', type=int, help='override recursion order')
    parser.add_argument('--override-spin-selection', type=str, help='override list of selected spins (comma-separated)')
    parser.add_argument('--override-kept-pole-order', type=int, help='override kept pole order')
    parser.add_argument('--override-grid-max', type=float, help='override maximal delta for discretized grid')
    parser.add_argument('--override-precision', type=int, help='override precision')
    parser.add_argument('--override-r-max', type=int, help='override order in the r-expansion for integrated blocks')
    parser.add_argument('--parallelize', type=str,
                        help='parallelization command, such as mpirun or srun, together with eventual parameters')
    parser.add_argument('--paramfile', type=str, nargs=2,
                        help='the path sdpb paramfile and paramfileOPE')
    parser.add_argument('--poles-in-x', nargs='?', const=True, type=str_to_bool,
                        help='the poles in the blocks are given in x and not in Delta')
    parser.add_argument('-p', '--precision', type=int, help='alias of --override-precision, deprecated')
    parser.add_argument('--precision-dict', type=str,
                        help='python dict associating Lambda to the precision for internal computations and sdpb')
    parser.add_argument('-r', '--replacement', type=str,
                        help='replacement rule for parameters in the crossing equations (such as rank of the group)')
    parser.add_argument('--sdpb-bin', type=str,
                        help='directory containing the binaries of sdpb and pvm2sdp')
    parser.add_argument('--sdpfiles', type=str,
                        # help='the path of the folder in which to save the sdpb input files'
                        help=argparse.SUPPRESS)
    parser.add_argument('--discrete-delta', nargs='?', const=True, type=str_to_bool,
                        help='use tables of blocks evaluated in a grid of dimensions rather than the polynomial '
                             'approximations')
    parser.add_argument('--grid-max-dict', type=str,
                        help='python dict associating Lambda to the maximal value of Delta in the discretized grid')
    parser.add_argument('--grid-spacing-dict', type=str,
                        help="python dict associating Lambda to the spacing of Delta's in the discretized grid")
    parser.add_argument('--spin-selection-dict', type=str,
                        help=argparse.SUPPRESS)
    parser.add_argument('--r-max-dict', type=str,
                        help="python dict associating Lambda to the maximal order in the r expansion for integrated"
                             " blocks")
    parser.add_argument('--integrated-blocks', type=str,
                        help='path of the integrated conformal blocks')
    parser.add_argument('--zzb-ab-deriv', type=str,
                        help='type of derivatives in the blocks: zzb or ab')

    ##########################################################
    # Build the command specific parsers                     #
    ##########################################################

    # Bisect
    parser_bisect = subparsers.add_parser('bisect', description='perform a binary search on a single parameter',
                                          parents=[parser],
                                          formatter_class=SortingHelpFormatter)
    parser_bisect.add_argument('-m', '--bisect-min', type=float,
                               help='lower end of the bisection')
    parser_bisect.add_argument('-M', '--bisect-max', type=float,
                               help='upper end of the bisection')
    parser_bisect.add_argument('-t', '--threshold', type=float,
                               help='bisection threshold')
    parser_bisect.add_argument('-b', '--to-bisect', type=str,
                               help='operator(s) to bisect in the form (name,L,"gap") or (name,L,"twistGap")')
    parser_bisect.add_argument('--hotstart', type=float, nargs='?', default=None, const=0.1, metavar='TRIGGER',
                               help='hotstarts each run from the previous when the bisection distance is smaller '
                                    'than TRIGGER (default 0.1)')

    parser_feasibility = subparsers.add_parser('feasibility', description='check if a point is allowed or not',
                                               parents=[parser],
                                               formatter_class=SortingHelpFormatter)

    # Delaunay
    parser_delaunay = subparsers.add_parser('delaunay', description='perform a Delaunay search on multiple parameters',
                                            parents=[parser],
                                            formatter_class=SortingHelpFormatter)
    parser_delaunay.add_argument('-a', '--assumptions', nargs='*', type=str,
                                 help='list of assumptions in the form (name,Lspec,type[,Delta[,angle]]). '
                                      'Use "$1","$2",... to indicate variable parameters'
                                 )
    parser_delaunay.add_argument('-i', '--initial', type=str, nargs='+',
                                 help='coordinates of an initial point for the delaunay method. '
                                      'Will replace the placeholders $1,$2,... in --assumptions')
    parser_delaunay.add_argument('-t', '--threshold', type=float,
                                 help='threshold on the size of the triangles in the delaunay method')

    # Dumpsave
    parser_dumpsave = subparsers.add_parser('dumpsave', description='precompute polynomial matrix vectors',
                                            parents=[parser],
                                            formatter_class=SortingHelpFormatter)
    parser_dumpsave.add_argument('--skip-identity-dumpsave', action='store_true',
                                 help='skip the generation of the dumpsave for the identity vector')

    # OPE
    parser_ope = subparsers.add_parser('ope', description='maximize or minimize an OPE coefficient',
                                       parents=[parser],
                                       formatter_class=SortingHelpFormatter)
    parser_ope.add_argument('-o', '--ope', type=str,
                         help='operator whose OPE to extremize in the form (name,L,"isolatedValue",Delta[,angle])')
    parser_ope.add_argument('--ope-bound', default='u', type=str,
                         help='put a lower (l) or upper (u) bound or both (lu) on the OPE coefficient')

    # Partially shared
    for par in [parser_bisect, parser_ope, parser_feasibility]:
        par.add_argument('-a', '--assumptions', nargs='*', type=str,
                         help='list of assumptions in the form (name,Lspec,type[,Delta[,angle]])\n'
                         )
        par.add_argument('-f', '--functional', const='1e-10', nargs='?', type=str,
                         help='compute the zeros of the extremal functional with the given threshold (default: 1e-10)')
        par.add_argument('--spectrum-script', type=str,
                         help='directory containing the spectrum extraction script')
        par.add_argument('--unisolve-bin', type=str,
                         help='directory containing the binary of MPSolve for the spectrum extraction')

    # Since sometimes we launch a dumpsave by replacing the command with 'dumpsave' from within launch.py,
    # it's just easier to add dummy assumptions with no meaning that will be ignored instead of removing them
    for flag, options in [
        (('-a', '--assumptions'), (str, '*')),
        (('--a-over-c',), (float, None)),
        (('-b', '--to-bisect'), (str, None)),
        (('-f', '--functional'), (str, '?')),
        (('-i', '--initial'), (str, '+')),
        (('-m', '--bisect-min'), (float, None)),
        (('-M', '--bisect-max'), (float, None)),
        (('-o', '--ope'), (str, None)),
        (('--ope-bound',), (str, None)),
        (('--spectrum-script',), (str, None)),
        (('-t', '--threshold'), (float, None)),
        (('--unisolve-bin',), (str, None))
    ]:
        if options[1] is None:
            parser_dumpsave.add_argument(*flag, help=argparse.SUPPRESS, type=options[0])
        else:
            parser_dumpsave.add_argument(*flag, help=argparse.SUPPRESS, type=options[0], nargs=options[1])

    return base_parser


def array_argparse():
    """Parses the command line arguments for the routine launch.py"""
    parser = argparse.ArgumentParser(prog='launch.py',
                                     formatter_class=argparse.RawTextHelpFormatter,
                                     description='Launches an array of jobs based on a python script in scripts/ '
                                                 'which must be edited prior to running this file')
    parser.add_argument('script', type=str,
                        help='script defining the array of jobs to be launched\n'
                             'or project folder / monitor file if using --recover')
    parser.add_argument('-d', '--dry-run', action='store_true',
                        help='only writes the scripts but does not run anything')
    parser.add_argument('-e', '--executable', default='sbatch',
                        help='uses another executable in place of sbatch')
    parser.add_argument('--email', type=str,
                        help='email the given address when all jobs are done')
    parser.add_argument('-f', '--scratch-files',
                        help='choose an alternative path for the sdp_files folder')
    parser.add_argument('-i', '--interpreter',
                        help='python interpreter used.\n'
                             'If absent tries PyPy first and, if unavailable, falls back to CPython')
    parser.add_argument('-j', '--job', type=str, default='all',
                        help='specify one or more jobs to recover from (only for --recover),\n'
                             'it can be an integer, ranges 1-3,5 or (a)ll (default)')
    parser.add_argument('--recover', action='store_true',
                        help='re-launches unfinished bisection jobs starting from where they left off')
    parser.add_argument('-s', '--skip', action='store_true',
                        help='do not ask for compiling the dumpsave')
    parser.add_argument('-v', '--version', action='version', version=VERSION_INFO,
                        help='display version information and exit')

    return parser


def monitor_argparse():
    """Parses the command line arguments for the routine monitor"""
    parser = argparse.ArgumentParser(prog='monitor.py',
                                     formatter_class=argparse.RawTextHelpFormatter,
                                     description='Writes info about the job on a monitor text file')
    parser.add_argument('file', type=str,
                        help='the path of the monitor text file or its containing folder')
    parser.add_argument('action', choices=['started', 'ended', 'communicate', 'read', 'kill', 'summarize'],
                        nargs='?', default='read',
                        help='choose the action to perform\nread (default):\tprint contents of monitor file,\nkill:\t\t'
                             'cancel a scheduled or running job on slurm,\nsummarize:\tcreate summary.m from '
                             'the results currently present in the project folder')
    parser.add_argument('--email-to', type=str,
                        # help='send an email after the action ended if it was the last job'
                        help=argparse.SUPPRESS)
    parser.add_argument('--filter', type=str,
                        help='show only messages that do *not* match the regular expression given')
    parser.add_argument('-A', '--after', type=int, default=0,
                        help='show A lines after a positive match to --grep', metavar='A')
    parser.add_argument('-B', '--before', type=int, default=0,
                        help='show B lines before a positive match to --grep', metavar='B')
    parser.add_argument('-g', '--grep', type=str,
                        help='show only messages that match the regular expression given')
    parser.add_argument('-j', '--job', type=str, default='all',
                        help='specify one or more jobs to which apply the action "read" or "kill" (default: all),\n'
                             'it can be an integer, ranges 1-3,5 or a '
                             'keyword (r)unning, (q)ueued, (a)ll, (f)inished or (e)rror')
    parser.add_argument('-n', '--max', type=int, nargs='+', metavar='N',
                        help='display at most the last N messages for each job\n(if absent, all messages are shown; '
                             'if negative, show first N; if N1 N2 shows first N1 and last N2 lines)')
    parser.add_argument('-m', '--message', type=str, default='',
                        # help='a custom message to write in the monitor (only for action=communicate)',
                        help=argparse.SUPPRESS)
    parser.add_argument('--no-color', action='store_true',
                        help='show the output with no colors')
    parser.add_argument('--output', type=str,
                        # help='path to write the summary of the results (only for action=ended)',
                        help=argparse.SUPPRESS)
    parser.add_argument('-r', '--repeat', const=2, default=None, nargs='?', type=int,
                        help='if the action is "read" repeats the command every REPEAT seconds (default: 2)')
    parser.add_argument('-t', '--total', type=int, default=1,
                        # help='total number of jobs inside an array (only needed for action=ended)',
                        help=argparse.SUPPRESS)
    parser.add_argument('-w', '--watch-sdpb', const=3, default=0, nargs='?', type=int, metavar='COUNT',
                        help='after the line "Executing sdpb..." shows the last COUNT (3) lines of the stdout.log file')
    parser.add_argument('-v', '--version', action='version', version=VERSION_INFO,
                        help='display version information and exit')

    return parser


if __name__ == '__main__':
    args = single_argparse().parse_args()
    print(args)
