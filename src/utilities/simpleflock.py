# Modified from https://github.com/derpston/python-simpleflock.git

import time
import os


class SimpleFlock:
    """Provides an interface for file locking by creating a ~lockfile next to it.
    Intended to be used with the `with` syntax."""

    def __init__(self, path, timeout=None, mode='r+'):
        self._path = path
        self._lock_path = os.path.join(os.path.dirname(self._path), '~' + os.path.basename(self._path))
        self._timeout = timeout
        self._fd = None
        self.mode = mode
        self._lock_fd = None
        self._no_errors = False

    def __enter__(self):
        if self.mode in ['r', 'rb'] and not os.path.isfile(self._path):
            raise FileNotFoundError(f'[Errno 2] No such file or directory: \'{self._path}\'')
        temp_mode = 'a+b' if 'b' in self.mode else 'a+'
        start_lock_search = time.time()
        while True:
            try:
                self._lock_fd = open(self._lock_path, 'x')
            except FileExistsError:
                if self._timeout is not None and time.time() > (start_lock_search + self._timeout):
                    # Exceeded the user-specified timeout.
                    raise IOError(f'File {self._path} temporarily unavailable, timed out after {self._timeout} seconds.')
            else:
                # Lock acquired!
                self._fd = open(self._path, temp_mode)
                if 'w' in self.mode:
                    self._fd.truncate(0)
                elif 'a' in self.mode:
                    self._fd.seek(0, 2)
                elif 'r' in self.mode:
                    self._fd.seek(0)
                self._no_errors = True
                return self._fd

            # It would be nice to avoid an arbitrary sleep here, but spinning
            # without a delay is also undesirable.
            time.sleep(.1)

    def __exit__(self, *args):
        self._lock_fd.close()
        self._fd.close()
        self._lock_fd = None
        self._fd = None
        # Lock removed
        if self._no_errors:
            try:
                os.remove(self._lock_path)
            except FileNotFoundError:
                print('Could not find the lock file, something went wrong.')
                raise


if __name__ == '__main__':
    print('Acquiring lock...')
    with SimpleFlock('locktest', mode='w', timeout=2):
        print('Lock acquired.')
        time.sleep(20)
    print('Lock released.')


