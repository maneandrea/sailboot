import re
import os
import sys
from pyparsing import *

import monitor


def load_block(file_descriptor, args):
    """Parses a mathematica file containing the replacement rules for the blocks"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # These are the fields that we need to populate
    # output = {scalar_block_name:[[deriv00,...],...], 'spin':int, 'delta_minus_x':spin+d-2, 'poles':[pole1,...]}

    # If the block is directly in the path args.blocks we give it the name 'scalar_block'
    # Otherwise we give it the name of the subfolder it's in
    this_folder = os.path.dirname(os.path.abspath(file_descriptor.name))
    if this_folder == os.path.abspath(args.blocks):
        scalar_block_name = 'scalar_block'
    else:
        subfolder = this_folder.replace(os.path.abspath(args.blocks), '')
        scalar_block_name = re.sub(r'[/\\]', '_', subfolder).strip('_')

    output = {scalar_block_name: []}

    found_L_d = re.search(r'-d(\d+)-.*-L(\d+)-', os.path.basename(file_descriptor.name))
    if found_L_d:
        output['spin'] = int(found_L_d.group(2))
        output['delta_minus_x'] = int(found_L_d.group(1)) + int(found_L_d.group(2)) - 2
    else:
        output['spin'] = 0
        output['delta_minus_x'] = 0
        print_monitor('Warning: spin and delta_minus_x not found.')

    content = file_descriptor.read()

    tokens = re.finditer(
        r'((zzbDeriv|abDeriv)\[(\d+),\s*(\d+)\]\s*->)|(shiftedPoles\s*->\s*)|{|}|(\*x\^(\d+))|(\*x)|,|\+|-', content)
    tokens = list(tokens)

    deriv_dict = {}

    reading_deriv = 0, 0
    apply_minus = False

    tok = None

    # Parsing derivatives
    for pos, tok in enumerate(tokens):
        if tok.group(0)[0] in 'za':  # either zzbDeriv or abDeriv
            reading_deriv = int(tok.group(3)), int(tok.group(4))
            deriv_dict[reading_deriv] = {}

        elif tok.group(0)[0] == '*':  # a factor of x^n
            reading_power = 1 if tok.group(0) == '*x' else int(tok.group(7))
            if apply_minus:
                deriv_dict[reading_deriv][reading_power] = '-' + content[tokens[pos - 1].end():tok.start()].strip(' \t\n')
                apply_minus = False
            else:
                deriv_dict[reading_deriv][reading_power] = content[tokens[pos - 1].end():tok.start()].strip(' \t\n')

        elif tok.group(0)[0] in '+-':
            to_add = content[tokens[pos - 1].end():tok.start()].strip(' \t\n')
            if to_add:  # we check if to_add is empty, which will happen between `...*x^n + ...`
                if apply_minus:
                    deriv_dict[reading_deriv][0] = '-' + to_add
                    apply_minus = False
                else:
                    deriv_dict[reading_deriv][0] = to_add
            if tok.group(0)[0] == '-':  # Remember the minus for later
                apply_minus = True

        elif tok.group(0)[0] in ',}':  # In the rare event that a polynomial ends with a x^0 coefficient
            if 'x' not in tokens[pos - 1].group(0):  # The previous token was not an x^n
                if apply_minus:
                    deriv_dict[reading_deriv][0] = '-' + content[tokens[pos - 1].end():tok.start()].strip(' \t\n')
                    apply_minus = False
                else:
                    deriv_dict[reading_deriv][0] = content[tokens[pos - 1].end():tok.start()].strip(' \t\n')

        if tok.group(0)[0] == 's':  # We arrived to shifted poles
            break

    if tok is None:
        print_monitor('Error: Could not parse block file, no token found')
        sys.exit(1)

    # Parsing poles
    poles_string = content[tok.end():]
    braces = 0
    is_double = False
    current = ''
    output['poles'] = []
    for char in poles_string:
        if char == '{':
            braces += 1
        elif char == '}':
            braces -= 1
            if current:
                output['poles'].append(current)
                if is_double:
                    output['poles'].append(current)
                current = ''
        elif char == ',':
            if current:
                output['poles'].append(current)
                if is_double:
                    output['poles'].append(current)
                current = ''
            elif braces == 1:
                is_double = True
        elif char not in ['\n', ' ', '\t']:
            current += char

    # Free the memory
    del content

    try:
        # max_n is the max value of n, which corresponds to Lambda, not n_max (unfortunate naming)
        max_n = max([a[0] for a in deriv_dict.keys()])
        # This part is in case we load a block from a file that contains more derivatives
        if args.Lambda and max_n > args.Lambda:
            max_n = args.Lambda
    except ValueError:
        print_monitor("Error: could not parse the block. I found no derivatives in it.")
        sys.exit(1)

    # Populate the list of derivatives
    for row in range(max_n + 1):
        output[scalar_block_name].append([])
        if args.zzb_ab_deriv == 'zzb':
            col_range = range(min(row + 1, max_n - row + 1))
        else:
            col_range = range(int((max_n - row)/2) + 1)
        for col in col_range:
            try:
                output[scalar_block_name][row].append([])
            except KeyError:
                print_monitor(f'Warning: malformed derivative table is missing entry ({row},{col})')

            monom_dict = deriv_dict[(row, col)]

            # Like this we make sure that if there are zero coefficients they are not skipped
            for power in range(max(monom_dict.keys()) + 1):
                output[scalar_block_name][row][col].append(monom_dict.get(power, '0'))

            del deriv_dict[(row, col)]

    return output


def load_crosseqns(file_descriptor, args):
    """Parses a mathematica file containing the crossing equations as a list of vectors of matrices
    and additional data"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    content = file_descriptor.read()

    # First we remove all the comments
    mathematica_comment = nestedExpr('(*', '*)').suppress()
    content = mathematica_comment.transformString(content)

    # The definition of deltaPhi may be given as a combination of Δ1, Δ2, Δ3 and Δ4
    # Furthermore one can have also other greek letters
    def greek_replace(match):
        try:
            if match.group(1):
                return {
                    'Alpha': 'Α',
                    'Beta': 'Β',
                    'Gamma': 'Γ',
                    'Delta': 'Δ',
                    'Epsilon': 'Ε',
                    'Zeta': 'Ζ',
                    'Eta': 'Η',
                    'Theta': 'Θ',
                    'Iota': 'Ι',
                    'Kappa': 'Κ',
                    'Lambda': 'Λ',
                    'Mu': 'Μ',
                    'Nu': 'Ν',
                    'Xi': 'Ξ',
                    'Omicron': 'Ο',
                    'Pi': 'Π',
                    'Rho': 'Ρ',
                    'Sigma': 'Σ',
                    'Tau': 'Τ',
                    'Upsilon': 'Υ',
                    'Phi': 'Φ',
                    'Chi': 'Χ',
                    'Psi': 'Ψ',
                    'Omega': 'Ω'
                }[match.group(2)]
            else:
                return {
                    'Alpha': 'α',
                    'Beta': 'β',
                    'Gamma': 'γ',
                    'Delta': 'δ',
                    'Epsilon': 'ε',
                    'CurlyEpsilon': 'ε',
                    'Zeta': 'ζ',
                    'Eta': 'η',
                    'Theta': 'θ',
                    'Iota': 'ι',
                    'Kappa': 'κ',
                    'Lambda': 'λ',
                    'Mu': 'μ',
                    'Nu': 'ν',
                    'Xi': 'ξ',
                    'Omicron': 'ο',
                    'Pi': 'π',
                    'Rho': 'ρ',
                    'Sigma': 'σ',
                    'Tau': 'τ',
                    'Upsilon': 'υ',
                    'Phi': 'φ',
                    'CurlyPhi': 'φ',
                    'Chi': 'χ',
                    'Psi': 'ψ',
                    'Omega': 'ω'
                }[match.group(2)]
        except KeyError:
            print_monitor(f'Error: expression {match.group(0)} could not be parsed')
            sys.exit(1)

    content = re.sub(r'\\\[(Capital)?(\w+)]', greek_replace, content)

    # The rules must be given as ->
    content = content.replace(r'\[Rule]', '->')

    # Then we define the grammar of Mathematica. We expect these fields
    # crossingEquations = { Vector1, Vector2, ...};
    # fileNames = [optional];
    # identityBlock = Vector;
    # vectorNaming = [optional];
    # Note: this does *not* parse all Mathematica code, just the specific one for the crossing equations!
    # In particular it parses:
    #  1. A list of vectors of matrices whose entries are sums of blocks, with Head 'F[block_name]' or
    #     H['block_name'] and body a list of rules, multiplied by an arbitrary Mathematica expression.
    #     If the expression is not just a numerical literal it must be included in a wrapper denoted
    #     coeff[...]. See more details on the format at the end.
    #  2. A (list of) replacement rule where both members are strings or alphabetical variables.
    #  3. A (list of) strings or alphabetical variables
    #  4. All these things must be given as semicolon-separated assignment expressions key = val.

    # Action from parsing '+' or '-'. Replaces coeff -> -coeff if it finds a '-'
    def starts_with_minus(string, loc, tokens):
        if string[loc] == '-':
            try:
                old = tokens[-1]['block*coeff'].get('coeff', ['1'])
                if isinstance(old, list):
                    # Pyparsing 2.4 outputs coeff : ['...'] while Pyparsing 3.0 outputs coeff : '...'
                    old = old[0]
                if re.match(r'\d+(\.\d+)?', old):
                    # Just put a minus in front of a number
                    tokens[-1]['block*coeff']['coeff'] = ['-' + old]
                else:
                    # A more complicated expression requires to be parenthesised
                    tokens[-1]['block*coeff']['coeff'] = ['-(' + old + ')']
            except (KeyError, TypeError) as e:
                print_monitor('Error: something went wrong in parsing the coefficients: ' + str(e))
                sys.exit(1)

    # Action from parsing an F or an H or an I block
    def deriv_maps_action(string, loc, tokens):
        block_name = list(tokens.asDict()['block'].keys())[0]
        if string[loc] == 'F':
            try:
                tokens['block'][block_name]['derivMap'] = 'odd'
            # Special case for an empty list of rules
            except TypeError:
                tokens['block'][block_name] = ParseResults()
                tokens['block'][block_name]['derivMap'] = 'odd'
        elif string[loc] == 'H':
            try:
                tokens['block'][block_name]['derivMap'] = 'even'
            # Special case for an empty list of rules
            except TypeError:
                tokens['block'][block_name] = ParseResults()
                tokens['block'][block_name]['derivMap'] = 'even'
        elif string[loc] == 'I':
            try:
                tokens['block'][block_name]['derivMap'] = None
            # Special case for an empty list of rules
            except TypeError:
                tokens['block'][block_name] = ParseResults()
                tokens['block'][block_name]['derivMap'] = None

    # Function for flattening an array
    def flatten(arr):
        final = []
        running = arr
        while running:
            current = running.pop()
            if isinstance(current, list):
                for elem in current:
                    running.append(elem)
            else:
                final.append(current)
        return list(reversed(final))

    # Greek letters
    greeks = 'αβγδεζηθικλμνξοπρστυφχψωΑΒΓΔΕΖΗΘΙΚΛΜΝΟΠΡΣΥΦΧΨΩ'

    # Variable name
    mathematica_var = Word(alphas + greeks, alphanums + greeks)
    # Positive integer or real number
    number = originalTextFor(Word(nums) + Optional('.' + Word(nums)))
    # Mathematica string
    mathematica_string = QuotedString(quoteChar='"') ^ mathematica_var
    # Mathematica rule (LHS, RHS)
    mathematica_rule = mathematica_string + FollowedBy('->'),\
                       Suppress('->') + (number ^ mathematica_string) + Suppress(Optional(','))
    # DictOf the expression above, but with ZeroOrMore instead of OneOrMore
    mathematica_dict_of = Dict(ZeroOrMore(Group(mathematica_rule[0] + mathematica_rule[1])))
    # List of mathematica rules
    mathematica_rules_list = Suppress(Optional('{')) + mathematica_dict_of + Suppress(Optional('}'))

    # Signed number
    mathematica_literal = number ^ originalTextFor('-' + number) ^ Literal('-').setParseAction(replaceWith('-1'))
    # A block coefficient
    mathematica_block_coeff = (mathematica_literal ^ (
            Suppress('coeff') + QuotedString(quoteChar='[', endQuoteChar=']'))).setResultsName('coeff')
    # A block coefficient that does not start with minus
    mathematica_block_coeff_but_not_minus = (
            number ^ (Suppress('coeff') + QuotedString(quoteChar='[', endQuoteChar=']'))).setResultsName('coeff')
    # A block head (containing the name)
    block_head = Suppress(Char('FHI')) + Suppress('[') + mathematica_string + Suppress(']')
    # A single block
    mathematica_block = Dict(
        Group(block_head + Suppress('[') + mathematica_rules_list + Suppress(']'))).setResultsName(
        'block').setParseAction(deriv_maps_action)

    # A block possibly multiplied by a coefficient
    mathematica_block_times_coeff = Group(Group(mathematica_block).setResultsName('block*coeff') ^ Group(
        mathematica_block_coeff + Suppress(Optional('*')) + mathematica_block).setResultsName(
        'block*coeff') ^ Group(
        mathematica_block + Suppress(Optional('*')) + mathematica_block_coeff_but_not_minus).setResultsName(
        'block*coeff'))

    # Plus or minus
    plusminus = Literal('+') ^ Literal('-')
    # The blocks that appear after the first one
    mathematica_block_added = (Suppress(plusminus) + mathematica_block_times_coeff).setParseAction(
        starts_with_minus)
    # A block entry, namely a (sum of) blocks possibly multiplied by coefficients or a zero entry
    mathematica_block_entry = Group(Literal('0')).setResultsName('zero') ^ \
        Group(mathematica_block_times_coeff + mathematica_block_added[...]).setResultsName('sum')

    # List of variables or strings
    mathematica_list = Suppress('{') + delimitedList(mathematica_string) + Suppress('}')

    # Forward() is used to make recursive definitions
    mathematica_expr = Forward()
    # Mathematica expression as the right hand side, either the single expression above or a list of expressions
    mathematica_expr <<= Group(mathematica_block_entry) ^ Group(
        Suppress('{') + delimitedList(mathematica_expr) + Suppress('}'))
    # Expression of the form variableName = mathExpression(;|EndOfString)
    mathematica_line = Group(
        mathematica_var + Suppress('=') + Group(mathematica_expr ^ mathematica_rules_list ^ mathematica_list) +
        Suppress(Literal(';') ^ StringEnd())
    )
    # Semicolon separated expressions as above
    mathematica_doc = Dict(ZeroOrMore(mathematica_line))

    final_result = {}

    try:
        parsed = mathematica_doc.parseString(content, parseAll=True)
    except ParseException:
        parsed = mathematica_doc.parseString(content)
        print_monitor('Warning: Not all content parsed. Only got {' + ','.join(parsed.asDict().keys()) + '}.')

    parsed_as_dict = parsed.asDict()

    # Check that crossingEquations is there
    if 'crossingEquations' not in parsed:
        print_monitor(f'Error: The field "crossingEquations" could not be found in {list(parsed.keys())}')
        sys.exit(1)

    # Check that crossingEquations is there
    if 'identityBlock' not in parsed:
        print_monitor(f'Error: The field "identityBlock" could not be found in {list(parsed.keys())}')
        sys.exit(1)

    # Validate the lambda expressions in spinParity
    if 'spinParity' in parsed:
        try:
            ssel = parsed_as_dict['spinParity']
            for lambdaexpr in ssel.values():
                eval(lambdaexpr)(2)
        except (SyntaxError, NameError, KeyError, IndexError) as e:
            print_monitor(
                'Error: the lambda expression:\n\t' + str(e) + '\ncannot be parsed. I will drop this entry')
            sys.exit(1)
        else:
            final_result['spinParity'] = ssel

    # Validate the regular expressions in fileNames
    if 'fileNames' in parsed:
        try:
            fil = parsed_as_dict['fileNames']
            for regexpr in fil.values():
                re.match(regexpr, '')
        except re.error as e:
            print_monitor(
                'Error: the regular expression:\n\t' + e.pattern + '\ncannot be parsed. I will drop this entry')
            sys.exit(1)
        else:
            final_result['fileNames'] = fil

    # Namings of the vectors. The purpose of these is to use those names in the gap assignments
    final_result['vectorNaming'] = []
    if 'vectorNaming' in parsed:
        final_result['vectorNaming'] = flatten(parsed_as_dict['vectorNaming'])
    else:
        final_result['vectorNaming'] = []

    # Now we construct the crossing equations. The user has several options
    # 1. Give a single block or a sum of blocks
    #    -> This is interpreted as a single vector with one entry being a one by one matrix
    # 2. Give a list of blocks or sums of blocks
    #    -> This is interpreted as a single vector with one by one matrix entries
    # 3. Give a list of lists blocks or sums of blocks
    #    -> This is interpreted as a list of vectors vector with one by one matrix entries
    # 4. Give a list of lists of matrices of blocks or sums of blocks
    #    -> This is interpreted as a list of vectors with nxn matrix entries
    # Point 3 and 4 may be mixed, in the sense that the lists that appear as elements of the
    # top level list may have matrix entries or sums of blocks entries
    #
    # Then we also need to transpose it because it's easier to have matrices of vectors
    # rather than vectors of matrices. Often the equations are written the other way
    # around so it's better if the input format follows the more conventional way.

    def restructure_vectors(field):
        matrix_vectors = []

        if len(parsed[field]) == 1:
            temp_vectors = parsed_as_dict[field][0]
        else:
            print_monitor('Error: the vectors do not have the required shape.')
            sys.exit(1)

        if isinstance(temp_vectors, dict):
            # Only a sum of blocks
            matrix_vectors.append([[[temp_vectors]]])
        elif isinstance(temp_vectors, list):
            # Possibly a list of vectors of blocks or a single vector
            if isinstance(temp_vectors[0], dict):
                # A single vector
                matrix_vectors.append([[[]]])
                for entry in temp_vectors:
                    matrix_vectors[-1][0][0].append(entry)
            else:
                # A list of vectors
                for vector in temp_vectors:
                    if all([isinstance(entry, dict) for entry in vector]):
                        # Single blocks interpreted as 1x1 matrices
                        matrix_vectors.append([[[]]])
                        for entry in vector:
                            matrix_vectors[-1][0][0].append(entry)
                    else:
                        # A matrix of blocks
                        try:
                            size = len(vector[0])
                            if size != len(vector[0][0]):
                                print_monitor('Error: the matrices must be square.')
                                sys.exit(1)
                            matrix_vectors.append([])
                            for row in range(size):
                                matrix_vectors[-1].append([])
                                for col in range(size):
                                    matrix_vectors[-1][row].append([])
                                    for entry in vector:
                                        matrix_vectors[-1][row][col].append(entry[row][col])
                        except IndexError:
                            print_monitor('Error: the block entries do not have the required shape as matrices.\n'
                                          'Are you sure they are all n×n with the same n?')
                            sys.exit(1)
        else:
            print_monitor('Error: the vectors are neither lists nor dicts.')
            sys.exit(1)

        return matrix_vectors

    final_result['crossingEquations'] = restructure_vectors('crossingEquations')

    if 'integratedBlocks' in parsed:
        final_result['integratedBlocks'] = restructure_vectors('integratedBlocks')

    # A small check to make sure that there are names for each vector
    n_crosseq = len(final_result['crossingEquations'])
    n_names = len(final_result['vectorNaming'])
    if n_crosseq > n_names:
        print_monitor('Warning: there are not enough names for all the vectors provided. '
                      f'Adding name(s) "{n_names + 1}"...')
        for n in range(n_names + 1, n_crosseq + 1):
            final_result['vectorNaming'].append(str(n))
    elif n_crosseq < n_names:
        print_monitor('Warning: there are more names than vectors provided. I will remove the extra names')
        final_result['vectorNaming'] = final_result['vectorNaming'][:n_crosseq]

    # Finally we build the identity block

    def restructure_identity(field):
        identity_vector = []

        if len(parsed[field]) == 1:
            temp_vectors = parsed_as_dict[field][0]
        else:
            print_monitor('Error: the identity vector does not have the required shape.')
            sys.exit(1)

        # Also here we leave the choice of writing a single block or a vector of blocks
        if isinstance(temp_vectors, dict):
            # Only a sum of blocks
            identity_vector.append(temp_vectors)
        elif isinstance(temp_vectors, list) and isinstance(temp_vectors[0], dict):
            # A single vector
            for entry in temp_vectors:
                identity_vector.append(entry)
        else:
            print_monitor('Error: the identity vector does not have the required shape.')
            sys.exit(1)

        return identity_vector

    final_result['identityBlock'] = restructure_identity('identityBlock')

    if 'integratedBlocks' in parsed:
        if 'integratedConstraint' not in parsed:
            print_monitor('Error: the integratedConstraint cannot be missing if integratedBlocks is there.')
            sys.exit(1)
        else:
            final_result['integratedConstraint'] = restructure_identity('integratedConstraint')

    return final_result
