# Choice of the optimal order for a given Lambda
ORDER_DEFAULT = {
    3: 30,
    7: 40,
    11: 50,
    19: 60,
    27: 60,
    31: 70,
    35: 80,
    43: 90
}

# Choice of the optimal kept_pole_order for a given Lambda
KEPT_POLE_ORDER_DEFAULT = {
    3: 8,
    7: 8,
    11: 8,
    19: 14,
    27: 20,
    31: 28,
    35: 32,
    43: 40
}

# Choice of optimal mask for selecting a subset of spins
SPIN_SELECTION_DEFAULT = {
    11: range(0, 22),
    19: list(range(0, 27)) + [49, 50],
    23: list(range(0, 27)) + [29, 30, 33, 34, 37, 38, 41, 42, 45, 46],
    27: list(range(0, 27)) + [29, 30, 33, 34, 37, 38, 41, 42, 45, 46, 49, 50],
    35: list(range(0, 45)) + [47, 48, 51, 52, 55, 56, 59, 60, 63, 64, 67, 68],
    43: list(range(0, 65)) + [67, 68, 71, 72, 75, 76, 79, 80, 83, 84, 87, 88]
}

# Choice of the optimal grid_spacing for a given Lambda
GRID_SPACING_DEFAULT = {
    3: ('0.1', '0.8'),
    19: ('0.1', '1'),
    43: ('0.1', '1.5')
}

# Choice of the optimal grid_max for a given Lambda
GRID_MAX_DEFAULT = {
    3: 10,
    19: 50,
    43: 100
}

# Choice of the optimal highest power in the r-expansion of the integrated blocks for a given Lambda
R_MAX_DEFAULT = {
    3: 5,
    7: 8,
    11: 10,
    19: 12,
    27: 16,
    31: 20,
    35: 22,
    43: 24
}

# Choice of the optimal value of bit precision for internal computations and also for sdpb
PRECISION_DEFAULT = {
    3: 300,
    7: 400,
    11: 500,
    19: 600,
    27: 700,
    31: 800,
    35: 900,
    43: 1000
}

# Precision for the parameter of the integrated constraint
PARAM_PREC = 20

# Precision of the output results
RESULT_PRECISION = 32

# Precision for the external dimensions
EXTERNAL_DIM_PREC = 20

# Precision for floating point operations with points
POINT_PRECISION = 60