import re
import os
import sys
import decimal
from decimal import Decimal
from mpmath import mp

import monitor
from utilities import params

# Precision for the parameter of the integrated constraint
decimal.getcontext().prec = params.PARAM_PREC


def bisect_index(cutoff, array):
    """Given a cutoff Λ returns the first position in array where the value exceeds Λ"""
    low = 0
    high = len(array) - 1
    while high - low > 1:
        current = (high + low) // 2
        if mp.mpf(array[current]) > cutoff:
            high = current
        else:
            low = current

    return high


def load_discretized_block(file_descriptor, args):
    """Parses a mathematica file containing the replacement rules for the Delta-discretized blocks"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # These are the fields that we need to populate
    # output = {scalar_block_name:[[deriv00,...],...], 'spin':int, 'delta_list':list}

    # If the block is directly in the path args.discrete_blocks we give it the name 'scalar_block'
    # Otherwise we give it the name of the subfolder it's in
    this_folder = os.path.dirname(os.path.abspath(file_descriptor.name))
    if this_folder == os.path.abspath(args.discrete_blocks):
        scalar_block_name = 'scalar_block'
    else:
        subfolder = this_folder.replace(os.path.abspath(args.discrete_blocks), '')
        scalar_block_name = re.sub(r'[/\\]', '_', subfolder).strip('_')

    output = {scalar_block_name: []}

    found_L = re.search(r'-L(\d+)-', os.path.basename(file_descriptor.name))
    if found_L:
        output['spin'] = int(found_L.group(1))
    else:
        output['spin'] = 0
        print_monitor('Warning: spin not found.')

    content = file_descriptor.read()

    tokens = re.finditer(
        r'((zzbDeriv|abDeriv)\[(\d+),\s*(\d+)\]\s*->)|(deltaList\s*->\s*)|{|}|,', content)
    tokens = list(tokens)

    deriv_dict = {}

    reading_deriv = 0, 0

    tok = None

    # Parsing derivatives
    skip_comma = False
    for pos, tok in enumerate(tokens):
        if tok.group(0)[0] in 'za':  # either zzbDeriv or abDeriv
            reading_deriv = int(tok.group(3)), int(tok.group(4))
            deriv_dict[reading_deriv] = []
            skip_comma = False

        elif tok.group(0)[0] in ',}' and not skip_comma:  # block evaluated in the next value of Delta
            deriv_dict[reading_deriv].append(content[tokens[pos - 1].end():tok.start()].strip(' \t\n{}'))
            if tok.group(0)[0] == '}':
                skip_comma = True

        if tok.group(0)[0] == 'd':  # We arrived to deltaList
            break

    if tok is None:
        print_monitor('Error: Could not parse block file, no token found')
        sys.exit(1)

    # Parsing delta list
    deltas_string = content[tok.end():]
    output['delta_list'] = []
    for val in deltas_string.strip('{}\n ').split(','):
        to_app = val.strip(' \n\t')
        if to_app:
            output['delta_list'].append(to_app)

    # Free the memory
    del content

    try:
        # max_n is the max value of n, which corresponds to Lambda, not n_max (unfortunate naming)
        max_n = max([a[0] for a in deriv_dict.keys()])
        # This part is in case we load a block from a file that contains more derivatives
        if max_n > args.Lambda:
            max_n = args.Lambda
    except ValueError:
        print_monitor("Error: could not parse the block. I found no derivatives in it.")
        sys.exit(1)

    # Now we check against args.grid_max to see if we need to cut the list
    try:
        this_max = mp.mpf(output['delta_list'][-1])
        if args.grid_max >= this_max:
            if args.grid_max > this_max:
                print_monitor(f'Warning: Requested grid_max={args.grid_max} but the tables reach only {this_max}')
            index = len(output['delta_list'])
        else:
            index = bisect_index(args.grid_max, output['delta_list'])
            output['delta_list'] = output['delta_list'][:index]
    except IndexError:
        # It was a constant block
        index = None

    # Populate the list of derivatives
    for row in range(max_n + 1):
        output[scalar_block_name].append([])
        for col in range(min(row + 1, max_n - row + 1)):
            output[scalar_block_name][row].append([])

            output[scalar_block_name][row][col] += deriv_dict[(row, col)][:index]

    return output


def load_integrated_block(file_descriptor, args):
    """Parses a mathematica file containing the integrated blocks"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # These are the fields that we need to populate
    # output = {integrated_block_name:[delta1, delta2, ...], 'spin':int, 'delta_list':list}

    # If the block is directly in the path args.integrated_blocks we give it the name 'int_block'
    # Otherwise we give it the name of the subfolder it's in
    this_folder = os.path.dirname(os.path.abspath(file_descriptor.name))
    if this_folder == os.path.abspath(args.integrated_blocks):
        int_block_name = 'int_block'
    else:
        subfolder = this_folder.replace(os.path.abspath(args.integrated_blocks), '')
        int_block_name = re.sub(r'[/\\]', '_', subfolder).strip('_')

    output = {}

    found_L = re.search(r'-L(\d+)-', os.path.basename(file_descriptor.name))
    if found_L:
        output['spin'] = int(found_L.group(1))
    else:
        output['spin'] = 0
        print_monitor('Warning: spin not found.')

    content = file_descriptor.read()

    tokens = re.finditer(
        r'(deltaList\s*->\s*)|}|{|,', content)
    tokens = list(tokens)

    integrated_list = []
    tok = None

    # Populate the list of blocks evaluated in Delta
    for pos, tok in enumerate(tokens):
        if tok.group(0)[0] in ',}':
            integrated_list.append(content[tokens[pos - 1].end():tok.start()].strip(' \t\n{'))

        if tok.group(0)[0] == 'd':  # We arrived to deltaList
            break

    if tok is None:
        print_monitor('Error: Could not parse integrated block file, no token found')
        sys.exit(1)

    # Parsing delta list
    deltas_string = content[tok.end():]
    output['delta_list'] = []
    for val in deltas_string.strip('{}\n ').split(','):
        output['delta_list'].append(val.strip(' \n\t'))

    # Free the memory
    del content

    # Populate the list of integrated blocks
    output[int_block_name] = integrated_list

    # Now we check against args.grid_max to see if we need to cut the list
    try:
        this_max = mp.mpf(output['delta_list'][-1])
        if args.grid_max >= this_max:
            if args.grid_max > this_max:
                print_monitor(f'Warning: Requested grid_max={args.grid_max} but the tables reach only {this_max}')
        else:
            index = bisect_index(args.grid_max, output['delta_list'])
            output['delta_list'] = output['delta_list'][:index]
            output[int_block_name] = output[int_block_name][:index]
    except IndexError:
        # Empty block
        pass

    return output


def load_integrated_constraint(file_descriptor, param_value, args):
    """Parses a mathematica file containing the table of integrated constraint varying a single parameter"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # These are the fields that we need to populate
    # output = {integrated_constraint:str, parameter:str}

    # We give it the name of the subfolder or int_block if the folder is args.integrated_blocks
    this_folder = os.path.dirname(os.path.abspath(file_descriptor.name))
    if this_folder == os.path.abspath(args.integrated_blocks):
        int_constraint_name = 'int_block'
    else:
        subfolder = this_folder.replace(os.path.abspath(args.integrated_blocks), '')
        int_constraint_name = re.sub(r'[/\\]', '_', subfolder).strip('_')

    content = file_descriptor.read()

    # \Z is end-of-string, unlike $ which matches also before \n
    tokens = re.finditer(r'("?)(-?\d+\.?\d*)(\1)\s*->\s*|\Z', content)
    tokens = list(tokens)

    if len(tokens) == 1:
        if param_value is not None:
            print_monitor(f'Warning: param is given in {int_constraint_name} but the table {file_descriptor.name}'
                          ' has only one entry')
        return {int_constraint_name: content.strip(' \t\n}')}
    elif param_value is None:
        print_monitor(f'Error: param is not given in {int_constraint_name} but the table {file_descriptor.name}'
                      ' has more than one entry')
        sys.exit(1)
    else:
        tok = None

        # Transform the parameter in decimal
        if isinstance(param_value, mp.mpf):
            dec_param = Decimal(mp.nstr(param_value, params.PARAM_PREC))
        else:
            dec_param = Decimal(str(param_value))

        # Iterate through the list of constraints and select one
        for pos, tok in enumerate(tokens[:-1]):
            # +0 does a trivial arithmetic operation which forces the numbers to be truncated to the desired precision
            if Decimal(tok.group(2)) + 0 == dec_param + 0:
                try:
                    return {int_constraint_name: content[tok.end():tokens[pos+1].start()].strip(' \t\n},')}
                except IndexError:
                    print_monitor(f'Error: parameter {tok.group(2)} has no value next to it')
                    sys.exit(1)

        if tok is None:
            print_monitor(f'Error: Could not parse integrated constraint file {file_descriptor.name}, no token found')
            sys.exit(1)

        print_monitor(f'Error: could not find entry {param_value} in the table {file_descriptor.name}')
        sys.exit(1)
