import argparse
import os
import sys
import re
import glob
import math

Warning_text = '\033[1m\033[93mWarning\033[0m'
Error_text = '\033[1m\033[91mError\033[0m'


def parse_job_specification(string, all_jobs):
    """Turns a job specification such as 1-3, 1,2,3, "all" or "error" into a list"""

    single_job = re.match(r'\d+$', string)
    if single_job:
        return [int(string)] if int(string) in all_jobs else []

    if string == 'all' or string == 'a':
        return list(all_jobs.keys())

    if string == 'error' or string == 'e':
        return [a for a, b in all_jobs.items() if b[-1] == 'Has crashed']

    def range_replace(match_obj):
        lis = range(int(match_obj.group(1)), int(match_obj.group(2)) + 1)
        return ','.join([str(a) for a in lis])

    parsed_string = re.sub(r'(\d+)-(\d+)', range_replace, string)
    if re.match(r'(\d+,)+\d+$', parsed_string):
        return [int(a) for a in parsed_string.split(',') if int(a) in all_jobs]
    else:
        print(Error_text + ': the given string of jobs does not have the required form.')
        sys.exit(1)


def recover(monitor_entry, script, i, tot):
    """Recovers the upper and lower limits of a bisection from an unfinished session documented in a monitor entry"""

    def safe_sub(regex, repl, val):
        newval = re.sub(regex, repl, val)
        if newval == val:
            print(Warning_text + f': could not make the replacement {regex} -> {repl}')
        return newval

    upper = math.inf
    lower = - math.inf
    checking = None

    for row in monitor_entry:
        is_checking = re.match(r'Checking Δ = ([\d.]+)$', row)
        if is_checking:
            checking = float(is_checking.group(1))
        else:
            is_allowed = re.match('\tallowed', row)
            is_disallowed = re.match('\tdisallowed', row)
            if checking and is_allowed:
                if lower < checking:
                    lower = checking
            elif checking and is_disallowed:
                if upper > checking:
                    upper = checking

    if math.isfinite(lower) and math.isfinite(upper):
        with open(script, 'r') as f:
            script_content = f.read()

        script_content = safe_sub(
            r' (-m|--bisect-min) [\d.]+ ',
            f' -m {lower:.9f} ',
            script_content
        )
        script_content = safe_sub(
            r' (-M|--bisect-max) [\d.]+ ',
            f' -M {upper:.9f} ',
            script_content
        )
        script_content = safe_sub(
            r' --job \d+',
            f' --job {i} ',
            script_content
        )
        script_content = safe_sub(
            r' started -j \d+',
            f' started -j {i} ',
            script_content
        )
        script_content = safe_sub(
            r' ended -j \d+ -t \d+',
            f' ended -j {i} -t {tot}',
            script_content
        )
        print(f'Written script {script} with [min, max] = [{lower:.9f},{upper:.9f}]')
        os.rename(script, script + '.bk')
        with open(script, 'w+') as f:
            f.write(script_content)

        return script

    else:
        print(Warning_text + f': skipping file {script} because the upper and lower limits have not been found')
        return None


def recover_and_launch(script_list, monitor_file, given_jobs):
    """Loads the monitor and check it against the script files, then proceeds with the recovery and launch"""
    try:
        with open(monitor_file, 'r') as f:
            monitor = eval(f.read())
    except (ValueError, SyntaxError):
        print(Error_text + ': could not parse monitor file. It might be corrupted.')
        sys.exit(1)
    except FileNotFoundError:
        print(Error_text + f': file {monitor_file} not found.')
        sys.exit(1)

    script_dict = {int(re.sub(r'.*_j(\d+)\.run', r'\1', a)): a for a in script_list}

    if set(script_dict.keys()) == set(monitor.keys()):

        to_launch = []

        jobs_to_redo = parse_job_specification(given_jobs, monitor)
        tot = len(jobs_to_redo)

        for i, job in enumerate(jobs_to_redo):
            recovered = recover(monitor[job], script_dict[job], i+1, tot)
            if recovered:
                to_launch.append(recovered)

        ans = None
        while ans not in ['y', 'Y', 'n', 'N', 'yes', 'no', 'Yes', 'No']:
            ans = input(f'Launch {len(to_launch)} jobs?[y/n]\n')

        if ans in ['y', 'Y', 'yes', 'Yes']:
            return to_launch
        else:
            print('Ok, but the script files have been updated to the new bisection limits')
            return []

    else:
        print(Error_text + ': the script jobs do not match the monitor entries:')
        print('monitor = [' + ','.join([str(j) for j in monitor.keys()]) + ']')
        print('scripts = [' + ','.join([str(j) for j in script_dict.keys()]) + ']')
        sys.exit(1)


def main_recover(monitor_file, jobs):
    if os.path.isfile(monitor_file):
        proj = os.path.dirname(monitor_file)
        scripts = glob.glob(os.path.join(proj, '*_j*.run'))
        if len(scripts) > 0:
            return recover_and_launch(
                scripts,
                os.path.join(proj, 'monitor.txt'),
                jobs
            )
        else:
            print(Error_text + f': the path {proj} does not contain any script file of the form *_j*.run')
            sys.exit(1)
    else:
        print(Error_text + f': the file {monitor_file} does not exist')
        sys.exit(1)
