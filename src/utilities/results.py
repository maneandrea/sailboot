#!/usr/bin/env python3
"""Utility to retrieve the results in JSON format and summarize them into a single mathematica file"""

import json
import os
import sys
from datetime import datetime
import re


def boilerplate_plot(proj_name, plots):
    """Returns some Mathematica boilerplate code for plotting a generic thing"""

    add_lines = [
        'ListLinePlot[',
        '\t{'
    ]

    plot_lines = []
    for key, val in plots.items():
        if val:
            plot_lines.append(
                f'\t\tSortBy[{{#[[1]], -#[[2]]}} &]@plot["{proj_name}"]["{key}"]'
            )

    add_lines.append(',\n'.join(plot_lines))

    add_lines += [
        '\t},',
        f'\tFrameLabel->{{"\\[LeftAngleBracket]job_param\\[RightAngleBracket]", "{proj_name}"}},',
        '\tFrame->True, ImageSize->450, GridLines->Automatic,',
        '\tFrameStyle->Directive[FontFamily->"Latin Modern Sans", FontSize->16, Black]',
        '];'
    ]

    return "\n".join(add_lines)


def summarize(folder, by_hand=False):
    """Takes all results in folder and summarizes them in a .m file"""

    all_files = os.listdir(folder)
    all_files = [os.path.join(folder, a) for a in all_files if re.search(r'.*\.json$', a)]

    printed = []

    def print_return(text):
        """Prints text and also remembers it so that it can return it"""
        nonlocal printed
        print(text)
        printed.append(text)

    # We don't use all these fields, it depends on which problem we are studying
    plots = {'lower_bound': [],
             'upper_bound': [],
             'allowed': [],
             'disallowed': [],
             'spectrum': []}

    def rel_path(path):
        """Makes a path relative from the notebook directory"""
        return os.path.relpath(path, folder)

    put_set_directory = False

    # Read all the files
    for file in all_files:
        with open(file, 'r') as f:
            result = json.loads(f.read())

            result_keys = result.keys()

            # Here we can have different behaviors according to the type of plot
            if 'to_bisect' in result_keys:
                plots['disallowed'].append([*result['point'], result['disallowed']])
                plots['allowed'].append([*result['point'], result['allowed']])
            elif 'ope' in result_keys:
                if 'lower_bound' in result_keys:
                    plots['lower_bound'].append([*result['point'], result['lower_bound']])
                if 'upper_bound' in result_keys:
                    plots['upper_bound'].append([*result['point'], result['upper_bound']])
            elif 'spectrum_from' in result_keys:
                put_set_directory = True
                plots['spectrum'].append([*result['point'], 'Import["' + rel_path(result['spectrum_file']) + '"]'])
            elif 'delaunay' in result_keys:
                plots['disallowed'] = plots['disallowed'] + result['disallowed']
                plots['allowed'] = plots['allowed'] + result['allowed']
            elif 'allowed_is' in result_keys:
                if result['allowed'] == result['allowed_is']:
                    plots['allowed'].append(result['point'])
                else:
                    plots['disallowed'].append(result['point'])
            else:
                print_return('Warning: Unknown type of plot, missing expected keys from ' + ', '.join(result.keys()))
                break

    # Save them
    summary = os.path.join(folder, 'summary.m')
    if by_hand:
        print(f'Writing plot in {summary}')
    else:
        print(f'All jobs have terminated, writing plot in {summary}')

    pre_lines = []
    post_lines = []
    if os.path.isfile(summary):
        print_return('Updating existing summary.m file')
        updating = True
        with open(summary, 'r') as f:
            for line in f.readlines():
                match_comment = re.match(r'\(\* Plot (created|updated) on', line)
                match_setdir = re.match(r'SetDirectory', line)
                if match_comment or match_setdir:
                    pre_lines.append(line)
                else:
                    match_plot_line = re.match(r'plot\["[^"]+"]\["[^"]+"] = ', line)
                    if not match_plot_line:
                        post_lines.append(line)
    else:
        updating = False

    with open(summary, 'w+') as f:

        for line in pre_lines:
            f.write(line)

        if updating:
            f.write(datetime.now().strftime('(* Plot updated on %A %d/%m/%Y at %H:%M *)\n'))
        else:
            f.write(datetime.now().strftime('(* Plot created on %A %d/%m/%Y at %H:%M *)\n'))
        if put_set_directory:
            f.write('SetDirectory[NotebookDirectory[]];\n\n')

        proj_name = os.path.basename(os.path.realpath(os.path.join(folder, '..')))
        for plot_name, the_plot in plots.items():
            if the_plot:
                f.write(f'plot["{proj_name}"]["{plot_name}"] = ')
                f.write('{')
                f.write(
                    ', '.join(
                        '{' + ', '.join(
                            [coord.strip('"\'') for coord in point]
                        ) + '}' for point in the_plot
                    )
                )
                f.write('};\n')

        if not updating:
            f.write(f'\n\n\n(* Plotting function generated for {proj_name} feel free to modify *)\n')
            f.write('ThePlot = ' + boilerplate_plot(proj_name, plots))
            pdf_out = f'{proj_name}.pdf'
            f.write(f'\n\n\n\nExport["{pdf_out}", ThePlot];\n')

        for line in post_lines:
            f.write(line)

    # These prints are also appended to the monitor file
    print_return(f'Plot created, check it out in {os.path.abspath(summary)}')

    return printed


if __name__ == '__main__':
    summarize(sys.argv[1])
