import pickle

import mpmath

import monitor
import copy
import os
import re
import glob
import sys
from multiprocessing import Pool

from utilities import simpleflock, params


def initialize_pool():
    """Initializes a pool worker in such a way that signals do not trigger the handler of the main process"""
    import signal

    def graceful_exit(*args):
        sys.exit(0)

    signal.signal(signal.SIGTERM, graceful_exit)
    signal.signal(signal.SIGINT, graceful_exit)


def apply_assumption(operator, monitor_file, job, sdpfiles):
    """Applies a single assumption, meant to be used in parallel with multiprocessing"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(monitor_file, job, text)

    vector_file = os.path.join(sdpfiles, 'pickles', f'temp_j{job}',
                               f'polymatrix_{operator.irrep}-{operator.spin}.pickle')

    try:
        with open(vector_file, 'rb') as p:
            polymatrix = pickle.load(p)

        if operator.type == 'gap':
            has_shifted = polymatrix.shift(operator.gap)
            # Since we are here, we save also the pickle file with the polynomial basis
            polymatrix.prefactor.get_polynomials(polymatrix.max_exponent//2)
            if has_shifted:
                with open(vector_file, 'w+b') as p:
                    pickle.dump(polymatrix, p)
            return None
        elif operator.type == 'twistGap':
            has_shifted = polymatrix.shift(operator.gap + operator.spin)
            polymatrix.prefactor.get_polynomials(polymatrix.max_exponent//2)
            if has_shifted:
                with open(vector_file, 'w+b') as p:
                    pickle.dump(polymatrix, p)
            return None
        elif operator.type == 'isolatedValue':
            addition = polymatrix.evaluate(operator.gap)
            return addition
        elif operator.type == 'isolatedTwist':
            addition = polymatrix.evaluate(operator.gap + operator.spin)
            return addition
        elif operator.type == 'skip':
            with open(vector_file, 'w+b') as p:
                polymatrix.skip = True
                pickle.dump(polymatrix, p)
            return None
        else:
            print_monitor(f'Warning: assumption {operator.type} not known. Skipping')
    except FileNotFoundError:
        print_monitor(f'Warning: vector name {operator.irrep} with spin {operator.spin} not known. Skipping')


def compute_polynomial_bases(name_spin, monitor_file, job, sdpfiles):
    """Computes the polynomial bases to be used for writing the xml file"""

    name, spin = name_spin

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(monitor_file, job, text)

    vector_file = os.path.join(sdpfiles, 'pickles', f'temp_j{job}', f'polymatrix_{name}-{spin}.pickle')

    try:
        with open(vector_file, 'rb') as p:
            polymatrix = pickle.load(p)
        polymatrix.prefactor.get_polynomials(polymatrix.max_exponent//2)

    except FileNotFoundError:
        print_monitor(f'Warning: vector name {name} with spin {spin} not known. Skipping')


def apply(pre_polynomial_matrices, args):
    """Applies the various gaps and operator dimensions to the given polynomial matrices"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # Here we put to_bisect together with the assumptions, it is assumed that the gap has been assigned to it
    if args.command == 'bisect' and not isinstance(args.to_bisect, str):
        all_assumptions = [*args.assumptions, args.to_bisect]
    else:
        all_assumptions = args.assumptions

    ##########################################################
    # Preparing the assumptions                              #
    ##########################################################

    # Expand the assumptions on multiple spins
    expanded_assumptions_isolated = []
    expanded_assumptions_shift = []
    for operator in all_assumptions:
        if isinstance(operator.spin, int):
            # We need to do the isolatedValues first because the underlying blocks might get shifted otherwise
            # In order to do that we keep two lists
            if operator.type in ['isolatedValue', 'isolatedTwist']:
                expanded_assumptions_isolated.append(operator)
            else:
                expanded_assumptions_shift.append(operator)
        else:
            # A small check
            if operator.type not in ['twistGap', 'isolatedTwist', 'skip']:
                print_monitor(f'Warning: You are assigning spin ranges as {operator.type} as opposed to twistGap or '
                              f'isolatedTwist. Are you sure?')

            # Predefined L ranges
            if operator.spin == 'Lall':
                spin_list = args.spin_selection
            elif operator.spin == 'Lall+':
                spin_list = (a for a in args.spin_selection if a > 0)
            elif operator.spin == 'Lodd':
                spin_list = (a for a in args.spin_selection if a % 2 == 1)
            elif operator.spin == 'Lodd+':
                spin_list = (a for a in args.spin_selection if (a % 2 == 1 and a > 1))
            elif operator.spin == 'Leven':
                spin_list = (a for a in args.spin_selection if a % 2 == 0)
            elif operator.spin == 'Leven+':
                spin_list = (a for a in args.spin_selection if (a % 2 == 0 and a > 0))
            else:
                print_monitor(f'Warning: Spin ranges specification {operator.spin} not known. Using Lall')
                spin_list = args.spin_selection

            for ell in spin_list:
                new_op = copy.copy(operator)
                new_op.spin = ell
                if new_op.type in ['isolatedValue', 'isolatedTwist']:
                    expanded_assumptions_isolated.append(new_op)
                else:
                    expanded_assumptions_shift.append(new_op)

    # Now we save all the vectors in separate pickle files so that we can act on them in parallel
    # Unless pre_polynomial_matrices is None, meaning that we previously saw that the present files are good to go
    base_path = os.path.join(args.sdpfiles, 'pickles', f'temp_j{args.job}')
    if pre_polynomial_matrices is not None:
        os.makedirs(base_path, exist_ok=True)
        for name, irrep in pre_polynomial_matrices.items():
            for spin, polymatrix in irrep.items():
                temp_file_name = os.path.join(base_path, f'polymatrix_{name}-{spin}.pickle')
                with open(temp_file_name, 'w+b') as f:
                    pickle.dump(polymatrix, f)

        # We also save the external dimensions and replacements, so we know if we can reuse them or not
        dim_repls_file_name = os.path.join(base_path, f'dims_and_repls.pickle')
        with open(dim_repls_file_name, 'w+b') as f:
            pickle.dump((args.external_dim, args.replacement), f)

    else:
        print_monitor(f'Using previously computed polynomial matrix vectors from {base_path}')

    polynomial_matrices = {'_isolated_vals': []}

    ##########################################################
    # Actual computations: shift and evaluations             #
    ##########################################################

    print_monitor(f'Shifting {len(expanded_assumptions_shift)} and '
                  f'evaluating {len(expanded_assumptions_isolated)} vectors...')

    # First do this for the isolated operators
    if len(expanded_assumptions_isolated) > 0:
        with Pool(processes=min(args.n_tasks, len(expanded_assumptions_isolated)),
                  initializer=initialize_pool) as pool:
            assumptions_gen = ((op, args.monitor_file, args.job, args.sdpfiles) for op in expanded_assumptions_isolated)
            polynomial_matrices['_isolated_vals'] = pool.starmap(apply_assumption, assumptions_gen)

    # Then for the shifts
    if len(expanded_assumptions_shift) > 0:
        with Pool(processes=min(args.n_tasks, len(expanded_assumptions_shift)),
                  initializer=initialize_pool) as pool:
            assumptions_gen = ((op, args.monitor_file, args.job, args.sdpfiles) for op in expanded_assumptions_shift)
            pool.starmap(apply_assumption, assumptions_gen)

    # Vectors that were unchanged over which we compute the polynomial basis
    # if pre_polynomial_matrices is None it means that we computed all this already
    unchanged = []

    # We load from pickle the needed vectors
    temp_folder = os.path.join(args.sdpfiles, 'pickles', f'temp_j{args.job}')
    try:
        if pre_polynomial_matrices is None:
            # Load all
            for f in glob.glob(os.path.join(temp_folder, f'polymatrix_*.pickle')):
                irrep_spin = re.match(r'.*polymatrix_([a-zA-Z0-9]+)-(\d+).pickle', f)
                if irrep_spin:
                    irrep = irrep_spin.group(1)
                    spin = int(irrep_spin.group(2))
                else:
                    print_monitor(f'Error: file name {f} invalid.')
                    sys.exit(1)

                if irrep not in polynomial_matrices:
                    polynomial_matrices[irrep] = {}
                with open(f, 'rb') as p:
                    polynomial_matrices[irrep][spin] = pickle.load(p)

        else:
            # Load only the ones that changed
            changed = [(operator.irrep, operator.spin) for operator in expanded_assumptions_shift]
            for name, irrep in pre_polynomial_matrices.items():
                if isinstance(irrep, dict):
                    for spin in irrep.keys():
                        if (name, spin) in changed:
                            temp_file_name = os.path.join(temp_folder, f'polymatrix_{name}-{spin}.pickle')
                            if name not in polynomial_matrices:
                                polynomial_matrices[name] = {}
                            with open(temp_file_name, 'rb') as p:
                                polynomial_matrices[name][spin] = pickle.load(p)
                        else:
                            unchanged.append((name, spin))
                            if name not in polynomial_matrices:
                                polynomial_matrices[name] = {}
                            polynomial_matrices[name][spin] = irrep[spin]
    except FileNotFoundError as e:
        print_monitor(f'Error: could not find file {e}. Try to remove the folder {temp_folder}')
        sys.exit(1)
    except EOFError:
        print_monitor(f'Error: found corrupted pickled file. Try to remove the folder {temp_folder}')
        sys.exit(1)

    # Finally we compute the polynomial basis for the unchanged vectors
    if len(unchanged) > 0:
        print_monitor(f'Evaluating bilinear bases for the remaining {len(unchanged)} vectors...')
        with Pool(processes=min(args.n_tasks, len(unchanged)), initializer=initialize_pool) as pool:
            assumptions_gen = ((name_spin, args.monitor_file, args.job, args.sdpfiles) for name_spin in unchanged)
            pool.starmap(compute_polynomial_bases, assumptions_gen)

    to_return = []
    vector_order = []
    for name, irrep in polynomial_matrices.items():
        if isinstance(irrep, dict):
            to_return += irrep.values()
            # The attribute delta_minus_x keeps track of the shifts
            vector_order += [
                (name, spin, irrep[spin].delta_minus_x) for spin in irrep.keys()
            ]
        elif irrep:
            to_return += irrep
            vector_order += len(irrep) * [('isolatedValue', 0, 0)]

    # If we want to extract the spectrum we save the order of the vectors in a file
    # Often it's the same for all jobs, but for safety we save it for all the jobs
    if args.functional is not None:
        hashesfolder = os.path.join(args.sdpfiles, 'pickles')
        if not os.path.exists(hashesfolder):
            try:
                os.makedirs(hashesfolder)
            except OSError:
                pass
        print_monitor(f'Saving vector order in {hashesfolder}/vector_order_j{args.job}.txt')
        with simpleflock.SimpleFlock(os.path.join(hashesfolder, f'vector_order_j{args.job}.txt'), mode='w+') as f:
            f.write('# position\tname\tspin\tDelta-x\n\n{\n')
            f.write(
                ',\n'.join(
                    "{0:d}: ('{1:s}', {2:d}, ".format(n,entry[0], entry[1]) +\
                    mpmath.nstr(entry[2], params.RESULT_PRECISION) + ')'
                    for n, entry in enumerate(vector_order)
                )
            )
            f.write('}\n')

    return to_return
