import re
import os
import sys
import pickle
from mpmath import mp

from makesdp import runsdpb, dumpsave, apply_gaps, get_spectrum
import monitor


def parse_sdpb_stdout(stdout_file):
    """Takes the file stdout.log and parses the line containing the termination reason"""

    with open(stdout_file, 'r') as f:
        for line in reversed(f.readlines()):
            matched_line = re.match('(-{2,})(.+?)(-{2,})$', line)
            if matched_line:
                return matched_line.group(2)

    return 'No matching line found. Check ' + stdout_file


def parse_sdpb_out(out_file):
    """Takes the file out.txt and parses all the fields"""

    parsed = {}
    with open(out_file, 'r') as f:
        for line in f.readlines():
            matched_line = re.match(r'(\w+)\s*=\s*([^;]*);$', line)
            if matched_line:
                parsed[matched_line.group(1)] = matched_line.group(2).strip('" ')

    return parsed


def dot_with_lambda(polymatrix, angle, args):
    """Given a polynomial matrix and an angle (or a unit vector) it builds the polynomial vector"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    mp.prec = args.precision

    if angle is None:
        # Assuming matrix is 1x1
        return polymatrix[0][0]
    elif isinstance(angle, float) or isinstance(angle, int) or isinstance(angle, mp.mpf):
        # Assuming matrix is 2x2 and we are given an angle
        the_angle = [mp.cos(angle), mp.sin(angle)]
        vec_len = 2
    elif hasattr(angle, '__iter__'):
        # Assuming matrix is nxn and we are given a (unit) vector
        the_angle = tuple(angle)
        vec_len = len(angle)
    else:
        print_monitor(f'Error: the angle must be None, a number or iterable, it is a {type(angle)}.')
        sys.exit(1)

    result = None
    try:
        for i in range(vec_len):
            for j in range(vec_len):
                if result is None:
                    result = [a * the_angle[i] * the_angle[j] for a in polymatrix[i][j]]
                else:
                    for k in range(len(result)):
                        result[k] += polymatrix[i][j][k] * the_angle[i] * the_angle[j]
    except IndexError:
        print_monitor(f'Error: the matrix is expected to be {vec_len}x{vec_len} because the OPE was given as a vector.')
        sys.exit(1)
    else:
        return result


def disallowed(args, filename_base, previous_checkpoint=None, extract_spectrum=False):
    """Calls sdpb and returns True if the point is disallowed and False otherwise"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # Load the vectors from the dumpsave
    vector_length, polynomial_matrices, normalization = dumpsave.get_polymatrices(args)

    objective = vector_length * [0]

    # Apply the gaps
    polynomial_matrices = apply_gaps.apply(polynomial_matrices, args)

    # Run sdpb
    this_mode = 'optimization' if extract_spectrum else 'feasibility'
    sdpb_result = runsdpb.do_sdp(args, objective, normalization, polynomial_matrices,
                                 filename_base, mode=this_mode, ini_checkpoint=previous_checkpoint
                                 )

    stdout_file, out_file, in_folder, out_folder = sdpb_result

    # Extract functional if needed
    if extract_spectrum:
        get_spectrum.extract(args, in_folder, out_folder)

    text = parse_sdpb_stdout(stdout_file)
    if text == 'found dual feasible solution':
        return in_folder, True
    elif text == 'found primal feasible solution':
        return in_folder, False
    elif text == 'found primal-dual optimal solution':
        if not extract_spectrum:
            # this is because param.sdpb doesn't have dualFeasible and primalFeasible as termination reasons
            print_monitor('Warning: this is a feasibility run but sdpb was looking for a primal-dual optimal solution.')
        return in_folder, True
    else:
        print_monitor('Error: unexpected output from sdpb: ' + text)
        sys.exit(1)


def maximize(args, sign, filename_base, extract_spectrum=False):
    """Calls sdpb and returns the maximal value of the objective"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # Load the vectors from the dumpsave
    vector_length, polynomial_matrices, identity_block = dumpsave.get_polymatrices(args)

    op = args.ope.irrep
    spin = args.ope.spin
    delta = args.ope.gap

    # The way I do it is α[id] = objective and α[op] = norm
    objective = identity_block

    # Operator whose OPE needs to be bounded
    if polynomial_matrices is None:
        vector_file = os.path.join(args.sdpfiles, 'pickles', f'temp_j{args.job}', f'polymatrix_{op}-{spin}.pickle')
        try:
            with open(vector_file, 'rb') as f:
                ope_polymatrix = pickle.load(f)
                normalization = (sign * ope_polymatrix).full_evaluate(delta)
        except FileNotFoundError:
            print_monitor(f'Error: file {vector_file} not found, could not load operator')
            sys.exit(1)
        except EOFError:
            print_monitor(f'Error: file {vector_file} seems to be corrupted, try removing the folder '
                          f'{os.path.dirname(vector_file)}')
            sys.exit(1)
    else:
        try:
            normalization = (sign * polynomial_matrices[op][spin]).full_evaluate(delta)
        except KeyError:
            print_monitor(f'Error: Operator {op}_{spin} not present in the crossing equations')
            sys.exit(1)

    # Apply the gaps
    polynomial_matrices = apply_gaps.apply(polynomial_matrices, args)

    # We do not need to divide by the prefactor because it is already included in the evaluated block
    normalization = dot_with_lambda(normalization, args.ope.angle, args)

    # Run sdpb
    sdpb_result = runsdpb.do_sdp(args, objective, normalization, polynomial_matrices,
                                 filename_base, mode='optimization', ini_checkpoint=None
                                 )

    stdout_file, out_file, in_folder, out_folder = sdpb_result

    # Extract functional if needed
    if extract_spectrum:
        get_spectrum.extract(args, in_folder, out_folder)

    text_dict = parse_sdpb_out(out_file)
    text = text_dict.get('terminateReason', 'not found!')
    if text == 'found primal-dual optimal solution':
        # return the objective parsed from the out file
        return mp.mpf(text_dict['dualObjective'])
    elif text == 'maxComplementarity exceeded':
        # if the problem is unbounded sdpb will terminate after maxComplementarity reaches its threshold
        print_monitor('Warning: max complementarity exceeded, the problem is likely unbounded')
        return None
    elif text == 'found primal feasible solution' or text == 'found dual feasible solution':
        # this is because paramOPE.sdpb has primalFeasible and dualFeasible as termination reasons
        print_monitor('Error: this is a maximization run but sdpb exited as soon as a solution was found.')
        sys.exit(1)
    else:
        print_monitor('Error: unexpected output from sdpb: ' + text)
        sys.exit(1)
