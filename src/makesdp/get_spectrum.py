import sys
import os
import datetime
import subprocess
import re
import json
from mpmath import mp

import monitor
from classes import myJSON
from utilities import params


def n_to_str(number):
    return ('{0:.' + str(params.RESULT_PRECISION) + 'g}').format(number)


def extract(args, in_folder, out_folder):
    """Computes the zeros of a functional and puts the result in the results folder"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    in_file = in_folder.rstrip('/\\') + '.xml'

    # unisolve can be given as a path to its folder or to its file. It it's not given we assume it can be found in PATH
    if args.unisolve_bin is None:
        unisolve = 'unisolve'
    else:
        if os.path.isdir(args.unisolve_bin):
            unisolve = os.path.join(args.unisolve_bin, 'unisolve')
        else:
            unisolve = args.unisolve_bin

    # spectrum.py can be given as a path to its folder or to its file. It it's not given we assume it can be found in PATH
    if args.spectrum_script is None:
        spectrum_py = 'spectrum.py'
    else:
        if os.path.isdir(args.spectrum_script):
            spectrum_py = os.path.join(args.spectrum_script, 'spectrum.py')
        else:
            spectrum_py = args.spectrum_script

    path = os.path.join(args.sdpfiles, 'pickles', f'vector_order_j{args.job}.txt')
    try:
        with open(path, 'r') as f:
            vector_order = eval(f.read())
    except FileNotFoundError:
        print_monitor(f'Error: file with mapping "order : (name, spin, Delta-L)" not found in {path}')
        sys.exit(1)
    except (NameError, SyntaxError) as e:
        print_monitor(f'Error: file vector_order.txt corrupted: {e}')
        sys.exit(1)

    ##########################################################
    # Executing spectrum extraction script                   #
    ##########################################################

    hash_now = format(hash(datetime.datetime.now()) & 0xffffffff, 'x')
    spectrum_file = os.path.join(args.output, f'spectrum-job{args.job}_{hash_now}.m')
    stdout_file = os.path.join(args.sdpfiles, f'spectrum_job{args.job}_{hash_now}.log')
    json_file = os.path.join(args.output, f'spectrum-job{args.job}.json')

    commands_list = [
        spectrum_py,
        '-s', in_file,
        '-o', out_folder,
        '-m', spectrum_file,
        '-p', str(args.precision),
        '-z', args.functional,
        '-u', unisolve
    ]

    print_monitor(f'Extracting spectrum, see {stdout_file}')

    with open(stdout_file, 'w+') as stdo:
        try:
            spectrum_process = subprocess.run(commands_list, stdout=stdo, stderr=stdo)
        except FileNotFoundError:
            print_monitor('Error: python script spectrum.py not found')
            sys.exit(1)

    if spectrum_process.returncode != 0:
        print_monitor(f'Error: The execution of spectrum.py has produced some errors. Check {stdout_file}')
        sys.exit(1)

    ##########################################################
    # Modifying the output to contain readable vectors       #
    ##########################################################

    def format_vector(number):
        irrep, spin, _ = vector_order.get(number, ('not_found', 0, 0))
        return 'vector["{}",{}] -> '.format(irrep, spin)

    # The zeros are saved as {zero, {error[, error...]}}. We keep only the zeros
    discard_errorbars_regex = r'{(NUM),\s*{(NUM,)*NUM}\s*}'.replace('NUM', r'\s*-?\d+\.\d+\s*')

    def shift_zero(number):
        """Shifts the zero in x so that it becomes a zero in Delta"""

        def funct(match):
            zero_x = mp.mpf(match.group(1))
            _, __, delta_minus_x = vector_order.get(number, ('not_found', 0, 0))
            zero_delta = zero_x + mp.mpf(delta_minus_x)
            return mp.nstr(zero_delta, mp.prec)

        def id(match):
            return match.group(0)

        if number is None:
            return id
        else:
            return funct

    try:
        with open(spectrum_file, 'r') as f:
            content = f.read()
            new_content = ''
            position = 0
            entries = re.finditer(r'((\d+)\s*->)', content)
            n = None
            for entry in entries:
                new_n = int(entry.group(2))

                # Every time we see n -> ... (or the end) we apply the replacements to the *previous* entry
                new_content += re.sub(
                    discard_errorbars_regex,
                    shift_zero(n),
                    content[position:entry.start()]
                ) + format_vector(new_n)
                n = new_n
                position = entry.end()

            new_content += re.sub(
                discard_errorbars_regex,
                shift_zero(n),
                content[position:]
            )

    except FileNotFoundError:
        print_monitor(f'Error: The spectrum file {spectrum_file} disappeared')
        sys.exit(1)
    except TypeError as e:
        print_monitor(f'Error: {e}')
        sys.exit(1)
    else:
        with open(spectrum_file, 'w') as f:
            f.write(f'(* Spectrum file. DO NOT move or rename this file! It is referenced by {json_file} *)\n\n\n')
            f.write(new_content)

    # Gather the data to be written in the result file and then dump it in json format
    necessary_args = {'spectrum_from': {k: getattr(args, k, None) for k in ['to_bisect', 'assumptions', 'Lambda', 'ope']}}
    necessary_args.update({
        'point': [n_to_str(x) for x in args.job_param],
        'threshold': args.functional,
        'spectrum_file': spectrum_file
    })
    try:
        necessary_args['spectrum_from']['to_bisect'].gap = None
    except (KeyError, AttributeError) as e:
        pass

    output = json.dumps(necessary_args, cls=myJSON.MyEncoder)

    with open(json_file, 'w+') as j:
        j.write(output)

    print_monitor(f'Spectrum extracted.')
