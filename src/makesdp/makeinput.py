import json
import os
import sys
import subprocess
import zipfile

from mpmath import mp

import monitor
from classes.polynomials import Polynomial
from classes.polynomial_matrix import DiscreteDeltaCollection, PolynomialMatrixWithPrefactor


def sdpb_input(args, filename, archive_name, objective, normalization, polynomial_matrices):
    """Write the input to sdpb"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    def reshuffle_with_norm(vector):
        """ Function for solving the normalization constraint by removing one component
            In detail: finds v' such that a . v = First[v'] + a' . Rest[v'] when normalization . a == 1,
            where a' is a vector of length one less than a"""
        def max_index_by(l):
            return max(enumerate(l), key=lambda el: abs(el[1]))[0]

        j = max_index_by(normalization)
        if normalization[j] == 0:
            print_monitor('Warning: found entirely zero vector. Could not normalize to 1.')
            const = vector[j]
        else:
            # Here we multiply by 1/normalization because division is not implemented in Polynomial
            const = (1 / normalization[j]) * vector[j]
        vector_diff = [vector[i] - normalization[i] * const for i in range(len(vector))]
        return [const] + vector_diff[:j] + vector_diff[j + 1:]

    def sdpb_input_xml():
        """Writes the xml file to be given as input to sdpb"""

        print_monitor(f'Writing xml to {filename}')

        mp.prec = args.precision

        # Writes a single node to the file. Children is a function that writes the children nodes
        def node(name, children):
            stream.write(f'<{name}>')
            children()
            stream.write(f'</{name}>\n')

        # Now we define some nodes
        def real(r):
            def fun():
                stream.write(mp.nstr(r, int(args.precision * 0.301), min_fixed=-mp.inf, max_fixed=mp.inf))
            return fun

        def integ(i):
            def fun():
                stream.write(str(i))
            return fun

        def vector(v):
            def fun():
                for element in v:
                    node('elt', real(element))
            return fun

        def polynomial(p):
            # Here we handle the cases where the entry is a polynomial, a list of coefficients or a single mpf
            def fun():
                if isinstance(p, Polynomial):
                    for coeff in p.coeffs:
                        node('coeff', real(coeff))
                elif isinstance(p, mp.mpf):
                    node('coeff', real(p))
                elif hasattr(p, '__iter__'):
                    for coeff in p:
                        node('coeff', real(coeff))
                else:
                    raise TypeError(f'Error: Polynomial of type {type(p)} not supported')
            return fun

        def polynomial_vector(pv):
            def fun():
                for poly in pv:
                    node('polynomial', polynomial(poly))
            return fun

        # The parameter pvm is an instance of PolynomialVectorMatrix
        def polynomial_vector_matrix(pvm):
            def fun():
                degree = pvm.max_exponent
                sample_points = pvm.get_Laguerre_sample(degree+1)
                sample_scalings = [pvm.prefactor.evaluate_x(a) for a in sample_points]
                try:
                    bilinear_basis = pvm.prefactor.get_polynomials(degree//2)
                except ValueError:
                    print_monitor('Error: matrix of bilinear forms either not positive semidefinite or singular.')
                    sys.exit(1)

                def pvs():
                    for row in pvm.polymatrix:
                        for pv in row:
                            node('polynomialVector', polynomial_vector(reshuffle_with_norm(pv)))

                node('rows', integ(len(pvm.polymatrix)))
                node('cols', integ(len(pvm.polymatrix[0])))
                node('elements', pvs)
                node('samplePoints', vector(sample_points))
                node('sampleScalings', vector(sample_scalings))
                node('bilinearBasis', polynomial_vector(bilinear_basis))

            return fun

        # If we have a DiscreteDeltaCollection we have to loop over the grid and do a variation of polynomial_vector_matrix
        def discrete_vector_matrix(entry, ddc):
            def fun():
                degree = 0
                sample_points = ddc.get_Laguerre_sample(degree + 1)
                sample_scalings = [ddc.prefactor.evaluate_x(a) for a in sample_points]
                try:
                    bilinear_basis = ddc.prefactor.get_polynomials(degree // 2)
                except ValueError:
                    print_monitor('Error: matrix of bilinear forms either not positive semidefinite or singular.')
                    sys.exit(1)

                def pvs():
                    for row in entry.matrix:
                        for pv in row:
                            node('polynomialVector', polynomial_vector(reshuffle_with_norm(pv.polyvector)))

                node('rows', integ(len(entry.matrix)))
                node('cols', integ(len(entry.matrix[0])))
                node('elements', pvs)
                node('samplePoints', vector(sample_points))
                node('sampleScalings', vector(sample_scalings))
                node('bilinearBasis', polynomial_vector(bilinear_basis))

            return fun

        # Lastly we define the main node
        def main_node():

            def pvms():
                for pvm in polynomial_matrices:
                    # a PolynomialVectorMatrix is going to be None if it is given with the gap assumption "skip"
                    if not pvm.skip:
                        if isinstance(pvm, PolynomialMatrixWithPrefactor):
                            node('polynomialVectorMatrix', polynomial_vector_matrix(pvm))
                        elif isinstance(pvm, DiscreteDeltaCollection):
                            try:
                                for entry in pvm.shifted_vector_list():
                                    node('polynomialVectorMatrix', discrete_vector_matrix(entry, pvm))
                            except ValueError as e:
                                print_monitor(f'Error: {e}')
                                sys.exit(1)
                        else:
                            print_monitor(f'Error: cannot dump xml of object of type {type(pvm)}')
                            sys.exit(1)

            node('objective', vector(reshuffle_with_norm(objective)))
            node('polynomialVectorMatrices', pvms)
            print(', done.')

        # Finally we call the main node
        with open(filename, 'w+') as stream:
            node('sdp', main_node)

        # Now we convert the xml into the desired input for sdpb
        print_monitor(f'Converting xml to pvm format into {archive_name}')

        # If args.sdpb_bin is not provided we assume that the executable can be found in the PATH environment variable
        if args.sdpb_bin is not None:
            executable = os.path.join(args.sdpb_bin, 'pvm2sdp')
        else:
            executable = 'pvm2sdp'

        pvm_proc = subprocess.run([executable, str(args.precision), filename, archive_name], capture_output=True)
        if pvm_proc.returncode != 0:
            log_file = os.path.join(args.sdpfiles, f'pvm2sdp_error_job{args.job}.log')
            with open(log_file, 'w+b') as f:
                f.write(pvm_proc.stderr)
            sys.stderr.write(pvm_proc.stderr.decode('utf-8'))
            print_monitor(f'Error: pvm2sdp has produced some errors. Check {log_file}')
            sys.exit(1)

    def sdpb_input_zip():
        """Writes the zip archive to be given as input to sdpb"""
        counter = 0

        dirname = os.path.dirname(archive_name)

        def add_file(name, content):
            """Adds a file to the archive"""
            full_name = os.path.join(dirname, name)
            with open(full_name, 'w') as tmp:
                json.dump(content, tmp, indent=2)

            try:
                z.write(full_name, arcname=name)
            except IOError as e:
                print_monitor(f'Error: I/O error while zipping file {name}: {e}')
                sys.exit(1)
            finally:
                os.remove(full_name)

        def real(r):
            return mp.nstr(r, int(args.precision * 0.301), min_fixed=-mp.inf, max_fixed=mp.inf)

        def polynomial(p):
            # Here we handle the cases where the entry is a polynomial, a list of coefficients or a single mpf
            if isinstance(p, Polynomial):
                return p
            elif isinstance(p, mp.mpf):
                return Polynomial([p])
            elif hasattr(p, '__iter__'):
                return Polynomial(list(p))
            else:
                raise TypeError(f'Error: Polynomial of type {type(p)} not supported')

        def polynomial_vector_matrix(pvm):
            degree = pvm.max_exponent
            sample_points = pvm.get_Laguerre_sample(degree + 1)
            sample_scalings = [pvm.prefactor.evaluate_x(a) for a in sample_points]
            try:
                bilinear_basis = pvm.prefactor.get_polynomials(degree // 2)
            except ValueError:
                print_monitor('Error: matrix of bilinear forms either not positive semidefinite or singular.')
                sys.exit(1)

            the_bases_even = []
            the_bases_odd = []

            for poly in bilinear_basis:
                the_bases_even.append([])
                the_bases_odd.append([])
                for pt, scale in zip(sample_points, sample_scalings):
                    poly_eval = mp.sqrt(scale) * Polynomial(poly).eval(pt)
                    the_bases_even[-1].append(real(poly_eval))
                    the_bases_odd[-1].append(real(mp.sqrt(pt) * poly_eval))

            the_c = []
            the_B = []
            for r, row in enumerate(pvm.polymatrix):
                for pv in row[r:]:
                    reshuffled = reshuffle_with_norm(pv)
                    for pt, scale in zip(sample_points, sample_scalings):
                        gen = (scale * polynomial(r).eval(pt) for r in reshuffled)
                        the_c.append(real(next(gen)))
                        the_B.append([real(-g) for g in gen])

            return {
                'dim': len(pvm.polymatrix),
                'num_points': degree + 1,
                'bilinear_bases_even': the_bases_even,
                'bilinear_bases_odd': the_bases_odd[:(degree + 1) // 2],
                'c': the_c,
                'B': the_B
            }

        def discrete_vector_matrix(entry, ddc):
            sample_points = ddc.get_Laguerre_sample(1)
            sample_scalings = [ddc.prefactor.evaluate_x(a) for a in sample_points]

            the_c = []
            the_B = []
            for r, row in enumerate(entry.matrix):
                for pv in row[r:]:
                    reshuffled = reshuffle_with_norm(pv.polyvector)
                    for pt, scale in zip(sample_points, sample_scalings):
                        gen = (scale * polynomial(r).eval(pt) for r in reshuffled)
                        the_c.append(real(next(gen)))
                        the_B.append([real(-g) for g in gen])

            return {
                'dim': len(entry.matrix),
                'num_points': 1,
                'bilinear_bases_even': [['1']],
                'bilinear_bases_odd': [],
                'c': the_c,
                'B': the_B
            }

        print_monitor(f'Writing sdpb input to {archive_name}')

        with zipfile.ZipFile(archive_name, 'w') as z:

            write_obj = (real(b) for b in reshuffle_with_norm(objective))
            add_file('objectives.json', {
                'constant': next(write_obj),
                'b': list(write_obj)
            })

            for pvm in polynomial_matrices:
                if isinstance(pvm, PolynomialMatrixWithPrefactor):
                    add_file(f'block_{counter}.json', polynomial_vector_matrix(pvm))
                    counter += 1
                elif isinstance(pvm, DiscreteDeltaCollection):
                    try:
                        for entry in pvm.shifted_vector_list():
                            add_file(f'block_{counter}.json', discrete_vector_matrix(entry, pvm))
                            counter += 1
                    except ValueError as e:
                        print_monitor(f'Error: {e}')
                        sys.exit(1)
                else:
                    print_monitor(f'Error: cannot convert object of type {type(pvm)}')
                    sys.exit(1)

            add_file('control.json', {
                'num_blocks': counter,
                'command': f"sailboot.makesdp.makeinput.sdpb_input"
            })

    if args.legacy_input:
        sdpb_input_xml()
    else:
        sdpb_input_zip()
