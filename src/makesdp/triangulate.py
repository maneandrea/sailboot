from mpmath import mp
import itertools

from classes.points import Point
from utilities import params

# Precision of the computation for the Delaunay triangulation
mp.dps = params.POINT_PRECISION


def delaunay(list_allowed, list_disallowed):
    """Performs the Delaunay triangulation of the given points and returns new points to check together with the
    minimal distance between them"""

    # List of tuples where 1 means allowed and 0 means disallowed
    all_points = [(1, Point(a)) for a in list_allowed] + [(0, Point(d)) for d in list_disallowed]
    # Sort by coordinates
    all_points.sort(key=lambda t: t[1])

    # The triangulation is a list of triplets of indices based on the order of the above list
    triangulation, points, edges = guibas_stolfi(all_points)

    new_points = []
    for triangle in triangulation:
        central = get_central_point(triangle, all_points)
        if central:
            new_points.append(central)

    # We return the list of new points decreasingly sorted by the sqrt(area) of the triangle they came from
    return [p for p in sorted(new_points, key=lambda x: x[1], reverse=True)]


def guibas_stolfi(sorted_points, offset=0):
    """Guibas-Stolfi 'divide and conquer' algorithm"""

    l = len(sorted_points)
    if l > 3:

        # We make sure that we cut when the x coordinate increases and not in the middle of a column
        cut = None
        previous_x = sorted_points[l // 2 - 1][1].coords[0]
        for c, p in enumerate(sorted_points[l // 2:]):
            if p[1].coords[0] != previous_x:
                cut = c + l // 2
                break

        if cut is None:
            # The list is comprised of points on a vertical line
            return guibas_stolfi_vertical(sorted_points, offset=offset)
        else:
            # Otherwise we do the divide-and-conquer step
            half_left = sorted_points[:cut]
            half_right = sorted_points[cut:]

        left_data = guibas_stolfi(half_left, offset)
        right_data = guibas_stolfi(half_right, offset + cut)

        # left/right_data contains triangulation, point_dict and edge_dict
        # triangulation is a set {(vertex1, vertex2, vertex3), ...} of all triangles
        # point_dict is a dictionary {vertex: {points connected to it} ... }
        # edge_dict is a dictionary {(vertex1, vertex2): {points in the triangle with vertex1-vertex2}, ...}
        # It is important that the keys of the dict and the set are ordered

        return merge(left_data, right_data, sorted_points, offset)

    elif l == 3:
        return {(offset, offset+1, offset+2)}, {
            offset: {offset+1, offset+2},
            offset+1: {offset, offset+2},
            offset+2: {offset, offset+1}
        }, {
            (offset, offset+1): {offset+2},
            (offset, offset+2): {offset+1},
            (offset+1, offset+2): {offset},
        }

    elif l == 2:
        return set(), {offset: {offset+1}, offset+1: {offset}}, {(offset, offset+1): set()}
    else:
        return set(), {offset: set()}, {}


def guibas_stolfi_vertical(sorted_points, offset=0):
    """Performs the Delaunay triangulation for a set of points on the same vertical line"""

    point_dict = {}
    edge_dict = {}
    point_dict[offset] = {offset + 1}
    for p in range(offset + 1, len(sorted_points) + offset - 1):
        point_dict[p] = {p - 1, p + 1}
        edge_dict[(p, p + 1)] = set()
    edge_dict[(offset, offset + 1)] = set()
    point_dict[len(sorted_points) + offset - 1] = {len(sorted_points) + offset - 2}

    return set(), point_dict, edge_dict


def merge(left_data, right_data, points, offset):
    """Performs the merge part in the Guibas-Stolfi algorithm"""

    left, left_point_dict, left_edge_dict = left_data
    right, right_point_dict, right_edge_dict = right_data

    points_left = list(left_point_dict.keys())
    points_right = list(right_point_dict.keys())

    point_dict = left_point_dict | right_point_dict
    edge_dict = left_edge_dict | right_edge_dict
    triangles = left.union(right)

    ##########################################################
    # Helper functions                                       #
    ##########################################################

    def get_point(n):
        """Takes the referenced point from the list of points and returns a Point instance"""
        return points[n - offset][1]

    def find_base_edge():
        """Finds the bottom-most, not intersecting, left-right edge"""

        start_left = points_left[0]
        start_right = points_right[-1]

        def angle_with_normal(base, new, normal, maximize=False):
            sign = -1 if maximize else 1
            normal_point = get_point(base) + normal
            return normal_point.angle_alternative(get_point(new), get_point(base), sign=sign)

        def go_clockwise(start):
            """Generator that goes clockwise in the convex hull starting from the rightmost vertex"""

            previous = start
            current = start
            # vector pointing outside the convex hull, continuously updated
            normal = Point((1, 1))

            # Set of visited points.
            # First of all, we should never get to the end, and in the event, we should get
            # back to the starting point. This is just a failsafe to never run indefinitely
            visited = set()

            def choose():
                nonlocal previous
                nonlocal current
                nonlocal normal
                # Choose the one with maximal angle with the normal
                previous = current
                try:
                    current = max(point_dict[current], key=lambda x: angle_with_normal(current, x, normal, maximize=True))
                except ValueError:
                    raise StopIteration
                normal = (get_point(current) - get_point(previous)).rotate_90()
                visited.add(previous)

            choose()
            yield previous, current
            # Normally we should find a candidate earlier, this is a failsafe
            while current not in visited:
                choose()
                yield previous, current

        def go_counterclockwise(start):
            """Generator that goes counterclockwise in the convex hull starting from the leftmost vertex"""

            previous = start
            current = start
            # vector pointing outside the convex hull, continuously updated
            normal = Point((-1, -1))

            # Set of visited points.
            # First of all, we should never get to the end, and in the event, we should get
            # back to the starting point. This is just a failsafe to never run indefinitely
            visited = set()

            def choose():
                nonlocal previous
                nonlocal current
                nonlocal normal
                # Choose the one with minimal angle with the normal
                previous = current
                try:
                    current = min(point_dict[current], key=lambda x: angle_with_normal(current, x, normal))
                except ValueError:
                    raise StopIteration
                normal = (get_point(current) - get_point(previous)).rotate_minus90()
                visited.add(previous)

            choose()
            yield previous, current
            # Normally we should find a candidate earlier, this is a failsafe
            while current not in visited:
                choose()
                yield previous, current

        left_gen = go_counterclockwise(start_left)
        right_gen = go_clockwise(start_right)

        try:

            l_current, l_next = next(left_gen)
            r_current, r_next = next(right_gen)

            while True:
                # Here we look for the edge by walking along the convex hull
                # We go clockwise for the right half and counterclockwise for the left half
                pprint(f'Looking for base edge {l_current}-{r_current}')
                if get_point(r_current).counter_clockwise(get_point(l_current), get_point(l_next), resolve_ties=1):
                    pprint(f'Found that {r_current} is to the left of {l_current}-{l_next}')
                    l_current, l_next = next(left_gen)
                elif get_point(l_current).counter_clockwise(get_point(r_next), get_point(r_current)):
                    pprint(f'Found that {l_current} is to the right of {r_current}-{r_next}')
                    r_current, r_next = next(right_gen)
                    # If we update the right we start over the generator of the left
                    left_gen = go_counterclockwise(start_left)
                    l_current, l_next = next(left_gen)
                else:
                    pprint(f'Accepted base edge {l_current} - {r_current}')
                    return l_current, r_current

        except (StopIteration, RuntimeError):
            print('Warning: base edge not found, using O(n^2) algorithm to find it')
            # If the base edge could not be found, we search for it the hard way (which makes the algorithm O(n^2))
            # This should not happen and it's just a failsafe
            midpoint_x = (get_point(points_left[-1]).coords[0] + get_point(points_right[0]).coords[0]) / 2
            candidate_edges = []
            for l_edge, r_edge in itertools.product(points_left, points_right):
                xa, ya = get_point(l_edge).coords
                xb, yb = get_point(r_edge).coords
                if xa == xb:
                    candidate_edges.append(((l_edge, r_edge), (-mp.inf, (yb-ya)**2)))
                else:
                    midpoint_y = (xa*yb - xb*ya + midpoint_x*(ya - yb)) / (xa - xb)
                    candidate_edges.append(((l_edge, r_edge), (midpoint_y, (yb-ya)**2 + (xb-xa)**2)))
            pprint(f'out of {points_left}, {points_right} I selected the edge {min(candidate_edges, key=lambda x: x[1])[0]}')
            return min(candidate_edges, key=lambda x: x[1])[0]

    def remove_edges(removal_list):
        """Removes the triangle associated to the given candidate updates the dicts accordingly"""

        # We may have a list of objects scheduled for removal, we do them one by one
        for edge1, edge2 in removal_list:
            point_dict[edge1].remove(edge2)
            point_dict[edge2].remove(edge1)

            v1, v2 = tuple(sorted((edge1, edge2)))

            for extra in edge_dict[(v1, v2)]:
                to_remove = tuple(sorted((v1, v2, extra)))
                try:
                    triangles.remove(to_remove)
                except KeyError:
                    pass

            del edge_dict[(v1, v2)]

    def add_triangle(triangle):
        """Adds the given triangle to the list and updates the dicts accordingly"""

        triangles.add(tuple(sorted(triangle)))
        v1, v2, v3 = sorted(triangle)
        point_dict[v1] = point_dict[v1].union({v2, v3})
        point_dict[v2] = point_dict[v2].union({v1, v3})
        point_dict[v3] = point_dict[v3].union({v1, v2})

        edge_dict[(v1, v2)] = edge_dict.get((v1, v2), set()).union({v3})
        edge_dict[(v2, v3)] = edge_dict.get((v2, v3), set()).union({v1})
        edge_dict[(v1, v3)] = edge_dict.get((v1, v3), set()).union({v2})

    ##########################################################
    # Main loop: find base edge, submit candidates, accept   #
    ##########################################################

    def pprint(*text):
        #print(*text)
        pass

    def criterion_1(candidate):
        """Tells whether criterion 1 for accepting a candidate is satisfied (i.e. the angle is smaller than 180 deg)"""
        return 0 < candidate[1][0] < int(100000 * mp.pi)

    def criterion_2(candidate, base_left, base_right, candidates_list, pos):
        """Tells whether criterion 2 for accepting a candidate is satisfied (i.e. next not in circumscribed circle)"""
        try:
            next_point = candidates_list[pos + 1][0]
        except IndexError:
            # No next potential candidate
            return True
        else:
            return not is_inside_circle(
                get_point(candidate[0]),
                get_point(base_left),
                get_point(base_right),
                get_point(next_point)
            )

    pprint('-----------Searching base edge-----------')

    base_l, base_r = find_base_edge()
    candidates_available = True
    at_least_one_candidate = False

    pprint('----------------New merge----------------')

    while candidates_available:

        pprint(f'Base edge: {base_l} - {base_r}')

        # Save here the candidates found
        newly_found_left = None
        newly_found_right = None

        # Save here the edges to be removed
        to_remove_left = []
        to_remove_right = []

        # Sort candidates by the the angle with the base edge. Take only those that are connected to the base
        # The candidates contain (point, angle from base)
        candidates_left = []
        for p in point_dict[base_l]:
            if p != base_r:
                candidates_left.append((
                    p,
                    get_point(p).angle(get_point(base_r), get_point(base_l), two_pi_minus=True)
                ))
        # Filter by criterion 1, then sort by angle
        candidates_left = list(filter(criterion_1, candidates_left))
        candidates_left.sort(key=lambda x: x[1])
        pprint(candidates_left)

        candidates_right = []
        for p in point_dict[base_r]:
            if p != base_l:
                candidates_right.append((
                    p,
                    get_point(p).angle(get_point(base_l), get_point(base_r))
                ))
        # Filter by criterion 1, then sort by angle
        candidates_right = list(filter(criterion_1, candidates_right))
        candidates_right.sort(key=lambda x: x[1])
        pprint(candidates_right)

        # The candidates satisfy criterion 1, so we check for criterion 2
        for i, candidate in enumerate(candidates_left):
            if criterion_2(candidate, base_l, base_r, candidates_left, i):
                # Temporarily accept the candidate and break the loop
                newly_found_left = candidate[0]
                pprint(f'Found from left a candidate {newly_found_left}')
                break
            else:
                # Defer removal of the edge of this candidate until the left candidate is accepted
                pprint(f'Removing edge from left {base_l}, {candidate[0]}')
                # This tuple contains the first two points of the edge and the positions of the triangle they are in
                to_remove_left.append((candidate[0], base_l))

        # The candidates satisfy criterion 1, so we check for criterion 2
        for i, candidate in enumerate(candidates_right):
            if criterion_2(candidate, base_l, base_r, candidates_right, i):
                # Temporarily accept the candidate and break the loop
                newly_found_right = candidate[0]
                pprint(f'Found from right a candidate {newly_found_right}')
                break
            else:
                # Defer removal of the edge of this candidate until the right candidate is accepted
                pprint(f'Removing edge from right {base_r}, {candidate[0]}')
                # This tuple contains the first two points of the edge and the positions of the triangle they are in
                to_remove_right.append((candidate[0], base_r))

        if newly_found_left is None and newly_found_right is None:
            pprint('No candidates anymore, stopping')
            candidates_available = False
        elif newly_found_right is None and newly_found_left is not None:
            # Found only on the left
            remove_edges(to_remove_left)
            add_triangle((newly_found_left, base_l, base_r))
            base_l = newly_found_left
            at_least_one_candidate = True
        elif newly_found_right is not None and newly_found_left is None:
            # Found only on the right
            remove_edges(to_remove_right)
            add_triangle((newly_found_right, base_l, base_r))
            base_r = newly_found_right
            at_least_one_candidate = True
        else:
            # Found both check if one is inside the other
            at_least_one_candidate = True
            if is_inside_circle(get_point(base_r), get_point(base_l), get_point(newly_found_left),
                                get_point(newly_found_right)):
                # The right point is inside the circle, so we take the left
                pprint('Accepting right candidate')
                remove_edges(to_remove_right)
                add_triangle((newly_found_right, base_l, base_r))
                base_r = newly_found_right
            else:
                # We just assume that the other works out by the correctness of the algorithm
                pprint('Accepting left candidate')
                remove_edges(to_remove_left)
                add_triangle((newly_found_left, base_l, base_r))
                base_l = newly_found_left

    # If no candidates have been found at all, we submit the base edge anyway
    if not at_least_one_candidate:
        pprint('No candidate was submitted, but I will add the edge anyway')
        edge_dict[(base_l, base_r)] = edge_dict.get((base_l, base_r), set())
        point_dict[base_l] = point_dict.get(base_l, set()).union({base_r})
        point_dict[base_r] = point_dict.get(base_r, set()).union({base_l})

    pprint('Triangles: ' + str(triangles))
    pprint('Points: ' + str(point_dict))
    pprint('Edges: ' + str(edge_dict))
    return triangles, point_dict, edge_dict


def get_central_point(triangle, points):
    """Gets the central point of a triangle, returns None if vertices are all allowed or all disallowed"""

    def get_point(n):
        return points[triangle[n]][1], points[triangle[n]][0]

    try:
        p1, a1 = get_point(0)
        p2, a2 = get_point(1)
        p3, a3 = get_point(2)

        if a1 == 1 and a2 == 1 and a3 == 1:
            # All allowed
            return None
        elif a1 == 0 and a2 == 0 and a3 == 0:
            # All disallowed
            return None
        else:
            # We do not return it if the points are collinear
            ax, ay = p1.coords
            bx, by = p2.coords
            cx, cy = p3.coords
            d = ax * (by - cy) + bx * (cy - ay) + cx * (ay - by)
            if d == 0:
                return None
            else:
                # Here I just take the average of the points and return also the sqrt of 2*area (proxy for a distance)
                return ((p1+p2+p3)/3).coords, mp.sqrt(abs(d))

    except IndexError:
        # There should not be such triangles
        print('Warning: I found a degenerate triangle with two edges')

        p1, p2 = Point(points[triangle[0]][1]), Point(points[triangle[1]][1])
        a1, a2 = points[triangle[0]][0], points[triangle[1]][0]

        if a1 == 1 and a2 == 1:
            return None
        elif a1 == 0 and a2 == 0:
            return None
        else:
            return ((p1+p2)/2).coords


def is_inside_circle(vertex1, vertex2, vertex3, point):
    """Returns True if point is inside the interior of the circumscribed circle to vertex1,2,3"""

    ax, ay = vertex1.coords
    bx, by = vertex2.coords
    cx, cy = vertex3.coords
    px, py = point.coords

    d = 2*(ax*(by-cy)+bx*(cy-ay)+cx*(ay-by))

    if d == 0:
        print(f'Warning: I found a degenerate cicle: {point} in {vertex1}, {vertex2}, {vertex3}. '
              f'Returning False.')
        return False

    # Coordinates of circumcenter
    ux = ((ax**2+ay**2)*(by-cy)+(bx**2+by**2)*(cy-ay)+(cx**2+cy**2)*(ay-by))/d
    uy = ((ax**2+ay**2)*(cx-bx)+(bx**2+by**2)*(ax-cx)+(cx**2+cy**2)*(bx-ax))/d

    # Radius of circle and distance of point to it
    rsq = (ux-ax)**2+(uy-ay)**2
    ssq = (ux-px)**2+(uy-py)**2

    return ssq < rsq

