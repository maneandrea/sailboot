import json
import os

import monitor
from classes import *
from makesdp import readoutput
from utilities import params


def n_to_str(number):
    return ('{0:.' + str(params.RESULT_PRECISION) + 'g}').format(number)


def main_bisection(args):
    """Performs the bisection algorithm"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # If all points were allowed, we rerun it one
    # more times with the extremes increased
    all_disallowed = True
    all_allowed = True

    # Previous checkpoint for hotstarting from previous runs
    previous = None

    def inject_gap(gap):
        args.to_bisect.gap = gap

    def inject_param(param):
        args.replacement[args.to_bisect] = param

    # Are we bisecting over an operator dimension or over a parameter?
    if isinstance(args.to_bisect, gaps.Operator):
        info_string = 'Δ'
        inject_fn = inject_gap
    else:
        if args.replacement is None:
            args.replacement = {}
        info_string = args.to_bisect
        inject_fn = inject_param

    # String of parameters
    param_string = '-'.join(['{:.5g}'.format(par) for par in args.job_param])
    if len(args.job_param) == 0:
        param_show = ''
    elif len(args.job_param) == 1:
        param_show = ' with parameter {:.5g}'.format(args.job_param[0])
    else:
        param_show = ' with parameters ' + ', '.join(['{:.5g}'.format(par) for par in args.job_param])

    def do_bisection(this_lower, this_upper):
        """Usual bisection algorithm"""

        upper = this_upper
        lower = this_lower
        nonlocal all_allowed
        nonlocal all_disallowed
        nonlocal previous

        if args.hotstart:
            print_monitor(f'Hotstarting with threshold {args.hotstart}')

        while upper - lower > args.threshold:
            current = (upper + lower) / 2
            print_monitor('Checking {:s} = {:.6f}'.format(info_string, current))

            # Assign the gap to to_bisect
            inject_fn(current)

            # Start the run
            previous, run = readoutput.disallowed(
                args,
                filename_base='bisection_Delta-{:.5g}_params-'.format(current) + param_string,
                previous_checkpoint=None if upper - lower > args.hotstart else previous
            )
            if run:
                upper = current
                print_monitor('\tdisallowed')
                all_allowed = False
            else:
                lower = current
                print_monitor('\tallowed')
                all_disallowed = False

        return lower, upper

    print_monitor('Starting bisection for ' + str(args.to_bisect) + param_show)
    lower_bound, upper_bound = do_bisection(args.bisect_min, args.bisect_max)

    # Check if there was at least one of each
    if all_disallowed:
        new_lower = 2*args.bisect_min-args.bisect_max
        new_upper = args.bisect_min
        inject_fn(new_lower)

        print_monitor('All points have been found disallowed. Running one more time with ' +
                      f'min={new_lower} and max={new_upper}')
        previous, run = readoutput.disallowed(
            args,
            filename_base='bisection_Delta-{:.5g}_params-'.format(new_lower) + param_string,
            previous_checkpoint=None
        )
        if run:
            print_monitor('All points have been found disallowed again, '
                          '{:s} < {:.9f}. Run terminated.'.format(info_string, new_lower))
            return
        else:
            lower_bound, upper_bound = do_bisection(new_lower, new_upper)

    elif all_allowed:
        new_lower = args.bisect_max
        new_upper = 2*args.bisect_max-args.bisect_min
        inject_fn(new_upper)

        print_monitor('All points have been found allowed. Running one more time with ' +
                      f'min={new_lower} and max={new_upper}')
        previous, run = readoutput.disallowed(
            args,
            filename_base='bisection_Delta-{:.5g}_params-'.format(new_upper) + param_string,
            previous_checkpoint=None
        )
        if not run:
            print_monitor(f'All points have been found allowed again, '
                          '{:s} > {:.9f}. Run terminated.'.format(info_string, new_upper))
            return
        else:
            lower_bound, upper_bound = do_bisection(new_lower, new_upper)

    print_monitor('Bisection terminated for {0:s} in [{1:.9f},{2:.9f}]'.format(info_string, lower_bound, upper_bound))

    # Extract functional if needed
    if args.functional is not None:
        print_monitor('Now running one last time in optimization mode to extract the functional')
        inject_fn(upper_bound)
        readoutput.disallowed(
            args,
            filename_base='functional_Delta-{:.5g}_params-'.format(upper_bound) + param_string,
            previous_checkpoint=previous,
            extract_spectrum=True
        )

    # Gather the data to be written in the result file and then dump it in json format
    necessary_args = {k: getattr(args, k) for k in ['to_bisect', 'assumptions', 'Lambda']}
    necessary_args.update({'point': [n_to_str(x) for x in args.job_param],
                           'allowed': n_to_str(lower_bound), 'disallowed': n_to_str(upper_bound)})
    try:
        necessary_args['to_bisect'].gap = None
    except KeyError:
        pass

    output = json.dumps(necessary_args, cls=myJSON.MyEncoder)

    output_name = os.path.join(args.output, f'bisection-job{args.job}.json')

    with open(output_name, 'w+') as f:
        f.write(output)

    print_monitor('Written output in ' + output_name)
