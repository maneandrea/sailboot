import json
import os

import monitor
from classes import *
from makesdp import readoutput
from utilities import params


def n_to_str(number):
    return ('{0:.' + str(params.RESULT_PRECISION) + 'g}').format(number)


def main_feasibility(args):
    """Performs the feasibility run"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # String of parameters
    param_string = '-'.join(['{:.5g}'.format(par) for par in args.job_param])
    if len(args.job_param) == 0:
        param_show = ''
    elif len(args.job_param) == 1:
        param_show = ' with parameter {:.5g}'.format(args.job_param[0])
    else:
        param_show = ' with parameters ' + ', '.join(['{:.5g}'.format(par) for par in args.job_param])

    print_monitor('Starting feasibility run' + param_show)

    # Start the run
    _, disallowed = readoutput.disallowed(
        args,
        filename_base='feasibility_params-' + param_string,
        previous_checkpoint=None
    )

    if disallowed:
        allowed_string, allowed_num = 'disallowed', 0
    else:
        allowed_string, allowed_num = 'allowed', 1

    print_monitor('Feasibility run terminated: ' + allowed_string)

    # Extract functional is not available
    if args.functional is not None:
        print_monitor('Warning: spectrum mode not allowed for feasibility runs')

    # Gather the data to be written in the result file and then dump it in json format
    necessary_args = {k: getattr(args, k) for k in ['assumptions', 'Lambda']}
    necessary_args.update({'point': [n_to_str(x) for x in args.job_param],
                           'allowed': allowed_num, 'allowed_is': 1})

    output = json.dumps(necessary_args, cls=myJSON.MyEncoder)

    output_name = os.path.join(args.output, f'feasibility-job{args.job}.json')

    with open(output_name, 'w+') as f:
        f.write(output)

    print_monitor('Written output in ' + output_name)
