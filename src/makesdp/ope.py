import json
import os
from mpmath import mp

import monitor
from classes import *
from makesdp import readoutput
from utilities import params


def n_to_str(number):
    return mp.nstr(number, params.RESULT_PRECISION)


def main_ope(args):
    """Performs the OPE extremization algorithm"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    mp.prec = args.precision

    op = args.ope.irrep
    spin = args.ope.spin
    delta = mp.mpf(str(args.ope.gap))

    ope_upper_bound = None
    ope_lower_bound = None

    spectrum_q = True if args.functional is not None else False

    # String of parameters
    param_string = '-'.join(['{:.5g}'.format(par) for par in args.job_param])
    if len(args.job_param) == 0:
        param_show = ''
    elif len(args.job_param) == 1:
        param_show = ' with parameter {:.5g}'.format(args.job_param[0])
    else:
        param_show = ' with parameters ' + ', '.join(['{:.5g}'.format(par) for par in args.job_param])

    if 'u' in args.ope_bound:
        print_monitor(f'Looking for upper bound on {op}(Δ={mp.nstr(delta,3)},l={spin})' + param_show)
        ope_upper_bound = readoutput.maximize(
            args,
            1,
            filename_base=f'ope-upper_{op}_{mp.nstr(delta,3)}_{spin}_params-' + param_string,
            extract_spectrum=spectrum_q
        )
        if ope_upper_bound is not None:
            ope_upper_bound = - ope_upper_bound
            print_monitor(f'\tupper bound = {mp.nstr(ope_upper_bound,6)}')
        else:
            print_monitor(f'\tupper bound = ∞')

    if 'l' in args.ope_bound:
        print_monitor(f'Looking for lower bound on {op}(Δ={mp.nstr(delta,3)},l={spin})' + param_show)
        ope_lower_bound = readoutput.maximize(
            args,
            -1,
            filename_base=f'ope-lower_{op}_{mp.nstr(delta,3)}_{spin}_params-' + param_string,
            extract_spectrum=spectrum_q
        )
        if ope_lower_bound is not None:
            print_monitor(f'\tlower bound = {mp.nstr(ope_lower_bound, 6)}')
        else:
            print_monitor(f'\tlower bound = -∞')

    # Gather the data to be written in the result file and then dump it in json format
    necessary_args = {k: getattr(args, k) for k in ['ope', 'assumptions', 'Lambda']}
    necessary_args.update({'point': [n_to_str(x) for x in args.job_param]})
    if ope_lower_bound is not None:
        necessary_args['lower_bound'] = n_to_str(ope_lower_bound)
    if ope_upper_bound is not None:
        necessary_args['upper_bound'] = n_to_str(ope_upper_bound)

    output = json.dumps(necessary_args, cls=myJSON.MyEncoder)

    output_name = os.path.join(args.output, f'ope-job{args.job}.json')

    with open(output_name, 'w+') as f:
        f.write(output)

    print_monitor('Written output in ' + output_name)
