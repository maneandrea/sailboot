import os
import json
import sys
import time
from mpmath import mp

from utilities import simpleflock, params
from makesdp import triangulate
import monitor
from classes import myJSON

# Seconds between consecutive checks for new points during the idle state
IDLE_DELAY = 1


def n_to_str(number):
    return mp.nstr(mp.mpf(number), params.RESULT_PRECISION)


class QueueInteract:
    """Class for interacting with the queue file, provides a "with" interface"""

    def __init__(self, args):
        self.queue_file = os.path.join(args.sdpfiles, 'queue.txt')
        self.monitor_file = args.monitor_file
        self.job = args.job
        self._fd = None
        self._flock = None
        self._old_dict = None

    def print_monitor(self, text):
        print(text)
        monitor.write_to_monitor(self.monitor_file, self.job, text)

    def __enter__(self):
        self._flock = simpleflock.SimpleFlock(self.queue_file, timeout=180, mode='r+')
        self._fd = self._flock.__enter__()
        # We try to parse the content and if something goes wrong we save the corrupted file somewhere else
        try:
            self._old_dict = eval(self._fd.read(), {'mpf': mp.mpf})
        except (ValueError, SyntaxError) as err:
            self.print_monitor('Warning: Could not parse queue file ' + str(err))
            self.print_monitor('Saving old file in ' + self.queue_file.replace('queue.txt', f'queue_error_j{self.job}.txt'))
            with open(self.queue_file.replace('queue.txt', f'queue_error_j{self.job}.txt'), 'w+') as g:
                self._fd.seek(0)
                g.write('# There was an error in this queue.txt file. Here is what I could recover\n')
                g.write(self._fd.read())
                self._old_dict = {}
        finally:
            return self._old_dict

    def __exit__(self, *args):

        try:
            allowed = str(self._old_dict.get('allowed', []))
            disallowed = str(self._old_dict.get('disallowed', []))
            running = str(self._old_dict.get('running', {}))
            queued = str(self._old_dict.get('queued', []))
        except (TypeError, AttributeError):
            print('Error: Something went wrong')
            raise
        else:
            self._fd.truncate(0)
            self._fd.seek(0)
            self._fd.write('{\n')
            self._fd.write('"allowed": ' + allowed + ',\n')
            self._fd.write('"disallowed": ' + disallowed + ',\n')
            self._fd.write('"running": ' + running + ',\n')
            self._fd.write('"queued": ' + queued + '\n')
            self._fd.write('}\n')
        finally:
            self._flock.__exit__()


def setup(args):
    """Sets up the Delaunay search by creating the queue file"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    queue_file = os.path.join(args.sdpfiles, 'queue.txt')

    if not os.path.isfile(queue_file):
        # We create the queue.txt file for the first job who gets it, the others will wait until it's created
        with simpleflock.SimpleFlock(queue_file, timeout=10, mode='r+') as q:
            q.write('{\n')
            q.write('"allowed": [],\n')
            q.write('"disallowed": [],\n')
            q.write('"running": {},\n')
            q.write(f'"queued": [{tuple(args.initial)}]\n')
            q.write('}\n')
        print_monitor('Added initial point (' + ','.join(n_to_str(a) for a in args.initial) + ')')
    else:
        with QueueInteract(args) as old_dict:
            old_queue = old_dict.get('queued', [])
            old_dict['queued'] = old_queue + [tuple(args.initial)]

    main_delaunay(args)


def push_point(args):
    """Pushes one or more points to the queue"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    with QueueInteract(args) as old_dict:
        potential_points = triangulate.delaunay(old_dict['allowed'], old_dict['disallowed'])
        running = []
        for job, point in old_dict['running'].items():
            if os.path.isfile(os.path.join(args.sdpfiles, f'job-{job}-dead')):
                print_monitor(f'Removing job {job} since it stopped running')
                old_dict['running'][job] = None
            elif point:
                running.append(point)

        dont_resubmit = set(old_dict['queued'] + running)
        max_new_dist = -mp.inf
        submit_points = []
        for point, distance in potential_points:
            if distance > max_new_dist:
                max_new_dist = distance
            if distance > args.threshold and point not in dont_resubmit:
                submit_points.append(point)
                max_new_dist = distance
                break

        if len(submit_points) > 0:
            print_monitor('Added point (' + ','.join(n_to_str(n) for n in submit_points[0]) + '), '
                          f'minimal distance reached: {n_to_str(max_new_dist)}')
            old_dict['queued'] = old_dict.get('queued', []) + [submit_points[0]]
            # We return True for continuing and False to wait and retry
            return True
        else:
            if len(running) > 0:
                print_monitor('Found no points to add, will wait for jobs to complete')
                return False
            elif max_new_dist > 0:
                print_monitor(f'No more points to add, reached distance={n_to_str(max_new_dist)}')
                return True
            else:
                print_monitor('Queue depleted, failed to reach desired accuracy')
                return True


def pull_point(args):
    """Pulls a point from the queue and puts it into running"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    with QueueInteract(args) as old_dict:
        if old_dict['queued']:
            point = old_dict['queued'][0]
            old_dict['queued'] = old_dict['queued'][1:]
            old_dict['running'].update({args.job: tuple(point)})
        else:
            point = None

    if point is None:
        print_monitor('All points completed')
        return None
    else:
        return point


def complete_point(args, outcome):
    """Takes a completed point from the running dict to the list of completed ones"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    with QueueInteract(args) as old_dict:
        try:
            last_running = old_dict['running'][args.job]
        except KeyError:
            print_monitor('Warning: found no running points for this job, so I cannot move it to completed')
        else:
            old_dict['running'][args.job] = None

        if outcome:
            old_dict['disallowed'].append(last_running)
            print_monitor('\tdisallowed')
        else:
            old_dict['allowed'].append(last_running)
            print_monitor('\tallowed')


def execute_point(point, args):
    """Calls sdpb on a point to see if it is allowed or not"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    print_monitor('Checking point (' + ','.join([n_to_str(a) for a in point]) + ')')


def main_delaunay(args):
    """Main routine of the delaunay method. Loops until the queue is depleted"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # Points that have been checked
    checked_points = {'allowed': [], 'disallowed': []}

    current_point = pull_point(args)
    try:
        while current_point:

            # Execute this point to see if it is disallowed
            outcome = execute_point(current_point, args)
            complete_point(args, outcome)
            if outcome:
                checked_points['disallowed'].append(current_point)
            else:
                checked_points['allowed'].append(current_point)

            # Add new points to the queue, if any
            success = push_point(args)
            while not success:
                time.sleep(IDLE_DELAY)
                success = push_point(args)

            # Pull a new point and loop
            current_point = pull_point(args)

    except OSError:
        print_monitor('Warning: an error occurred, writing down what I found so far')
        open(os.path.join(args.sdpfiles, f'job-{args.job}-dead'), 'r+')

    finally:
        # Gather the data to be written in the result file and then dump it in json format
        necessary_args = {k: getattr(args, k) for k in ['assumptions', 'Lambda']}
        necessary_args.update({'delaunay': True,
                               'allowed': [[n_to_str(a) for a in p] for p in checked_points['allowed']],
                               'disallowed': [[n_to_str(a) for a in p] for p in checked_points['disallowed']]})

        output = json.dumps(necessary_args, cls=myJSON.MyEncoder)

        output_name = os.path.join(args.output, f'delaunay-job{args.job}.json')

        with open(output_name, 'w+') as f:
            f.write(output)

        print_monitor('Written output in ' + output_name)
