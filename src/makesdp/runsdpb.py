import os
import sys
import subprocess
import datetime

import monitor
from classes import polynomial_matrix
from makesdp import makeinput


def run_sdpb(args, input_archive, input_folder, output_folder, mode, ini_check_folder):
    """Runs an instance of sdpb and returns the path of the output files"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    # If args.sdpb_bin is not provided we assume that the executable can be found in the PATH environment variable
    if args.sdpb_bin is not None:
        executable = os.path.join(args.sdpb_bin, 'sdpb')
    else:
        executable = 'sdpb'

    if mode == 'feasibility':
        paramfile = args.paramfile[0]
    elif mode == 'optimization':
        paramfile = args.paramfile[-1]
    else:
        print_monitor(f'Warning: mode {mode} not known, using feasibility')
        paramfile = args.paramfile[0]

    # Define two files inside output_folder to redirect standard output and standard errors
    stdout_file = os.path.join(output_folder, 'stdout.log')
    stderr_file = os.path.join(output_folder, 'stderr.log')

    print_monitor(f'Executing sdpb. See {stdout_file}')

    # This is not specific to the SLURM workload manager. The mpi method can be specified in args.
    commands_list = [executable,
                     '-s', input_archive,
                     '-o', output_folder,
                     '-p', paramfile,
                     '--precision', str(args.precision),
                     '-c', input_folder]
    if ini_check_folder:
        commands_list += ['-i', ini_check_folder]

    # args.parallelize can be like 'mpirun', 'srun --mpi=pmi2' etc...
    if args.parallelize is not None:
        commands_list = [*(args.parallelize.split()), *commands_list]

    print(' '.join(commands_list) + '\n\t> ' + stdout_file + '\n\t2> ' + stderr_file)

    # Here we run sdpb
    with open(stdout_file, 'w+') as stdo, open(stderr_file, 'w+') as stde:
        sdpb_process = subprocess.run(commands_list, stdout=stdo, stderr=stde)

    if sdpb_process.returncode != 0:
        print_monitor(f'Error: The execution of sdpb has produced some errors. Check {stderr_file}')
        with open(stderr_file, 'r') as e:
            sys.stderr.write(e.read())
        sys.exit(1)
    else:
        # Return the file paths, which can be used by the functions in readoutput.py, and also the other folders
        return stdout_file, os.path.join(output_folder, 'out.txt'), input_folder, output_folder


def do_sdp(args, objective, normalization, polynomial_matrices, filename_base, mode='feasibility', ini_checkpoint=None):
    """First calls the function for generating the input and then calls an instance of sdpb"""

    # We append the hash of the date to the file in order to avoid possible name collisions
    hash_now = format(hash(datetime.datetime.now()) & 0xffffffff, 'x')
    input_filename = os.path.join(args.sdpfiles, f'{filename_base}_{hash_now}.xml')
    input_foldername = os.path.join(args.sdpfiles, f'{filename_base}_{hash_now}', '')
    output_foldername = os.path.join(args.sdpfiles, f'{filename_base}_{hash_now}_out', '')

    if args.legacy_input:
        input_archive = input_foldername
    else:
        input_archive = os.path.join(input_foldername, 'in.zip')

    for path_to_create in [input_foldername, output_foldername]:
        if not os.path.exists(path_to_create):
            os.makedirs(path_to_create)

    # Call make input so that all files are generated
    makeinput.sdpb_input(args, input_filename, input_archive, objective, normalization, polynomial_matrices)

    # Finally call run_sdpb and return
    return run_sdpb(args, input_archive, input_foldername, output_foldername, mode, ini_checkpoint)
