import pickle
import os
import sys
import json
import re
import glob
from multiprocessing import Pool
import decimal
from decimal import Decimal
import mpmath.libmp
from mpmath import mp
try:
    from sympy.parsing.mathematica import parse_mathematica as math_parser_bugged
except ModuleNotFoundError:
    print('Warning: sympy not found. You will not be able to parse expressions inside coeff[] wrappers')

import monitor
from classes import blocks, discrete_blocks, polynomial_matrix
from utilities import mathematica, params

mpmath_backend = mpmath.libmp.BACKEND
if mpmath_backend != 'sage':
    print(f'Warning: using mpmath backend {mpmath_backend}, consider sage for improving performance.')

decimal.getcontext().prec = params.EXTERNAL_DIM_PREC


def math_parser(expr):
    """function parse_mathematica from sympy with the bug of https://github.com/sympy/sympy/pull/24102 corrected"""
    expr_sanitized = re.sub(
        r'[αβγδεζηθικλμνξοπρστυφχψωΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ]',
        lambda m: {
            'α': 'Alpha',
            'β': 'Beta',
            'γ': 'Gamma',
            'δ': 'Delta',
            'ε': 'Epsilon',
            'ζ': 'Zeta',
            'η': 'Eta',
            'θ': 'Theta',
            'ι': 'Iota',
            'κ': 'Kappa',
            'λ': 'Lambda',
            'μ': 'Mu',
            'ν': 'Nu',
            'ξ': 'Xi',
            'ο': 'Omicron',
            'π': 'Pi',
            'ρ': 'Rho',
            'σ': 'Sigma',
            'τ': 'Tau',
            'υ': 'Upsilon',
            'φ': 'Phi',
            'χ': 'Chi',
            'ψ': 'Psi',
            'ω': 'Omega',
            'Α': 'CapitalAlpha',
            'Β': 'CapitalBeta',
            'Γ': 'CapitalGamma',
            'Δ': 'CapitalDelta',
            'Ε': 'CapitalEpsilon',
            'Ζ': 'CapitalZeta',
            'Η': 'CapitalEta',
            'Θ': 'CapitalTheta',
            'Ι': 'CapitalIota',
            'Κ': 'CapitalKappa',
            'Λ': 'CapitalLambda',
            'Μ': 'CapitalMu',
            'Ν': 'CapitalNu',
            'Ξ': 'CapitalXi',
            'Ο': 'CapitalOmicron',
            'Π': 'CapitalPi',
            'Ρ': 'CapitalRho',
            'Σ': 'CapitalSigma',
            'Τ': 'CapitalTau',
            'Υ': 'CapitalUpsilon',
            'Φ': 'CapitalPhi',
            'Χ': 'CapitalChi',
            'Ψ': 'CapitalPsi',
            'Ω': 'CapitalOmega'
        }[m.group(0)],
        expr
    )

    from sympy import Symbol

    def repl(n, expr):
        return expr.replace(Symbol(f'CapitalDelta{n}'), Symbol(f'Δ{n}'))

    return repl(1, repl(2, repl(3, repl(4, math_parser_bugged(expr_sanitized)))))


def initialize_pool():
    """Initializes a pool worker in such a way that signals do not trigger the handler of the main process"""
    import signal

    def graceful_exit(*args):
        sys.exit(0)

    signal.signal(signal.SIGTERM, graceful_exit)
    signal.signal(signal.SIGINT, graceful_exit)


def convolve_block_from_file(name_spin, filename_args_delta_phi):
    """Multiplies blocks by v^Δφ in a parallelized fashion"""

    name, spin, custom_name = name_spin
    filename, args, delta_phi = filename_args_delta_phi

    # Load the block
    if args.discrete_delta:
        block = discrete_blocks.Block(filename, args)
    else:
        block = blocks.Block(filename, args)
    if delta_phi is not None:
        block.convolve_block(delta_phi)

    # Save it as a pickle
    base_path = os.path.join(args.sdpfiles, 'pickles', f'temp_j{args.job}')
    file_path = os.path.join(base_path, f'convolved_block_{name}_{spin}.pickle')
    os.makedirs(base_path, exist_ok=True)
    with open(file_path, 'w+b') as f:
        pickle.dump(block, f)

    return name, spin, custom_name, file_path


def get_polymatrices(args):
    """Gets the vectors of the crossing equations and the identity"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    vectors_file = os.path.join(args.sdpfiles, 'pickles', 'dumpsave-vectors.pickle')
    identity_file = os.path.join(args.sdpfiles, 'pickles', 'dumpsave-identity.pickle')

    def identity_exists():
        """Check if the identity vector was computed already for this job and if it had the same external dimensions"""

        nonlocal identity_file

        base_path = os.path.join(args.sdpfiles, 'pickles', f'temp_j{args.job}')
        if os.path.isdir(base_path):
            if os.path.isfile(os.path.join(base_path, 'identity.pickle')):
                try:
                    with open(os.path.join(base_path, 'dims_and_repls.pickle'), 'rb') as e:
                        external_dims, replacements = pickle.load(e)
                        if external_dims == args.external_dim and replacements == args.replacement:
                            identity_file = os.path.join(base_path, 'identity.pickle')
                            return True
                        else:
                            return False
                except FileNotFoundError:
                    print_monitor(f'Warning: file dims_and_repls.pickle for job {args.job} was removed')
                    return False
            else:
                return False
        else:
            return False

    def vectors_exist():
        """Check if the vectors were computed already for this job and if it had the same external dimensions"""
        base_path = os.path.join(args.sdpfiles, 'pickles', f'temp_j{args.job}')
        if os.path.isdir(base_path):
            if glob.glob(os.path.join(base_path, 'polymatrix_*.pickle')):
                # This is not 100% safe: if some files, but not all, exist, this would still return True
                try:
                    with open(os.path.join(base_path, 'dims_and_repls.pickle'), 'rb') as e:
                        external_dims = pickle.load(e)[0]
                        return external_dims == args.external_dim
                except FileNotFoundError:
                    print_monitor(f'Warning: file dims_and_repls.pickle for job {args.job} was removed')
                    return False
            else:
                return False
        else:
            return False

    got_vectors = os.path.isfile(vectors_file) or vectors_exist()
    got_vectors_projectwide = os.path.isfile(vectors_file)
    got_identity = os.path.isfile(identity_file) or identity_exists()

    # If we have both dumpsaves don't compute anything and if we have the job saves we don't even return the vectors
    if got_identity and got_vectors:
        with open(identity_file, 'rb') as f:
            print_monitor(f'Loading identity vector from {identity_file}')
            vector_tot_length, identity_block_vector = pickle.load(f)
        if got_vectors_projectwide:
            with open(vectors_file, 'rb') as f:
                print_monitor(f'Loading polynomial matrix vectors from {vectors_file}')
                vector_tot_length, poly_matrices_with_prefactor = pickle.load(f)
        else:
            poly_matrices_with_prefactor = None

    # If we have only the vectors we load them (or set them to None) and then compute the identity
    elif not got_identity and got_vectors:
        if got_vectors_projectwide:
            with open(vectors_file, 'rb') as f:
                print_monitor(f'Loading polynomial matrix vectors from {vectors_file}')
                vector_tot_length, poly_matrices_with_prefactor = pickle.load(f)
        else:
            # If this is None, the vectors will be loaded later by apply_gaps.py
            poly_matrices_with_prefactor = None
        vector_tot_length, _, identity_block_vector = make_dumpsave(
            args, do_identity=True, do_vectors=False)

    # This should not happen
    elif got_identity and not got_vectors:
        print_monitor('Warning: it is unusual that the identity dumpsave exists and the vector dumpsave does not. '
                      'Check if everything is correct.')
        vector_tot_length, poly_matrices_with_prefactor, _ = make_dumpsave(
            args, do_identity=False, do_vectors=True)
        with open(identity_file, 'rb') as f:
            print_monitor(f'Loading identity vector from {identity_file}')
            vector_tot_length, identity_block_vector = pickle.load(f)

    # If we have neither we just compute everything
    else:
        vector_tot_length, poly_matrices_with_prefactor, identity_block_vector = make_dumpsave(
            args, do_identity=True, do_vectors=True)

    return vector_tot_length, poly_matrices_with_prefactor, identity_block_vector


def numericize_expr_global(expr, args):
    """Turn expressions in the notebook into numbers"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    if re.match(r'-?\d+$', expr):
        return int(expr)
    elif re.match(r'-?\d+\.\d+$', expr):
        return mp.mpf(expr)
    else:
        expression = math_parser(expr)
        if expression.free_symbols == set():
            return mp.mpf(expression.evalf(mp.prec))
        else:
            symbols = {str(symbol) for symbol in expression.free_symbols}
            given = set(args.replacement.keys())
            if args.replacement is None or not symbols.issubset(given):
                print_monitor('Error: you must provide replacement rules for the symbols ' +
                              ','.join(symbols - given))
                sys.exit(1)
            else:
                rules = {key: mp.mpf(val) for key, val in args.replacement.items()}
                return mp.mpf(expression.evalf(mp.prec, subs=rules))


def replace_dollar_vars(path, args):
    """Replaces ${var} with the value of var from args.replacement"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    def action(match_obj):
        key = match_obj.group(1)
        return str(args.replacement[key])

    try:
        return re.sub(r'\${([^{}]+)}', action, path)
    except KeyError as e:
        print_monitor(f'Error: you must provide a replacement rule for the symbol {e}')
        sys.exit(1)


def get_right_deltaij_global(path, args):
    """Gets the correct args.delta_ij value"""

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    delta_ijs = []

    def save_delta_ij(match_obj):
        """Saves the delta_ij's found and at the same time builds the globbing pattern"""
        nonlocal delta_ijs
        i = int(match_obj.group(1)) - 1
        j = int(match_obj.group(2)) - 1
        try:
            delta_ij = Decimal(args.external_dim[i]) - Decimal(args.external_dim[j])
        except IndexError:
            print_monitor(f'Error: tried to load block with external operators {i} and {j} '
                          f'but only {len(args.external_dim)} dimensions provided: {args.external_dim}')
            sys.exit(1)
        else:
            delta_ijs.append(delta_ij)
        return '*'

    result = re.sub(r'\$D(\d)(\1)', '0', path)
    remaining_Dij = re.search(r'\$D(\d)(\d)', result)
    if remaining_Dij:
        # globbing pattern
        pattern = re.sub(r'\$D(\d)(\d)', save_delta_ij, result)
        files = glob.glob(pattern)
        # pattern that we use to compare a given file with the glob pattern
        re_pattern = pattern.replace('*', '(-?[0-9.]+)')

        for file in files:
            deltaij_match = re.match(re_pattern, file)
            compare_list = [Decimal(a) for a in deltaij_match.groups()]

            # if the external dimensions are equal as Decimals, we return
            if compare_list == delta_ijs:
                return file

        print_monitor(f'Error: no block found matching the pattern {pattern} with external dimensions ' +
                      ','.join(str(a) for a in delta_ijs))
        sys.exit(1)
    else:
        return result


def make_dumpsave(args, do_identity=True, do_vectors=True):
    """
    Makes the dumpsave of the crossing equations with the blocks substituted.
    The format is of binary type, and it is called "pickle"
    """

    def print_monitor(text):
        print(text)
        monitor.write_to_monitor(args.monitor_file, args.job, text)

    print_monitor('Parsing crossing equations and loading blocks...')

    with open(args.crosseqns, 'r') as c:
        extension = os.path.splitext(args.crosseqns)[1]
        if extension == '.json':
            print_monitor('Warning: The json format for the crossing equations is not implemented yet. '
                          "I'll read it anyway")
            crossing_eqns = json.load(c)
        elif extension in ['.m', '.wl']:
            crossing_eqns = mathematica.load_crosseqns(c, args)
        else:
            print_monitor('Error: The file is neither a json nor a mathematica file')
            sys.exit(1)

    ##########################################################
    # Parsing things like fileNames and derivMaps            #
    ##########################################################

    # Predefined values of derivMaps
    l = args.Lambda
    nm = args.n_max
    if args.zzb_ab_deriv == 'zzb':
        deriv_maps = {
            'even': [(n + i - 2, n - i) for n in range(1, nm + 1) for i in range(1, n + 1)],
            'odd': [(n + i - 1, n - i) for n in range(1, nm + 1) for i in range(1, n + 1)],
            'all': [(n + i - j, n - i) for n in range(1, nm + 1) for i in range(1, n + 1) for j in (1, 2)]
        }
    else:
        deriv_maps = {
            'even': [(m, n) for m in range(l + 1) for n in range(int((l - m) / 2) + 1) if m % 2 == 0],
            'odd': [(m, n) for m in range(l + 1) for n in range(int((l - m) / 2) + 1) if m % 2 == 1],
            'all': [(m, n) for m in range(l + 1) for n in range(int((l - m) / 2) + 1)]
        }

    # Parsing spinParity that associates a name to a choice of spins
    spin_parity = crossing_eqns.get('spinParity', None)

    # We also have a predefined value for spinParity, the obvious ones
    if spin_parity is None:
        spin_parity = {
            'Leven': [l for l in args.spin_selection if l % 2 == 0],
            'Lodd': [l for l in args.spin_selection if l % 2 == 1],
            'Lall': list(args.spin_selection)
        }
    else:
        for key, val in spin_parity.items():
            spin_parity[key] = eval(val)(args.l_max)

    # Add additional rule to parse single spins
    def select_spins(spin_key):
        try:
            return spin_parity[spin_key]
        except KeyError:
            try:
                return [int(spin_key)]
            except ValueError:
                print_monitor(f'Error: spin specification {spin_key} neither integer nor in ' + ','.join(spin_parity.keys()))
                sys.exit(1)

    # Parsing the pattern replacement that relate a block to its filename
    file_names = crossing_eqns.get('fileNames', None)

    # Parsing the pattern replacement that relate a block to its filename
    integrated_file_names = crossing_eqns.get('integratedFileNames', None)


    # We also have a predefined value for fileNames.
    # The predefined ones are as follows:
    # Name=scalar_blocks: it has all operators with the same dimension and is located in the block folder
    # Name=(alphabetic + _): it has all operators with the same dimension and is located in the Name folder
    # Name=(alphabetic + _)(four digits = ijkl):
    #       it corresponds to the block with external ops <ijkl>, it is located in the Name folder and it has
    #       delta12 = delta_i - delta_j and delta34 = delta_k - delta_l
    # Values of spin and deltaij are to be given as $S and $Dij (i.e. $D12, $D13, etc...)
    # To give a special name to a scalar block one must put it in a subfolder
    if file_names is None:
        file_names = {
            'scalar_block': '{}DerivTable-d{}-delta12-0-delta34-0-L$S-nmax{}-keptPoleOrder{}-order{}.m'.format(
                args.zzb_ab_deriv,
                args.dimensions,
                args.folder_n_max,
                args.kept_pole_order,
                args.order
            ),
            '[a-zA-Z_]+$': os.path.join(
                r'\0',
                r'{}DerivTable-d{}-delta12-0-delta34-0-L$S-nmax{}-keptPoleOrder{}-order{}.m'.format(
                    args.zzb_ab_deriv,
                    args.dimensions,
                    args.folder_n_max,
                    args.kept_pole_order,
                    args.order
                )
            ),
            r'[a-zA-Z_]*(\d)(\d)(\d)(\d)$': os.path.join(
                r'\0',
                r'{}DerivTable-d{}-delta12-$D\1\2-delta34-$D\3\4-L$S-nmax{}-keptPoleOrder{}-order{}.m'.format(
                    args.zzb_ab_deriv,
                    args.dimensions,
                    args.folder_n_max,
                    args.kept_pole_order,
                    args.order
                )
            )
        }

    if args.integrated_blocks and integrated_file_names is None:
        integrated_file_names = {
            'int_block': r'integratedBlock-d{}-delta12-0-delta34-0-L$S-rmax{}.m'.format(
                args.dimensions,
                args.folder_r_max,
            ),
            '[a-zA-Z_]+$': os.path.join(
                r'\0',
                r'integratedBlock-d{}-delta12-0-delta34-0-L$S-rmax{}.m'.format(
                    args.dimensions,
                    args.folder_r_max,
                )
            ),
            r'[a-zA-Z_]*(\d)(\d)(\d)(\d)$': os.path.join(
                r'\0',
                r'integratedBlock-d{}-delta12-$D\1\2-delta34-$D\3\4-L$S-rmax{}.m'.format(
                    args.dimensions,
                    args.folder_r_max,
                )
            )
        }

    def adjust_regex(regex):
        r"""Adjusts standard backreferences \1, \2, ... to python regex flavor \g<1>, \g<2>, ..."""

        regex = re.sub(r'\\(\d+)', r'\\g<\g<1>>', regex)
        return regex

    def get_filename(blockname, spin, custom_name, int_block):
        """Gets the block file name from the block name by looking in the regex dictionary"""

        # This function can be used for the integrated blocks or for the regular ones
        if int_block:
            location = args.integrated_blocks
        else:
            location = args.discrete_blocks if args.discrete_delta else args.blocks

        def get_right_deltaij(path):
            return get_right_deltaij_global(path, args)

        # the name of the file is deduced from the spin unless a custom name is given explicitly
        if custom_name is None:
            name_patterns = integrated_file_names if int_block else file_names
            for key, val in name_patterns.items():
                match = re.match(key, blockname)
                if match:
                    temp = re.sub(key, adjust_regex(val), blockname)

                    # This replaces $S with the spin and $Dij with args.external_dim[i] - ...[j]
                    temp = re.sub(r'\$S', str(spin), temp)
                    return get_right_deltaij(
                        os.path.join(location, temp)
                    )
        else:
            if spin:
                custom_name_final = re.sub(r'\$L', str(spin), custom_name)
            else:
                custom_name_final = custom_name

            custom_name_final = replace_dollar_vars(custom_name_final, args)

            if re.match(r'.*\.(m|wl)$', custom_name_final):
                return os.path.join(location, blockname, custom_name_final)
            else:
                return os.path.join(location, blockname, custom_name_final + '.m')

        print_monitor(f'Error: no filename found associated to the block name {blockname}')
        sys.exit(1)

    ##########################################################
    # Reading and rearranging crossing equations             #
    ##########################################################

    def numericize_expr(expr):
        return numericize_expr_global(expr, args)

    # Load the crossing equations
    crossing_equations = crossing_eqns['crossingEquations']

    # Load the things for the discrete delta
    if args.discrete_delta:
        try:
            # Load the integrated blocks
            integrated_blocks = crossing_eqns['integratedBlocks']

            # Load the integral constraint
            integral_constraint = crossing_eqns['integratedConstraint']

        except KeyError as e:
            print_monitor(f'Warning: the entry "{e.args[0]}" was not found in the crossing equations '
                          f'and --discrete-delta is set')
            integrated_blocks = None
            integral_constraint = None
    else:
        integrated_blocks = None
        integral_constraint = None

    # Load the identity block
    identity_block = crossing_eqns['identityBlock']

    # Load the names of the vectors
    vector_naming = crossing_eqns['vectorNaming']

    mp.prec = args.precision

    # These are the spinParity and delta_lists for each vector and the derivMaps for each matrix
    all_spin_par = {}
    all_deriv_map = {}

    # Loop through integrated constraints or not
    if integrated_blocks is None:
        all_crossing_eqns = [crossing_equations]
    else:
        all_crossing_eqns = [crossing_equations, integrated_blocks]

    if integral_constraint is None:
        all_constraints = [identity_block]
    else:
        all_constraints = [identity_block, integral_constraint]

    # Iterate over the equations: matrices of vectors (mathematica.py did the transposing)
    # We do this every time because we want to figure out all_spin_par and all_deriv_map
    # If needed, we do the same for the integrated constraints as well
    doing_integrated = False
    vector_tot_length = 0
    vector_length = 0
    for eqn_list in all_crossing_eqns:
        for v, vector in enumerate(eqn_list):
            for row in vector:
                for col in row:
                    for m, matrix_entry in enumerate(col):
                        if 'sum' in matrix_entry:
                            for addend in matrix_entry['sum']:

                                # We replace the string contained in the coefficient with its internal representation
                                current_coeff = addend['block*coeff'].get('coeff', ['1'])
                                if isinstance(current_coeff, list):
                                    # Pyparsing 2.4 outputs coeff : ['...'] while Pyparsing 3.0 outputs coeff : '...'
                                    current_coeff = current_coeff[0]
                                addend['block*coeff']['coeff'] = numericize_expr(current_coeff)

                                # We replace the block with a dictionary that has all the information ready to go
                                # Namely the filename (as a function of L), the derivMap, the deltaPhi (if needed)
                                cb = {'name': list(addend['block*coeff']['block'].keys())[0]}

                                # First we populate the dict
                                current_block_dict = addend['block*coeff']['block'][cb['name']]

                                cb['derivMap'] = current_block_dict.get('derivMap', None)
                                cb['spin'] = current_block_dict.get('spin', 'all')
                                # If deltaPhi is not provided it is assumed that the dimensions are fixed.
                                cb['deltaPhi'] = current_block_dict.get('deltaPhi', None)
                                # We give the possibility of specifying Δ, if not specified, the blocks remain polynomials
                                cb['delta'] = current_block_dict.get('delta', None)
                                if cb['delta'] is not None:
                                    cb['delta'] = numericize_expr(cb['delta'])

                                # It could be that the user wants to provide external_dim but keep deltaPhi precomputed
                                # Or that the user wants to use the default '(Δ2 + Δ3)/2' value.
                                # This is done with yes/true (use default) or no/false (do not replace deltaPhi)
                                if args.external_dim is not None:
                                    subs_for_deltaphi = {f'Δ{n+1}': mp.mpf(e_dim) for n, e_dim in enumerate(args.external_dim)}
                                    if cb['deltaPhi'] is None:
                                        pass
                                    elif cb['deltaPhi'].lower() in ['yes', 'true']:
                                        cb['deltaPhi'] = mp.mpf(math_parser('(Δ2 + Δ3)/2').evalf(mp.prec, subs_for_deltaphi))
                                    elif cb['deltaPhi'].lower() in ['no', 'false']:
                                        cb['deltaPhi'] = None
                                    else:
                                        cb['deltaPhi'] = mp.mpf(math_parser(cb['deltaPhi']).evalf(mp.prec, subs_for_deltaphi))
                                else:
                                    if cb['deltaPhi'] is not None and cb['deltaPhi'].lower() not in ['no', 'false']:
                                        print_monitor('Warning: deltaPhi provided but the external dimensions were not passed')
                                    cb['deltaPhi'] = None

                                # Then we also populate (and check) the spinParity and derivMap dictionaries
                                if v not in all_spin_par:
                                    all_spin_par[v] = cb['spin']
                                else:
                                    if all_spin_par[v] != cb['spin']:
                                        print_monitor('Error: the choices of spin selection rules are not the same for '
                                                      f'each vector. In particular the {v}th vector')
                                        sys.exit(1)

                                if m + vector_length not in all_deriv_map:
                                    all_deriv_map[m + vector_length] = cb['derivMap']
                                else:
                                    if cb['derivMap'] and all_deriv_map[m + vector_length] != cb['derivMap']:
                                        print_monitor('Error: the choices of derivative maps are not the same for '
                                                      f'each matrix. In particular the {m + vector_length}th matrix')
                                        sys.exit(1)

                                # almost never needed
                                cb['customName'] = current_block_dict.get('customName', None)

                                # Finally we put it back in
                                addend['block*coeff']['block'] = cb
                        else:
                            # We have encountered a zero entry, we infer derivMap and spin from the surrounding entries later
                            try:
                                del matrix_entry['zero']
                            except KeyError:
                                print_monitor('Warning: found entry without the sum key, treating it as zero vector.')
                            matrix_entry['sum'] = [{'block*coeff': {'block': {
                                'name': '_zero_block',
                                'deltaPhi': None,
                                'delta': None,
                                'customName': None
                            }}}]

        if not doing_integrated:
            vector_length = max(all_deriv_map.keys())
            vector_tot_length = sum([len(deriv_maps[m]) for m in all_deriv_map.values()])
            doing_integrated = True

    if integrated_blocks:
        try:
            vector_tot_length += len(integrated_blocks[0][0][0])
        except IndexError:
            print_monitor('Error: integratedBlocks is expected to be non-empty')
            exit(1)

    if do_identity:
        for constraint in all_constraints:
            for m, matrix_entry in enumerate(constraint):
                if 'sum' in matrix_entry:
                    for addend in matrix_entry['sum']:
                        # We replace the string contained in the coefficient with its internal representation
                        current_coeff = addend['block*coeff'].get('coeff', ['1'])
                        if isinstance(current_coeff, list):
                            # Pyparsing 2.4 outputs coeff : ['...'] while Pyparsing 3.0 outputs coeff : '...'
                            current_coeff = current_coeff[0]
                        addend['block*coeff']['coeff'] = numericize_expr(current_coeff)

                        # As before
                        cb = {'name': list(addend['block*coeff']['block'].keys())[0]}

                        # First we populate the dict
                        current_block_dict = addend['block*coeff']['block'][cb['name']]

                        cb['derivMap'] = current_block_dict.get('derivMap', None)
                        # If deltaPhi is not provided it is assumed that the dimensions are fixed.
                        cb['deltaPhi'] = current_block_dict.get('deltaPhi', None)
                        # We give the possibility of composing identity blocks by setting other blocks's Δ and L
                        cb['spin'] = int(current_block_dict.get('spin', 0))
                        current_delta = current_block_dict.get('delta', None)
                        if current_delta is not None:
                            cb['delta'] = numericize_expr(current_delta)
                        else:
                            cb['delta'] = 0

                        # Same as before
                        if args.external_dim is not None:
                            subs_for_deltaphi = {f'Δ{n+1}': mp.mpf(e_dim) for n, e_dim in enumerate(args.external_dim)}
                            if cb['deltaPhi'] is None:
                                pass
                            elif cb['deltaPhi'].lower() in ['yes', 'true']:
                                cb['deltaPhi'] = mp.mpf(math_parser('(Δ2 + Δ3)/2').evalf(mp.prec, subs_for_deltaphi))
                            elif cb['deltaPhi'].lower() in ['no', 'false']:
                                cb['deltaPhi'] = None
                            else:
                                cb['deltaPhi'] = mp.mpf(math_parser(cb['deltaPhi']).evalf(mp.prec, subs_for_deltaphi))
                        else:
                            if cb['deltaPhi'] is not None and cb['deltaPhi'].lower() not in ['no', 'false']:
                                print_monitor('Warning: deltaPhi provided but the external dimensions were not passed')
                            cb['deltaPhi'] = None

                        # Then we also check the derivMap dictionary
                        if cb['derivMap'] and all_deriv_map[m] != cb['derivMap']:
                            print_monitor('Error: the choices of derivative maps for the identity vector do not agree '
                                          f'with the polynomial matrices. In particular for the {m}th matrix')
                            sys.exit(1)

                        # We add the "param" field for the integrated correlator constraint
                        if cb['derivMap'] is None:
                            cb['param'] = current_block_dict.get('param', None)

                        # Finally the custom file name
                        cb['customName'] = current_block_dict.get('customName', None)

                        # Finally we put it back in
                        addend['block*coeff']['block'] = cb
                else:
                    # We have encountered a zero entry, we infer derivMap and spin from the surrounding entries later
                    try:
                        del matrix_entry['zero']
                    except KeyError:
                        print_monitor('Warning: found entry without the sum key, treating it as zero vector.')
                    matrix_entry['sum'] = [{'block*coeff': {'block': {
                        'name': '_zero_block',
                        'deltaPhi': None,
                        'delta': 0,
                        'customName': None
                    }}}]

    ##########################################################
    # Loading the blocks on the vectors                      #
    ##########################################################

    loaded_blocks = {}

    def get_block(blockname, spin, custom_name, integrated):
        """Creates and returns a block object from a file, or fetches it from loaded_blocks"""

        if (blockname, spin, custom_name, integrated) in loaded_blocks.keys():
            return loaded_blocks[(blockname, spin, custom_name, integrated)]
        else:
            if blockname == '_zero_block':
                # For the _zero_block spin actually stands for len(deriv_map)
                # and this thing contains already a PolynomialVector instance, not a Block instance
                if args.discrete_delta:
                    the_vec = polynomial_matrix.PolynomialVector(
                        [0] * spin, args.precision, convert_to_poly=False
                    )
                    loaded_blocks[(blockname, spin, custom_name, integrated)] = polynomial_matrix.DiscreteDeltaCollection(
                        [the_vec], [], 0
                    )
                else:
                    loaded_blocks[(blockname, spin, custom_name, integrated)] = polynomial_matrix.PolynomialVector(
                        [[0]] * spin, args.precision, 0
                    )
                return loaded_blocks[(blockname, spin, custom_name, integrated)]

            # This should be used only by the identity part when do_vectors is false because we do all this with a Pool
            else:
                if integrated:
                    filename = get_filename(blockname, spin, custom_name, True)
                    block = discrete_blocks.IntegratedBlock(filename, args)
                else:
                    filename = get_filename(blockname, spin, custom_name, False)
                    if args.discrete_delta:
                        block = discrete_blocks.Block(filename, args)
                    else:
                        block = blocks.Block(filename, args)
                for entry in block.block_entries:
                    loaded_blocks[(entry, spin, custom_name, integrated)] = block
                try:
                    return loaded_blocks[(blockname, spin, custom_name, integrated)]
                except KeyError:
                    print_monitor(f'Error: The file {filename} did not contain {blockname}')
                    sys.exit(1)

    def get_block_vector(blockname, spin, deriv_map, integrated, delta_phi=None, custom_name=None):
        """Gets the block from get_block and returns the vector with the given deriv_map"""

        if blockname == '_zero_block':
            # This is kind of a hack where we store the PolynomialVector of zeros in the same table as the Blocks
            # Then we use the second entry of the tuple for deriv_map instead of spin
            how_long = 1 if integrated else len(deriv_map)
            block = get_block('_zero_block', how_long, None, integrated)
            return block
        else:
            block = get_block(blockname, spin, custom_name, integrated)
            if delta_phi is not None:
                block.convolve_block(delta_phi)

            if args.discrete_delta:
                return block.make_vector(blockname, deriv_map)
            else:
                return block.make_poly_vector(blockname, deriv_map)

    def get_integral_constraint(blockname, custom_name, param_name):
        """Gets the integral constraint"""

        filename = get_filename(blockname, None, custom_name, True)
        block = discrete_blocks.IntegratedConstraint(filename, args, param_name)

        return block.make_vector(blockname)

    # We iterate again over the crossing equations and write the vectors here
    poly_matrices_with_prefactor = {}
    integrated_matrices = {}
    final_matrices = {}

    # This is just to check that the delta_list are consistent among vectors
    all_delta_lists = {}

    if do_vectors:
        # First we apply all the convolutions, then we construct the polynomial matrices
        print_monitor('Building polynomial matrices...')
        to_convolve = {}
        for v, vector in enumerate(crossing_equations):
            for spin in select_spins(all_spin_par[v]):
                for row in vector:
                    for col in row:
                        for matrix_entry in col:
                            for addend in matrix_entry['sum']:
                                name = addend['block*coeff']['block']['name']
                                custom_name = addend['block*coeff']['block']['customName']
                                if name != '_zero_block' and addend['block*coeff']['block']['deltaPhi'] is not None:
                                    to_convolve[(name, spin, custom_name)] = (
                                        get_filename(name, spin, custom_name, False),
                                        args,
                                        addend['block*coeff']['block']['deltaPhi']
                                    )
        saved_where = []
        if len(to_convolve) > 0:
            with Pool(processes=min(args.n_tasks, len(to_convolve)), initializer=initialize_pool) as pool:
                saved_where = pool.starmap(convolve_block_from_file, to_convolve.items())

        for name, spin, custom_name, file in saved_where:
            try:
                with open(file, 'rb') as f:
                    loaded_blocks[(name, spin, custom_name, False)] = pickle.load(f)
            except FileNotFoundError:
                print_monitor(f'Error: file {file} not found, something went wrong when multiplying by v^Δφ')
            except EOFError:
                print_monitor(f'Error: file {file} is corrupted, something went wrong when multiplying by v^Δφ')
            else:
                # We compute them every time we have do_vectors = True, so it's pointless to keep them
                os.remove(file)

        # We use the operator overloads to multiply by coeff and sum the blocks
        # It is assumed that upon sum and concatenation the prefactors agree
        # Here we rewrite vectors of matrices as matrices of vectors
        doing_integrated = False
        for eqn_list in all_crossing_eqns:
            for v, vector in enumerate(eqn_list):
                if not doing_integrated:
                    poly_matrices_with_prefactor[vector_naming[v]] = {}
                else:
                    integrated_matrices[vector_naming[v]] = {}
                for spin in select_spins(all_spin_par[v]):
                    # When discrete_delta is True, growing_matrix is a list of vector matrices, the entries being the
                    # values of Delta, otherwise it is a single matrix with polynomial vector entries
                    growing_matrix = []
                    current_prefactor = None
                    current_delta_minus_x = None
                    for r, row in enumerate(vector):
                        if not args.discrete_delta:
                            growing_matrix.append([])
                        for c, col in enumerate(row):
                            growing_vector = None
                            for m, matrix_entry in enumerate(col):
                                running_sum = None
                                for addend in matrix_entry['sum']:
                                    # Compute the block vector of a single addend
                                    this_deriv_map = addend['block*coeff']['block'].get('derivMap', all_deriv_map[m])
                                    new_vector = get_block_vector(
                                        addend['block*coeff']['block']['name'],
                                        spin,   # The spins have already been checked with all_spin_par
                                        deriv_maps[this_deriv_map] if this_deriv_map else None,
                                        doing_integrated,  # Integrated blocks?
                                        None,   # we did the convolution with deltaPhi already
                                        addend['block*coeff']['block'].get('customName', None)
                                    )

                                    # If the dimension was fixed
                                    delta_replacement = addend['block*coeff']['block']['delta']
                                    if delta_replacement is not None:
                                        try:
                                            new_vector = new_vector.evaluate(delta_replacement, as_poly=True)
                                            if not args.discrete_delta:
                                                # We must multiply by the prefactor because the blocks might be different
                                                evaluated_prefactor = get_block(
                                                    addend['block*coeff']['block']['name'],
                                                    spin,
                                                    addend['block*coeff']['block']['customName'],
                                                    False
                                                ).prefactor.evaluate(delta_replacement)
                                                new_vector = evaluated_prefactor * new_vector
                                        except ValueError as e:
                                            print_monitor(f'Error: {e}')
                                            sys.exit(1)

                                    # Multiply by coeff if necessary and add to the running total
                                    if 'coeff' in addend['block*coeff']:
                                        new_vector = addend['block*coeff']['coeff'] * new_vector
                                    if running_sum is None:
                                        running_sum = new_vector
                                    else:
                                        running_sum = running_sum + new_vector

                                    # We want to remember the prefactor too
                                    if all([
                                        current_prefactor is None,
                                        addend['block*coeff']['block']['name'] != '_zero_block',
                                        not args.discrete_delta
                                    ]):
                                        current_prefactor = get_block(
                                            addend['block*coeff']['block']['name'],
                                            spin,
                                            addend['block*coeff']['block']['customName'],
                                            False
                                        ).prefactor

                                    # And the delta_minus_x
                                    if current_delta_minus_x is None and \
                                            addend['block*coeff']['block']['name'] != '_zero_block':
                                        current_delta_minus_x = get_block(
                                            addend['block*coeff']['block']['name'],
                                            spin,
                                            addend['block*coeff']['block']['customName'],
                                            doing_integrated
                                        ).delta_minus_x

                                    # And also the delta_list
                                    if args.discrete_delta:
                                        this_delta_list = get_block(
                                            addend['block*coeff']['block']['name'],
                                            spin,
                                            addend['block*coeff']['block']['customName'],
                                            doing_integrated
                                        ).delta_list
                                        if v not in all_delta_lists and this_delta_list:
                                            all_delta_lists[v] = this_delta_list
                                        else:
                                            # We only check the length because this might give problems with precision
                                            if this_delta_list and len(all_delta_lists[v]) != len(this_delta_list):
                                                print_monitor('Error: the delta grid is not the same for '
                                                              f'each vector. In particular the {v}th vector')
                                                sys.exit(1)

                                # Concatenate vectors at each entry
                                if growing_vector is None:
                                    growing_vector = running_sum
                                else:
                                    growing_vector = growing_vector.concatenate(running_sum)

                            if args.discrete_delta:
                                # Put at entry (row,col) the vector we just computed
                                if not growing_matrix:
                                    for _ in range(len(growing_vector.vector_list)):
                                        growing_matrix.append(polynomial_matrix.Matrix([]))
                                for pos, entry in enumerate(growing_vector.vector_list):
                                    if len(growing_matrix[pos].matrix) <= r:
                                        growing_matrix[pos].matrix.append([entry])
                                    else:
                                        growing_matrix[pos].matrix[r].append(entry)
                            else:
                                # Put at entry (row,col) the vector we just computed
                                growing_matrix[-1].append(growing_vector)

                    # Now with all this work we can finally construct the final elements.
                    # For discrete_delta it is a DiscreteDeltaCollection while for regular bootstrap
                    # it is a PolynomialMatrixWithPrefactor
                    if args.discrete_delta:
                        discrete_delta_matrix = polynomial_matrix.DiscreteDeltaCollection(
                            growing_matrix,
                            [] if current_delta_minus_x is None else all_delta_lists[v],
                            current_delta_minus_x
                        )
                        if doing_integrated:
                            integrated_matrices[vector_naming[v]][spin] = discrete_delta_matrix
                        else:
                            poly_matrices_with_prefactor[vector_naming[v]][spin] = discrete_delta_matrix
                    else:
                        poly_matrix_with_prefactor = polynomial_matrix.PolynomialMatrixWithPrefactor(
                            current_prefactor,
                            growing_matrix,
                            current_delta_minus_x,
                            args.precision
                        )
                        poly_matrices_with_prefactor[vector_naming[v]][spin] = poly_matrix_with_prefactor

            doing_integrated = True

        # Finally we concatenate the two
        if integrated_matrices:
            for key in poly_matrices_with_prefactor.keys():
                final_matrices[key] = {}
                for spin in poly_matrices_with_prefactor[key].keys():
                    final_matrices[key][spin] = (
                        poly_matrices_with_prefactor[key][spin]
                    ).concatenate(integrated_matrices[key][spin])
        else:
            final_matrices = poly_matrices_with_prefactor

    identity_block_vector = None

    if do_identity:
        # Do the same as before but for the identity block
        print_monitor('Building identity block...')

        identity_block_vector = None
        doing_integrated = False

        for constraint in all_constraints:
            for m, matrix_entry in enumerate(constraint):
                running_sum = None
                for addend in matrix_entry['sum']:

                    addend_spin = addend['block*coeff']['block'].get('spin', 0)
                    this_deriv_map = addend['block*coeff']['block'].get('derivMap', all_deriv_map[m])
                    addend_deriv = deriv_maps[this_deriv_map] if this_deriv_map else None
                    addend_delta = addend['block*coeff']['block'].get('delta', 0)
                    custom_name = addend['block*coeff']['block'].get('customName', None)
                    param_name = addend['block*coeff']['block'].get('param', None)

                    # Compute the block vector of a single addend
                    if not doing_integrated:
                        new_vector = get_block_vector(
                            addend['block*coeff']['block']['name'],
                            # If not specified, the spin is zero, so identity blocks must be saved as having zero spin
                            addend_spin,
                            addend_deriv,
                            False,
                            addend['block*coeff']['block']['deltaPhi'],
                            custom_name
                            # If the blocks contain an x we set it to a particular value, by default -delta_minus_x
                        ).evaluate(addend_delta)
                    else:
                        new_vector = get_integral_constraint(
                            addend['block*coeff']['block']['name'],
                            custom_name,
                            param_name
                        )

                    # Compute the prefactor as well (calling get_block again is free because it is stored in the dict)
                    if addend['block*coeff']['block']['name'] != '_zero_block' and not args.discrete_delta:

                        try:
                            prefactor = get_block(
                                addend['block*coeff']['block']['name'],
                                addend_spin,
                                custom_name,
                                False
                            ).prefactor.evaluate(addend_delta)

                            new_vector = prefactor * new_vector
                        except ZeroDivisionError:
                            print_monitor(f'Error: the prefactor has a pole at Δ={addend_delta}. '
                                          'If you are trying to evaluate the identity block call it "identity" and '
                                          'set --auto to true')
                            sys.exit(0)

                    # Multiply by coeff if necessary and add to the running total
                    if 'coeff' in addend['block*coeff']:
                        new_vector = addend['block*coeff']['coeff'] * new_vector
                    if running_sum is None:
                        running_sum = new_vector
                    else:
                        running_sum = running_sum + new_vector

                # Concatenate vectors at each entry
                if identity_block_vector is None:
                    identity_block_vector = running_sum
                else:
                    identity_block_vector = identity_block_vector.concatenate(running_sum)

            doing_integrated = True

        identity_block_vector = identity_block_vector.polyvector

    ##########################################################
    # Saving or returning the output                         #
    ##########################################################

    # If the external dimensions are fixed and there are no parameters we can save everything.
    # If there are some free parameters in the identity we can save only the vectors
    # If the user gave dumpsave explicitly we save everything (unless skip_identity_dumpsave is set)
    if args.command == 'dumpsave':
        if args.skip_identity_dumpsave:
            # Save vectors
            save_vectors, save_identity = True, False
        else:
            # Save everything
            save_vectors, save_identity = True, True
    elif args.no_dumpsave:
        # The user asked explicitly to not save
        save_vectors, save_identity = False, False
    elif args.external_dim is None:
        if args.replacement is None:
            # No replacements nor external dim, save everything
            save_vectors, save_identity = True, True
        else:
            # We must recompute the identity each time
            save_vectors, save_identity = True, False
    else:
        # We cannot save anything a priori
        save_vectors, save_identity = False, False

    # If dumpsave is enabled we save all jobs into a single file, otherwise we save every job in its own file
    if save_vectors:
        vector_file_name = os.path.join(args.sdpfiles, 'pickles', 'dumpsave-vectors.pickle')
        try:
            with open(vector_file_name, 'x+b') as f:
                print_monitor('Saving vectors on ' + vector_file_name)
                pickle.dump((vector_tot_length, final_matrices), f)
        except FileExistsError:
            pass

    # As for the identity, we don't need to save it when dumpsave is disabled
    if save_identity:
        try:
            identity_file_name = os.path.join(args.sdpfiles, 'pickles', 'dumpsave-identity.pickle')
            with open(identity_file_name, 'x+b') as f:
                print_monitor('Saving identity on ' + identity_file_name)
                pickle.dump((vector_tot_length, identity_block_vector), f)
        except FileExistsError:
            pass
    # If we called this with do_identity = False identity_block_vector is None, so we don't want to save it
    elif do_identity:
        # If we can't save the identity vector project-wide, we save it for a single job together with the external dims
        # and the replacements
        os.makedirs(os.path.join(args.sdpfiles, 'pickles', f'temp_j{args.job}'), exist_ok=True)
        identity_file_name = os.path.join(args.sdpfiles, 'pickles', f'temp_j{args.job}', f'identity.pickle')
        dim_repls_file_name = os.path.join(args.sdpfiles, 'pickles', f'temp_j{args.job}', f'dims_and_repls.pickle')
        with open(identity_file_name, 'w+b') as f:
            pickle.dump((vector_tot_length, identity_block_vector), f)
        with open(dim_repls_file_name, 'w+b') as f:
            pickle.dump((args.external_dim, args.replacement), f)

    # This returns, in order, the length of the vectors, the polynomial matrix vectors and the identity
    return vector_tot_length, final_matrices, identity_block_vector
