(* ::Package:: *)

(****************************************
*  Template for the crossing equations  *
*****************************************

You need to define the following things:

  1. crossing vectors
  2. identity block vector
  3. naming of the vectors

You may define the following things

  4. derivative maps
  5. file names of the blocks
  6. selection rules for the spins

Note that you don't need a Mathematica licence for this.
It will be parsed by the Python program. As such, the allowed
syntax is a subset of Mathematica's and you cannot run any 
code here.

The mandatory entries require no knowledge in Python or Mathematica.
The optional entries require a bit of knowledge in Python and in
regular expressions.

****************************************)


(****************************************
*  Crossing vectors                     *
*****************************************

The crossing equations

    \[CapitalSigma]_i \[Lambda]OPE^T \[CenterDot] Vector_i(x) \[CenterDot] \[Lambda]OPE = -identityBlock

can be written as follows

    crossingEquations = { Vector1, Vector2, ... };

where   Vector<i> = { Matrix<i>1, Matrix<i>2, ... }
and  Matrix<i><j> = { {entry<i><j>11, entry<i><j>12, ...},
                      {entry<i><j>21, entry<i><j>22, ...},
                      ...                               }
and entry<i><j><k> is a linear combination of conformal blocks.

If you have a single 1\[Times]1 matrix, you can simplify this input as

    crossingEquations = entry;

If you have a single vector of 1\[Times]1 matrices, you can do

    crossingEquations = { entry1, entry2, ... };

If you have a list of vectors of 1\[Times]1 matrices, you can do

    crossingEquations = { Vector1, Vector2, ... };

with Vector<i> = { entry<i>1, entry<i>2, ...}

This simplification can be done also for a subset of the vectors.

*****************************************

The identity block vector is given in the form 

    identityBlock = { entry1, entry2, ... }

****************************************)


(****************************************
*  Conformal blocks                     *
*****************************************

A conformal block has the form

    F[<name>][<rules>]
or
    H[<name>][<rules>]

F is for loading the vector of odd derivatives and H for loading
the vector of even derivatives.
<name> is a Mathematica symbol or a string identifying the block.
It tells the program what block to load. There are two cases:

  1. The blocks are computed by other means and exported in JSON.
     Then <name> is the corresponding entry in the JSON file.
     The path of the file that contains the entry <name> needs to
     be specified in fileNames (see next).
     
  2. The blocks are computed by scalar_blocks. Then
   
     i) if <name> is "scalar_block", the blocks are searched in
         the value of the option --blocks.
    ii) if <name> is a combination of letters and "_" the blocks
         are searched in the folder <--blocks>/<name>, but only
         those with \[CapitalDelta]12 = \[CapitalDelta]34 = 0
   iii) if <name> is a combination of letters and "_" plus 4 digits
         from 1 to 9, the blocks are searched in <--blocks>/<name>
         but additionally python will know the values of \[CapitalDelta]12, \[CapitalDelta]34
         that it will need to fetch.
         
    E.g. if --block is /home/foo/blocks/, <name> is g3312 and \[CapitalDelta]1-\[CapitalDelta]2=0.3
    then python will look for blocks with \[CapitalDelta]12=0, \[CapitalDelta]34=0.3 in the folder
    /home/foo/blocks/g3312/
 

<rules> is a list of Rules and it must contain

    spin \[Rule] (Leven|Lodd|Lall),

while it can optionally contain

    deltaPhi \[Rule] (yes|no)|"<formula in terms of \[CapitalDelta]1, \[CapitalDelta]2, \[CapitalDelta]3, \[CapitalDelta]4>"

yes is equivalent to (\[CapitalDelta]2+\[CapitalDelta]3)/2
no mean do not add v^DeltaPhi to the blocks

\[CapitalDelta]1,\[CapitalDelta]2,... are Symbols. They will be replaced by their actual
values on every single run.

The blocks can additionally have parameters

    spin \[Rule] int
    delta \[Rule] real

to specify the spin and the dimension of the block. In the identity block, these are set to zero by default.
If you have your identity block saved as a separate file you must name it with "...-L0-...".

A sum of conformal blocks has the form

    coefficient1 block1 + coefficient2 block2 + ...

If coefficient<i> is a number, it can be put as such.
If it is a Mathematica expression it needs to be enclosed
in the Head coeff, e.g.

    coeff[N^2(N+1)/(N-2)] F[..][..] - 3 F[..][..]

****************************************)


(****************************************
*  Vector naming                        *
*****************************************

When you input assumptions such as gaps or operators to bisect
you need to refer to a vector where to apply the shifts in x.

This is done by setting

    vectorNaming = {<vector name 1>, <vector name 2>, ...};

Then you can assume a gap on the vector <vector name 1> of spin L by calling
sailboot.py with the option

   --assumptions '(<vector name 1>, L, "gap", <value of the gap>)'

If vectorNaming is missing the vectors will be assigned names "1", "2", ...
If there are not enough names the remaining ones will be filled up as
"<n>", "<n+1>", etc...

****************************************)


(****************************************
*  Other options                        *
*****************************************

The other options modify the defaults.

The filenames are given as 

    fileNames = { <name or regex> \[Rule] "<string>", ...};

and they give instruction on how to find the file containing an entry <name>.

The spin selections are given as

    spinSelection = { <string> \[Rule] "<python pure function>", ...};
    
and they give instruction as to what spins to select.

Then "<python pure function>" is a lambda expression that
  1. In the first case takes the value of Lambda and returns a list of 2-tuples,
     namely derivative orders in z,zb: (n,m) = \[PartialD]_z^n \[PartialD]_zb^m
  2. In the last case takes the value of L_Max and returns a list of integers,
     namely the spins.
  
While "regex" can contain any regular expression and <string> may contain references
to its captured groups with \0 (the whole expression), \1 (first group), \2, etc...
Then it can also contain placeholders $S (replaced by the spin) and $Dij (replaced
by \[CapitalDelta]ij with i,j=1,2,3,4).

****************************************)


crossingEquations = F["scalar_block"][{derivMap->odd, spin->Leven, deltaPhi->yes}];


identityBlock = F["identity"][{derivMap->odd, deltaPhi->yes}];


vectorNaming = {sigma};
