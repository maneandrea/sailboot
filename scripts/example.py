# This is an example of a python script that generates jobs
# It needs to define these things:
# - a function generate_args_list() which returns a list (or generator) of strings that can be parsed by argparse
# - a string proj_name
# - an absolute path proj_path
# - a string sbatch_opts that returns the header of the sbatch script

proj_name = 'example_run'
proj_path = '/home/your/directory/example_run'


# Unless you provide sdpb_bin in launcher.config, make sure to export the location of sdpb and pvm2sdp in PATH
sbatch_opts = '''\
#!/bin/bash
#SBATCH --chdir=/your/project/directory/
#SBATCH --time=24:00:00
#SBATCH --ntasks-per-node=10
#SBATCH --nodes=2
#SBATCH --mem=0
#SBATCH --job-name=test
#SBATCH --account=account-name

module load python
export PATH="/path/of/sdpb/executables/:$PATH"
'''

# Call ./sailboot.py --help to see the meaning of the parameters
# Things in the curly brackets will be replaced by .format(...) below
arguments = '''\
bisect -L 11 -p 650 -t 1e-3
-b '("sigma",0,"gap")' -m 3 -M 5
-a '("sigma",0,"isolatedValue",{0:6f})'
--job-param {0:6f}
--config {1}'''.replace('\n', ' ')

# Here you can put your custom config file. This is the default one given in the scripts folder
config_file = 'scripts/launcher.config'


def generate_args_list():
    """Must return a list (or generator) of strings that can be parsed by myargparser.single_argparse"""

    for i in [2, 2.1, 2.2, 2.3, 2.4, 2.5]:
        yield arguments.format(i, config_file)


##########################################
# Note if you are unfamiliar with "yield":
#
# For our purposes
#
# def generate_args_list():
#     ret = []
#     for i in <ranges>:
#         ret.append(<stuff>)
#     return ret
#
# is equivalent to
#
# def generate_args_list():
#     for i in <ranges>:
#         yield <stuff>
#
##########################################
