from setuptools import setup
import subprocess
import os
import re

VERSION_INFO = 'v1.2'

try:
    from_git = subprocess.run(['git', 'describe', '--dirty'], capture_output=True)
    if from_git.stdout.decode('utf-8'):
        VERSION_INFO = from_git.stdout.decode('utf-8').rstrip()
    else:
        raise RuntimeError(from_git.stderr.decode('utf-8'))
except FileNotFoundError:
    print('Warning: git executable not found, cannot parse version')
except RuntimeError as e:
    print('Warning: git execution produced the following error:')
    print(e)


s = setup(
    name='sailboot',
    version='1.2',
    packages=['sailboot', 'sailboot.classes', 'sailboot.makesdp', 'sailboot.utilities'],
    package_dir={'sailboot': 'src'},
    url='https://gitlab.com/maneandrea/sailboot',
    license='GPL v3.0',
    author='Andrea Manenti',
    author_email='andrea [dot] manenti [at] yahoo [dot] com',
    description='Highly customizable framework for performing numerical bootstrap computations',
    install_requires=['mpmath', 'pyparsing']
    # Uncomment to save tests and docs to /usr/share
    # , data_files=[('share/sailboot/docs', ['docs/documentation.pdf',
    #                                      'docs/documentation.tex',
    #                                      'docs/macros.sty',
    #                                      'docs/refs.bib',
    #                                      'docs/fig/test_plot.pdf']),
    #             ('share/sailboot/tests', ['tests/test.py',
    #                                       'tests/singlet_bound_N20.py',
    #                                       'tests/identity_block.py', 'tests/local_sbatch.py',
    #                                       'tests/param.sdpb', 'tests/paramOPE.sdpb',
    #                                       'tests/launcher.config',
    #                                       'tests/ON_crossing_eqns.m', 'tests/show_plot.nb'])]
)

# Build hook -- inject version info in sailboot.py
try:
    build_path = os.path.join(s.command_obj['build'].build_lib, 'sailboot', 'utilities')

    with open(os.path.join(build_path, 'myargparser.py'), 'r') as f:
        new_code = re.sub(r"VERSION_INFO = '[\w\-.]+'", f"VERSION_INFO = '{VERSION_INFO}'", f.read())

    with open(os.path.join(build_path, 'myargparser.py'), 'w') as f:
        f.write(new_code)

except KeyError:
    pass

# Install hook -- symlink executable in the bin directory
try:
    src_path = os.path.join(s.command_obj['install'].install_lib, 'sailboot')
    bin_path = s.command_obj['install'].install_scripts

    os.makedirs(bin_path, 0o755, exist_ok=True)

    for exe in ['sailboot', 'launch', 'monitor', 'grid_evaluate']:
        src = os.path.join(src_path, exe + '.py')
        lnk = os.path.join(bin_path, exe)

        os.chmod(src, 0o755)

        if not os.path.isfile(lnk):
            os.symlink(src, lnk)

except KeyError:
    pass
